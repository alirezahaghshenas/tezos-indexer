# Open Source License
# Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.


db-schema-help:
	@echo "/****************************************************************"
	@echo "It is recommended that you run the following command to get the complete DB schema:"
	@echo "${MAKE} db-schema-all-default"
	@echo "****************************************************************/"
db-schema:
	@${MAKE} -s --no-print-directory db-schema-help
	@echo "-- If you want to immediately run \`${MAKE} db-schema-all-default\`, press ENTER, else Ctrl-C" ; read
	@${MAKE} -s --no-print-directory db-schema-all-default

# This is one of the 2 rules that are recommended
db-schema-all-default:
	@${MAKE} -s --no-print-directory db-schema-core-default
	@${MAKE} -s --no-print-directory db-schema-tokens
	@${MAKE} -s --no-print-directory db-schema-mezos
	@${MAKE} -s --no-print-directory db-schema-tokens-mezos

# This is one of the 2 rules that are recommended
db-schema-all-multicore:
	@${MAKE} -s --no-print-directory db-schema-core-multicore
	@${MAKE} -s --no-print-directory db-schema-tokens
	@${MAKE} -s --no-print-directory db-schema-mezos
	@${MAKE} -s --no-print-directory db-schema-tokens-mezos
	@echo "-- Don't forget to 'fix' your DB after you're done with multicore mode, to ensure integrity of the data, by running \`make db-schema-all-default\`."

# db-schema-deactivate-multicore:
# #	@${MAKE} -s --no-print-directory db-schema-chain-base | grep -e --AFTERMULTICORE | bash src/db-schema/make_fkey.bash
# 	@${MAKE} -s --no-print-directory db-schema-all-default

# Rule - convenience, but slow
db-schema-views:
	@echo "-- src/db-schema/views.sql"
	@cat src/db-schema/views.sql

# Sub-meta-rule
db-schema-core-multicore:
	@${MAKE} -s --no-print-directory db-schema-versions | grep -ve --SEQONLY
	@${MAKE} -s --no-print-directory db-schema-chain-multicore
	@${MAKE} -s --no-print-directory db-schema-chain-pkeys-multicore
	@${MAKE} -s --no-print-directory db-schema-chain-fkeys-multicore
	@${MAKE} -s --no-print-directory db-schema-mempool

# Sub-meta-rule
db-schema-core-default:
	@${MAKE} -s --no-print-directory db-schema-versions | grep -ve --MULTICORE
	@${MAKE} -s --no-print-directory db-schema-chain-default
	@${MAKE} -s --no-print-directory db-schema-chain-pkeys-default
	@${MAKE} -s --no-print-directory db-schema-chain-fkeys-default
	@echo "-- src/db-schema/conversion_from_multicore.sql"
	@cat src/db-schema/conversion_from_multicore.sql
	@${MAKE} -s --no-print-directory db-schema-mempool

# Sub-rule
db-schema-versions:
	@echo "-- src/db-schema/versions.sql"
	@cat src/db-schema/versions.sql

# Sub-rule
db-schema-chain-base:
	@echo "-- src/db-schema/schemas.sql"
	@cat src/db-schema/schemas.sql
	@echo "-- src/db-schema/addresses.sql"
	@cat src/db-schema/addresses.sql
	@echo "-- src/db-schema/chain.sql"
	@cat src/db-schema/chain.sql
	@echo "-- src/db-schema/chain_functions.sql"
	@cat src/db-schema/chain_functions.sql
	@echo "-- src/db-schema/bigmaps.sql"
	@cat src/db-schema/bigmaps.sql

# Sub-meta-rule
db-schema-chain-default:
	@${MAKE} -s --no-print-directory db-schema-chain-base | grep -ve --MULTICORE

# Sub-meta-rule
db-schema-chain-multicore:
	@${MAKE} -s --no-print-directory db-schema-chain-base | grep -ve --SEQONLY | grep -ve --CONFLICT | sed -e 's/--NONCONFLICT//'

# Sub-meta-rule
db-schema-chain-fkeys-default:
	@${MAKE} -s --no-print-directory db-schema-chain-default | bash src/db-schema/make_fkey.bash

# Sub-meta-rule
db-schema-chain-fkeys-multicore:
	@${MAKE} -s --no-print-directory db-schema-chain-multicore | bash src/db-schema/make_fkey.bash

# Sub-meta-rule
db-schema-chain-pkeys-default:
	@${MAKE} -s --no-print-directory db-schema-chain-default | bash src/db-schema/make_pkey.bash

# Sub-meta-rule
db-schema-chain-pkeys-multicore:
	@${MAKE} -s --no-print-directory db-schema-chain-multicore | bash src/db-schema/make_pkey.bash

# Sub-rule
db-schema-mempool:
	@echo "-- src/db-schema/mempool.sql"
	@cat src/db-schema/mempool.sql

# Sub-rule
db-schema-tokens:
	@echo "-- src/db-schema/tokens.sql"
	@cat src/db-schema/tokens.sql

# Sub-meta-rule
db-schema-tokens-mezos:
	@${MAKE} -s --no-print-directory db-schema-tokens-mezos-base
	@${MAKE} -s --no-print-directory db-schema-tokens-mezos-extra

# Sub-rule
db-schema-tokens-mezos-base:
	@echo "-- src/db-schema/mezos_tokens.sql"
	@cat src/db-schema/mezos_tokens.sql

# Sub-rule
db-schema-tokens-mezos-extra:
	@echo "-- ocaml src/db-schema/mezos_tokens_gen_sql_queries.ml"
	@ocaml src/db-schema/mezos_tokens_gen_sql_queries.ml

# Sub-meta-rule
db-schema-mezos:
	@${MAKE} -s --no-print-directory db-schema-mezos-base
	@${MAKE} -s --no-print-directory db-schema-mezos-extra

# Sub-rule
db-schema-mezos-base:
	@echo "-- src/db-schema/mezos.sql"
	@cat src/db-schema/mezos.sql
	@echo "-- src/db-schema/block_finality.sql"
	@cat src/db-schema/block_finality.sql
	@echo "-- src/db-schema/operation_functions.sql"
	@cat src/db-schema/operation_functions.sql

# Sub-rule
db-schema-mezos-extra:
	@echo "-- ocaml src/db-schema/mezos_gen_sql_queries.ml"
	@ocaml src/db-schema/mezos_gen_sql_queries.ml

# leave the following on a single line, it's more convenient for things like `grep`
.PHONY: clean protos test-psql db-schema-help db-schema db-schema-all-default db-schema-all-multicore db-schema-views db-schema-core-multicore db-schema-core-default db-schema-versions db-schema-chain-base db-schema-chain-default db-schema-chain-multicore db-schema-chain-fkeys-default db-schema-chain-fkeys-multicore db-schema-chain-pkeys-default db-schema-chain-pkeys-multicore db-schema-mempool db-schema-tokens db-schema-tokens-mezos db-schema-tokens-mezos-base db-schema-tokens-mezos-extra db-schema-mezos db-schema-mezos-base db-schema-mezos-extra
