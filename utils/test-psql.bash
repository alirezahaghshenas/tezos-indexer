#!/bin/bash
# Open Source License
# Copyright (c) 2019-2020 Nomadic Labs <contact@nomadic-labs.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

set -xe
db=tezos-indexer-test-$(date +%Y%m%d%H%M%S)$((RANDOM))
createdb $db
make db-schema-all-default | psql $db
psql $db
echo "will run (dropdb $db) unless you Ctrl-C (in which case you'll be responsible for deleting it)"
read
dropdb $db



##################################################
exit
##################################################

echo "***** Pulling docker image (will take some time if you don't already have it)"
set -ex
docker pull postgres:12.4-alpine
set +ex
echo "***** OK."

for option in -medium -light -heavy ; do
    echo "***** Creating temporary local directory for storing postgres data"
    mkdir -p tmp-postgres
    echo "***** OK."
    echo "***** Run postgres docker image (note: this will fail if your local 5432 port is busy)"
    set -ex
    hash=$(docker run --rm  --name pg-docker -e POSTGRES_PASSWORD=docker -d -p 15432:5432 -v $PWD/tmp-postgres:/var/lib/postgresql/data postgres)
    set +ex
    echo "***** OK."
    echo "***** Waiting until the docker image is up and running -- warning, this will run forever if it fails to connect and you don't kill it"
    while sleep 1 ; do
        echo -n '.'
        if docker ps|grep -q '0.0.0.0:15432->5432/tcp'
        then
            echo
            break
        fi
    done
    echo "***** OK."
    echo "***** Waiting until we can actually connect to the postgres server (which is running in docker) -- will abort if it still can't connect after 100 seconds"
    for i in {1..100} ; do
        echo -n '.'
        sleep 1
        if PGPASSWORD=docker psql -p 15432 -h localhost -U postgres -d postgres <<< "select 'OK';" 2> /dev/null
        then
            echo
            break
        fi
    done
    echo "***** OK."
    echo "***** make db-schema-all$option"
    make db-schema-all$option | PGPASSWORD=docker psql -h localhost -p 15432 -U postgres -d postgres
    echo "***** OK."
    echo "***** Running psql in case you want to run manual testing:"
    PGPASSWORD=docker psql -h localhost -p 15432 -U postgres -d postgres
    echo "***** OK."
    echo "***** Stopping docker image"
    docker stop $hash
    echo "***** OK."
    echo "***** Removing temporary postgres directory"
    rm -fr tmp-postgres
    echo "***** OK."
done

echo "***** Bye!"
