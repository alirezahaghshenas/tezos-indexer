#!/bin/bash
# Open Source License
# Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
# Copyright (c) 2021 Philippe Wang <philippe.wang@gmail.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.


if [[ ${MULTISEGMENT_TEZOS_INDEXER+true} != "true" ]]
then
    $(dirname $(dirname "$0"))/tezos-indexer "$@"
    exit $?
fi

set -xe

###################################################################################
########## database
HOST=${POSTGRES_HOST:-localhost}
DB=${DB_NAME:$USER}
PORT=${POSTGRES_PORT:-5432}
PG_USER=${POSTGRES_USER:-tezos}

DB_USER=${DB_USER:-phil}
export PGPASSWORD=${DB_PASS:-phil}
PSQL_OPTIONS="-a"

########## node
TEZOS_NODE=${TEZOS_NODE:-"http://localhost:8732"}
TEZOS_NODE2=${TEZOS_NODE2:-$TEZOS_NODE}
TEZOS_NODE3=${TEZOS_NODE3:-$TEZOS_NODE}
TEZOS_NODE4=${TEZOS_NODE4:-$TEZOS_NODE}

########## indexer
TI=${INDEXER_SRC_PATH:-$(dirname $(dirname "$0"))}
LOGS_DIR=/var/log/tezos-indexer

BLOCK_CACHE_DIR=${BLOCK_CACHE_DIR:-"/blocks"} # no space in the path or it might fail
TEZOS_NETWORK=${TEZOS_NETWORK:mainnet}
INDEXER_DATAKINDS="--no-contract-balances --no-snapshot --skip-prebaby --no-watch --tokens-support"
INDEXER_RUN="--use-disk-cache --debug --verbosity=1 --verbose"
INDEXER_OTHER_OPTIONS=

########## parallelism
gsize=10000 # size of a segment of the chain
top=${TEZOS_TOP:-1475000} # last block to index
# It seems the best speed is achieved with a number of parallel jobs equal to 1.5 times the number of logical cores:

if [[ "${PROCESSES}" == "" ]] ; then
    if [[ -f /proc/cpuinfo ]] ; then
        PROCESSES=$(grep processor /proc/cpuinfo | wc -l)
    else
        PROCESSES=$(sysctl -n hw.ncpu)
    fi
    ((PROCESSES= PROCESSES * 15 / 10))
    if (( PROCESSES < 1 )) ; then
        PROCESSES=4 ;
    fi
fi
if (( PROCESSES == 0 )) ; then
    PROCESSES=1
fi
###################################################################################

# Override settings:
override="$(dirname "$0")/run-tezos-indexer-multicore.override.bash"
if [[ -f "$override" ]]
then
    . "$override"
fi

###################################################################################
# If you need to modify something below this line, please consider opening a merge request!
###################################################################################

PSQL="time psql -h $HOST -p $PORT -U $DB_USER $DB $PSQL_OPTIONS"
LOGS="$LOGS_DIR"/indexer-segment-$gsize-$HOST-$DB
INDEXER_OPTIONS1="$INDEXER_DATAKINDS $INDEXER_RUN $INDEXER_OTHER_OPTIONS --db=postgresql://$DB_USER@$HOST:$PORT/$DB --tezos-url=$TEZOS_NODE  --block-cache-directory=$BLOCK_CACHE_DIR"
INDEXER_OPTIONS2="$INDEXER_DATAKINDS $INDEXER_RUN $INDEXER_OTHER_OPTIONS --db=postgresql://$DB_USER@$HOST:$PORT/$DB --tezos-url=$TEZOS_NODE2 --block-cache-directory=$BLOCK_CACHE_DIR"
INDEXER_OPTIONS3="$INDEXER_DATAKINDS $INDEXER_RUN $INDEXER_OTHER_OPTIONS --db=postgresql://$DB_USER@$HOST:$PORT/$DB --tezos-url=$TEZOS_NODE3 --block-cache-directory=$BLOCK_CACHE_DIR"
INDEXER_OPTIONS4="$INDEXER_DATAKINDS $INDEXER_RUN $INDEXER_OTHER_OPTIONS --db=postgresql://$DB_USER@$HOST:$PORT/$DB --tezos-url=$TEZOS_NODE4 --block-cache-directory=$BLOCK_CACHE_DIR"


dropdb   -h $HOST -p $PORT -U $PG_USER $DB || true
createdb -h $HOST -p $PORT -U $PG_USER -O $DB_USER $DB


(cd $TI && make db-schema-all-multicore) | $PSQL

# get the list of all existing contract addresses <-- this is important to avoid deadlocks (which cause postgres to fail, therefore tezos-indexer to crash)
if ! [[ -f "$BLOCK_CACHE_DIR/list_of_contracts-$top.txt" ]]
then
    curl -s $TEZOS_NODE/chains/main/blocks/$top/context/contracts | tr -d '"[]'  | tr ',' '\n' | awk "{print \$0\" \"NR}" >  "$BLOCK_CACHE_DIR/list_of_contracts-$top.txt"
fi
# record all existing contract addresses
echo "\\copy c.addresses (address, address_id) from '$BLOCK_CACHE_DIR/list_of_contracts-$top.txt' DELIMITER ' ';" | $PSQL

(( iterations = top / gsize ))

set +x
(
    # starting by end of chain should go faster because blocks are slower to index there than at the beginning of the chain
echo -n "all: do-boot "
for (( i=iterations-1; i>=0; i-- )) ; do
    echo -n "do-$i "
done

echo
echo "do-boot:"
printf "\t%s\n" "PGPASSWORD=$PGPASSWORD time $TI/tezos-indexer --force-from=0 --bootstrap-upto=1 $INDEXER_OPTIONS1 > $LOGS-0-1.log 2> $LOGS-0-1.error.log"
echo -e '\t@echo Target \"do-boot\" done: $$(date)'

echo
for (( i=0; i<iterations; i++ )) ; do
    size=$gsize
    start=$(( i * size ))
    if (( start == 0 ))
    then
        start=2
    fi
    (( end = (i+1) * size ))
    echo "do-$i:"
    case "$(( i % 4 ))" in
        0)
            printf "\t%s\n" "PGPASSWORD=$PGPASSWORD time $TI/tezos-indexer --force-from=$start --bootstrap-upto=$end $INDEXER_OPTIONS1 > $LOGS-$start-$end.log 2> $LOGS-$start-$end.error.log || (x=\$\$? ; tail $LOGS-$start-$end.error.log && exit \$\$x)"
            ;;
        1)
            printf "\t%s\n" "PGPASSWORD=$PGPASSWORD time $TI/tezos-indexer --force-from=$start --bootstrap-upto=$end $INDEXER_OPTIONS2 > $LOGS-$start-$end.log 2> $LOGS-$start-$end.error.log || (x=\$\$? ; tail $LOGS-$start-$end.error.log && exit \$\$x)"
            ;;
        2)
            printf "\t%s\n" "PGPASSWORD=$PGPASSWORD time $TI/tezos-indexer --force-from=$start --bootstrap-upto=$end $INDEXER_OPTIONS3 > $LOGS-$start-$end.log 2> $LOGS-$start-$end.error.log || (x=\$\$? ; tail $LOGS-$start-$end.error.log && exit \$\$x)"
            ;;
        *)
            printf "\t%s\n" "PGPASSWORD=$PGPASSWORD time $TI/tezos-indexer --force-from=$start --bootstrap-upto=$end $INDEXER_OPTIONS4 > $LOGS-$start-$end.log 2> $LOGS-$start-$end.error.log || (x=\$\$? ; tail $LOGS-$start-$end.error.log && exit \$\$x)"
            ;;
    esac
    echo -e '\t@echo Target \"do-'$i'\" done: $$(date)'
done ) > temporary-Makefile
start="$(date)"

if (( PROCESSES <= 1 )) ;
then
    PGPASSWORD=$PGPASSWORD time $TI/tezos-indexer $INDEXER_OPTIONS1 > $LOGS-solo-multicore.log 2> $LOGS-solo-multicore.error.log
else
    make all -j "$PROCESSES" -f temporary-Makefile
fi

end="$(date)"
echo "Indexing started on $start - ended on $end"
# $TI/tezos-indexer --db-schema | time $PSQL
time make db-schema-all-default | $PSQL
echo "Indexing started on $start - ended on $end"


[[ $# -gt 0 ]] && $TI/tezos-indexer "$@"
