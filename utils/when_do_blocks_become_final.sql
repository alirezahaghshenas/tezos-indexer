CREATE OR REPLACE FUNCTION epoch_time_stamp (block_level int)
RETURNS int
AS $$
select extract(epoch from timestamp) from c.block where level = block_level;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION block_is_final (block_level int)
RETURNS bool
AS $$
DECLARE
  d int := (select level from c.block order by level desc limit 1) - block_level;
  t int := 0;
BEGIN
  IF d > 9 THEN RETURN true; END IF;
  d := (select count(distinct(level)) from c.block where level > block_level);
  IF d = 3 THEN t := (SELECT epoch_time_stamp(block_level+d)) - (SELECT epoch_time_stamp(block_level)); IF t >= 180 AND t <= 227 THEN RETURN true; END IF; END IF;
  IF d = 4 THEN t := (SELECT epoch_time_stamp(block_level+d)) - (SELECT epoch_time_stamp(block_level)); IF t >= 240 AND t <= 455 THEN RETURN true; END IF; END IF;
  IF d = 5 THEN t := (SELECT epoch_time_stamp(block_level+d)) - (SELECT epoch_time_stamp(block_level)); IF t >= 300 AND t <= 683 THEN RETURN true; END IF; END IF;
  IF d = 6 THEN t := (SELECT epoch_time_stamp(block_level+d)) - (SELECT epoch_time_stamp(block_level)); IF t >= 360 AND t <= 903 THEN RETURN true; END IF; END IF;
  IF d = 7 THEN t := (SELECT epoch_time_stamp(block_level+d)) - (SELECT epoch_time_stamp(block_level)); IF t >= 420 AND t <= 1107 THEN RETURN true; END IF; END IF;
  IF d = 8 THEN t := (SELECT epoch_time_stamp(block_level+d)) - (SELECT epoch_time_stamp(block_level)); IF t >= 480 AND t <= 1327 THEN RETURN true; END IF; END IF;
  IF d = 9 THEN t := (SELECT epoch_time_stamp(block_level+d)) - (SELECT epoch_time_stamp(block_level)); IF t >= 540 AND t <= 1539 THEN RETURN true; END IF; END IF;
  RETURN false;
END;
$$ LANGUAGE PLPGSQL;


alter table c.block add column final_after smallint;


drop function mark_block_final_after;
CREATE OR REPLACE FUNCTION mark_block_final_after (block_level int)
RETURNS smallint
AS $$
DECLARE
  t int := 0;
BEGIN
  t := (SELECT epoch_time_stamp(block_level+3)) - (SELECT epoch_time_stamp(block_level)); IF t >= 180 AND t <=  227 THEN update c.block set final_after = 3 where level = block_level; RETURN 3; END IF;
  t := (SELECT epoch_time_stamp(block_level+4)) - (SELECT epoch_time_stamp(block_level)); IF t >= 240 AND t <=  455 THEN update c.block set final_after = 4 where level = block_level; RETURN 4; END IF;
  t := (SELECT epoch_time_stamp(block_level+5)) - (SELECT epoch_time_stamp(block_level)); IF t >= 300 AND t <=  683 THEN update c.block set final_after = 5 where level = block_level; RETURN 5; END IF;
  t := (SELECT epoch_time_stamp(block_level+6)) - (SELECT epoch_time_stamp(block_level)); IF t >= 360 AND t <=  903 THEN update c.block set final_after = 6 where level = block_level; RETURN 6; END IF;
  t := (SELECT epoch_time_stamp(block_level+7)) - (SELECT epoch_time_stamp(block_level)); IF t >= 420 AND t <= 1107 THEN update c.block set final_after = 7 where level = block_level; RETURN 7; END IF;
  t := (SELECT epoch_time_stamp(block_level+8)) - (SELECT epoch_time_stamp(block_level)); IF t >= 480 AND t <= 1327 THEN update c.block set final_after = 8 where level = block_level; RETURN 8; END IF;
  t := (SELECT epoch_time_stamp(block_level+9)) - (SELECT epoch_time_stamp(block_level)); IF t >= 540 AND t <= 1539 THEN update c.block set final_after = 9 where level = block_level; RETURN 9; END IF;
  RETURN 10;
END;
$$ LANGUAGE PLPGSQL;
