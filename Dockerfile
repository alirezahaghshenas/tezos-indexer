# Open Source License
# Copyright (c) 2019-2021 Nomadic Labs <contact@nomadic-labs.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

FROM registry.gitlab.com/tezos/opam-repository:runtime-build-test-dependencies--4f2882db0c98232c8c12eaa3ace975ef42dfc12c as build

USER root
RUN apk add opam alpine-sdk m4 gmp-dev postgresql-dev perl libev-dev libffi libffi-dev
RUN apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/community hidapi-dev
RUN apk add postgresql-client

USER tezos
WORKDIR /home/tezos
ENV OPAMYES=true

RUN opam remote add default https://opam.ocaml.org

COPY tezos.Makefile .
RUN make tezos -f tezos.Makefile
COPY opam.Makefile .
# RUN make install-opam-deps-4.10.2 -f opam.Makefile
RUN make install-minimal-opam-deps -f opam.Makefile

COPY *.Makefile ./
COPY .git .git
RUN sudo chown tezos /home/tezos/.git
RUN git checkout src utils/run-tezos-indexer-multicore.bash Makefile
RUN eval $(opam env) && make protos
RUN eval $(opam env) && dune build ./_build/default/src/bin_indexer/main_indexer.exe
RUN cp -f ./_build/default/src/bin_indexer/main_indexer.exe tezos-indexer
# RUN rm -rf _build tezos
RUN /home/tezos/tezos-indexer --version
ENTRYPOINT ["/home/tezos/utils/run-tezos-indexer-multicore.bash"]
