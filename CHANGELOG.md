# Changelog

The recent version tags (`major.minor.patch`) follow these rules:

- `major` will be bumped each time it requires to re-run the indexer
  through the entire chain
- `minor` will be bumped when there's significant changes that don't
  require to bump the `major` number
- `patch` will be bumped for small changes that don't break
  compatibility whatsoever

That being said, the most important thing to determe the need to
update your indexer will probably be the descriptions of the changes
themselves.

### v9.3.8 - 2021-06-07 - add support for PtGRANAD
- add support for PtGRANAD
- fix mempool
- fix indexing of florencenet (silly-big values in big maps were breaking the URI matching)

### v9.3.7 - 2021-06-01 - multicore-mode-fix: breaking typo
- same as v9.3.4 if you don't use multicore mode
- same as v9.3.6 but with a 1-byte fix
- update version from 9.3.0 to 9.3.7 in versions.sql

### v9.3.6 - 2021-05-31 -- (skip me)
- failed tiny fix for v9.3.5

### v9.3.5 - 2021-05-31 - bug fix for when converting multicore mode to default mode
- fix: try harder at creating primary keys and foreign keys, and fail harder too when there's any issue
  - thanks to Edmond Lee (EcadLabs) for reporting the issue
  - this fix is also released in v9.1.9
- fix: compatibility with postgresql 12 for src/db-schema/operation_functions.sql

### v9.3.4 - 2021-05-27
- add extraction of tezos-storage URI
- multicore run: make cache of addresses depend on target block's height
- add convenience SQL functions `operation_id: (char * smallint * smallint) -> bigint` and `operation_id: (char * int * int) -> bigint`
- fix tokens foreign keys in multicore mode: some FA1.2 tokens were not being indexed in multicore mode
- add new read functions for operations: src/db-schema/operation_functions.sql


### v9.3.3 - 2021-05-21
- fix: assign 0 to balanceless contracts (which are contracts that tried to be originated but failed for whatever reason)
- add SQL convenience functions `operation_kind: smallint -> char` and `balance_kind: smallint -> char` to conveniently convert smallint representation of operation kinds or balance kinds into text representations
- update SQL for postgres 12
- fix mezos.sql: upgrade to v9.3's types

### v9.3.2 - 2021-05-16
- same as v9.3.0 but exits faster in case of error, and fix breaking typo in SQL schema

### v9.3.1 - 2021-05-16 -- broken, use ≥v9.3.2 instead
- same as v9.3.0 but exits faster in case of error

### v9.3.0 - 2021-05-14 -- broken, use ≥v9.3.2 instead
- **This version requires to re-index the whole chain. Read-only queries for v9.2.x are expected to still work.**
  * `C.tx.parameters` is now of type `jsonb` instead of `text`, but if you have a read query that stops working on that, you can apply a quick fix: just cast the column to text as in `C.tx.parameters::text`

- add big map names from annotations from contract storage types
  * new column: `c.bigmap.annots text`
- add some indexing of contract addresses and URI from smart contracts and big maps
  * new table: `c.uri`
  * new columns: `c.contract_script.strings text[]`, `c.contract_script.uri int[]`, `c.contract_script.contracts bigint[]`
  * new columns: `c.tx.strings text[]`, `c.tx.uri int[]`, `c.tx.contracts bigint[]`
  * new columns: `c.bigmap.strings text[]`, `c.bigmap.uri int[]`, `c.bigmap.contracts bigint[]`
  * new functions: `address(bigint []) returns char[]` (which overloads `address(bigint) returns char`), `uri(int []) returns char[]`
- add failsafe for columns of type `jsonb`: all `jsonb` columns that might contain the unsupported `\u0000` character now have a failsafe `json` column (which supports the NULL character)
  * for every column `name jsonb` (except for `error_trace jsonb`), now there is also a `name_ json` column, which will be filled only when data is rejected by the `jsonb` format (so far it has happened very rarely, so those columns are expected to be mostly empty columns)
  * regarding the `jsonb` columns that do not have a corresponding `json` column: that's because it should be "impossible enough" to have any "zero byte" in there

### v9.2.1 - 2021-04-28
- (multicore mode only) fix big map indexing
  * requires to reindex the whole chain if you indexed in multicore mode **and** use big map data
    * an alternative that avoids having to reindex the whole chain exists but it's complicated to explain, contact us if you want to go that way
- if you don't use multicore mode, this is the same as v9.2.0

### v9.1.7 - 2021-04-28
- (multicore mode only) fix big map indexing
  * requires to reindex the whole chain if you indexed in multicore mode **and** use big map data
    * an alternative that avoids having to reindex the whole chain exists but it's complicated to explain, contact us if you want to go that way
- if you don't use multicore mode, this is the same as v9.1.6

### v9.2.0 - 2021-04-21
- indexing non-applied operations
- computing block finality for {mainnet, edo2net, florencenet}
- fix indexes for table C.balance_updates_op (they were wrongly named and were consequently not being built at all)

### v9.1.6 - 2021-04-09
- make getting contract balances more resistant to chain reorganizations (https://gitlab.com/nomadic-labs/tezos-indexer/-/merge_requests/127)
- fix: do not try to get script of scriptless contracts more than once per contract

### v9.1.5 - 2021-04-06
- blockchain reorganization: more aggressive approach when deleting blocks
  - this fix was implemented because the reorganization that happened on edo2net at level 147276 made the indexer crash
  - if your indexer is stuck there, run `delete from c.block_hash where hash_id < 0; delete from c.block_hash where hash_id>147275;` and restart the indexer
  - more information recorded in database and more debug information printed when reorganization happens

### v9.1.4 - 2021-03-30
- print protocols when failing to run mempool watcher

### v9.1.3 - 2021-03-30
- same as v9.1.2 except built with an upgraded build system

### v9.1.2 - 2021-03-24
- fix mempool watch
- add `--watch-hook` CLI option to run an arbitrary command after a block has been indexed during the watch process

### v9.1.1 - 2021-03-18
- no upgrade from v9.1.0 needed if you never index blocks multiple times
- fix bigmap re-indexing (i.e., only for blocks that were already indexed)
- fix operation re-indexing

### v9.1.0 - 2021-03-17

- this version might be compatible to some extent with v9.0.x but it's recommended that you reindex from scratch, if you can.
  - if you absolutely need to upgrade an existing v9.0.x to v9.1.0, please get in touch.

- support for proto 9 (without baking accounts) `PsFLoren`
  - add indexing of `Endorsement_with_slot`
- fix update balance on nets other than mainnet
- change chain reorganization treatment: delete all blocks from lowest level of reorganization and reindex from there
  - this will force `block_level` = `block_hash_id` (some `block_hash_id` may not be equal to `block_level` temporarily: they'll be deleted when the reorganization is being treated)
- fetching headers of blocks 0 and 1 from node instead of having them hardcoded
  - this allows to use sandbox or private networks
- automatic selection of protocol (by "use next one if current one fails") during bootstrap
  - this simplifies CLI options
  - options `--mainnet` and all `--[name]net` have been deleted
  - options `--proto-001` .. `--proto-008` have been deleted
  - if your setup uses those options, you need to remove them or the indexer will fail to recognize those options and therefore exit
- remove table `C.contract`
- add tables `C.contract_script` and `C.manager`
- fix `big_map_diff` "clear" instruction
- fully deactivate file cache building by tezos-indexer: use `curl` instead!


### v9.0.6 - 2021-03-10
- update tezos dependency to use latest tokens API: fixes read_bytes error on operations that callback on contracts with entrypoints
- fix `C.operation_alpha.operation_kind` for "internal manager operations" (the value in that column for those internal operations where the value of the operation containing them)

### v9.0.5 - 2021-03-01
- update tezos dependency to use latest tokens API

### v9.0.4 - 2021-02-25
- fix big map copies (which were incomplete)
  * **if you use big maps, you'll have to reindex the whole chain, or at least reset the big map table (`delete from c.bigmap;`) and re-index the blocks that do have big map diffs.**
- add SQL index to C.endorsement
- add `--sql-rate`, which now also accepts 0 (default is still 1)
  * in other previous v9s, `--sql-rate` was removed and the SQL transaction size in blocks was 100
- multicore mode has significantly changed, it now runs without foreign keys and almost no primary keys, and almost no indexes, those are built afterwards when switching to non-multicore mode

### v9.0.3 - 2021-02-19
- hot fix: remove wrong deletion of blocks

### v9.0.2 - 2021-02-19 (use v9.0.3 instead)
- add missing fkeys (`c.operation_sender_and_receiver`) and not null (`c.operation_alpha.autoid`)

### v9.0.1 - 2021-02-18 (use v9.0.3 instead)
- fix proposal operation
  * bug went undetected because it may not be an issue when indexing by segments in multicore mode
- fix ballot operation
  * bug went undetected because `ophid` was used instead of `opaid`, and likely all values of `ophid` are in `opaid` (`opaid` is a greater set, which includes `ophid` or almost)
- remove `--depth` because it's likely useless (and because it was not supposed to be in v9.0.0 anyways)

### v9.0.0 - 2021-02-16

This release compacts the DB by simply avoiding repeating long hashes
in many tables, and also by removing a few unuseful columns.  As a
result the size on disk has been reduced by about 75%.

It also allows to index by segments, which means you can index a
specific segment of your choice, and you may do so concurrently.  With
an Intel Core i7-10710U and a good SSD, you can expect to process 7000
to 9000 blocks per minute on average indexing mainnet's first
1,320,000 blocks using that method, for the "core" data (i.e., the
data that comes directly from the blockchain's blocks).

- (beta8) fix recording of multiple blocks having the same level (reorganisation of the chain)
- (since beta7) add automatic transition from protocol 7 to 8 during bootstrap when using `--mainnet` option
- support edo2net and mainnet's upcoming upgrade to PtEdo2Zk
- **DB schema upgrade**: mainnet's first 1.32M blocks' data now takes less than 50GB on disk (instead of about 200GB)
  * to help with transitioning to the new schema, `src/db-schema/views.sql` provides SQL views that can act like tables of v8, only much slower (because they're views and views are slow unless they're tiny)
  * main tables are now under SQL schema `C`. For instance, `block` is now `C.block`.
  * token tables are now under SQL schema `T`.
  * bigmap tables are now under SQL schema `B`.
  * table `implicit` was removed; table `C.activation` was added. Tables `C.activation` and `C.reveal` contain all the information that was previously accessed using table `implicit`.
  * table `C.operation_sender_and_receiver` now also stores the information for Seed Nonce Relevation, Double Endorsement Evidence, Double Baking Evidence, Activate Account, Proposals.
  * queries written for previous versions will all need to be updated to new schema. It is expected that complex queries become less complex, while the most simple queries become more complex. In the former case, it's because joins should require less comparisons; in the latter case, it's because upgraded tables are less self-contained than before. Complex queries are also expected to perform better thanks to requiring fewer comparisons.
- 🆕 **multi-segment** indexing allows to spread the work to multiple Postgres processes
  * `--bootstrap-upto` combined with `--force-from` are the base that allows you to pick a segment to index, and run several indexers concurrently on multiple segments
  * `--threads` is *not* the recommended approach for speeding up (because it's not ready yet)
  * running concurrent indexer instances require having a "multi-core" DB schema, which can be obtained using `tezos-indexer --db-schema-multicore`
  * once you're done bootstrapping, you need to update the DB schema to a non-multicore schema, or you'll run into issues during the watch process because of chain reorganizations. Use `tezos-indexer --db-schema` to get the regular DB schema.
  * for an example of a setup to concurrently index multiple segments of the blockchain, cf. `utils/run-tezos-indexer-multicore.bash`
  * for mainnnet (it's irrelevant for other networks) it's recommended that you use `--skip-prebaby-contracts` on all indexers but one (which has to go further than Babylon)
  * it's recommended that you use `--no-contract-balances` on all indexers but one, which should ideally be running `--contract-balances-only`, if you want to speed things up
- **multiple jobs**: it is recommended that you use an indexer or a pool of indexer for only indexing the direct contents of the blocks, and several other instances to index other data: `--snapshots`, `--mempool-only`, `--contract-balances-only`
  * If you need FA1.2 tokens to be indexed, you need to activate `--tokens-support` on all segments containing tokens. They cannot be indexed without going through the chain.
- **cache**: it's highly recommended that you use a cache for the chain' s blocks, to allow reducing the number of queries to the node and to speed things up. Option `--block-cache-directory` does not imply `--use-disk-cache`, so don't forget the latter. You may use `--do-cache-blocks` for simplicity but it's not as efficient as running a `curl`-based script such as the example in the README file.
- **distance to postgres**: keep in mind that the physical distance between `tezos-indexer` and `postgresql` processes accounts for a non-zero ping, and increasing the distance can dramatically slow down the whole process. The load on `postgresql` is by far superior to the load on `tezos-indexer`. If you run two machines, it's recommended that you use `postgres` on the faster machine.
- **`fsync='off'`** in postgresql's configuration file may speed things up but also increase the chances of data corruption. It is recommended that you use `fsync='on'` if the bootstrap phase is over.

## 8.1.9 - 2021-02-14
- add automatic transition from protocol 7 to 8 during bootstrap when using `--mainnet` option

## 8.1.8 - 2021-02-11
- add support for Edonet2
- add support for PtEdo2Zk activation on mainnet
- drop support for Edonet

## 8.1.4 - 2021-01-28
- indexing of bigmaps for protocol Edo
- Fix `get_reveal` and `get_origination` (mezos)

## 8.1.3- 2021-01-26
- fix `version.sql` when writing to vanilla database

## 8.1.2 - 2021-01-21
- activate mempool watching for edo

## 8.1.1 - 2021-01-21
- fix: insertion for tokens (crash at block 61957 on delphinet)
- SQL: size of bigmap.{sender,receiver}

## 8.1.0 - 2021-01-20
- semantics update on table `balance`
- fix: do not commit transaction in case of crash (this unwelcome bug came with `--sql-rate`)

## 8.0.2 - 2021-01-17
- fix: fallback to RPC to get block when disk cache exists but is corrupted (wrong data or wrong blocks)
- improve logs
- update mezos for internal operations {internal, nonce}

## 8.0.1 - 2021-01-16
Upgrading to this version from 8.0.0 is necessary if you're using `--tokens-support`.
- fix: `--tokens-support`: some amounts were causing "more or less silent" overflows

## 8.0.0 - 2021-01-15

This release introduces quite a lot of new features and SQL schema upgrades (new tables and new columns).
This version deprecates all previous versions because of some important bug fixes that will not be backported to previous versions.

- tezos-indexer version recorded in the DB
  * new table: `indexer_version_history`
  * it will be much harder to start the indexer on an incompatible database!
- SQL: stop on error! Insignificant errors have been removed. All remaining errors are treated as serious enough to stop.
  * `tezos-indexer --db-schema | psql` will now abort on the first error encountered
  * `make db-schema-all-default` produces the same result as `tezos-indexer --db-schema`
  * it is, more than ever, recommended that you get the DB schema by using `tezos-indexer --db-schema` (or `make db-schema-all-default`), other generation rules or ways might give you incomplete and non-working DB schema
  * reminder: if you use the Makefile to get the DB schema, `make db-schema-all-default` is the recommended rule
  * SQL statements have been modified to avoid generating useless error messages
  * having an existing DB from a non-compatible version will trigger an error, therefore prevent you from applying the DB schema
  * we're going full postgresql! That means we no longer hesitate to use PG-specific features.
- ⚠️ `contract_balance.block_level` is now of type `int` instead of `bigint`
- include genesis (a.k.a. block 0) and block 1 in DB to ⚠️ enforce chain continuity⚠️  in DB
  * the `block` table has an additional foreign reference constraint that makes this break write compatibility with v7
  * this makes deletion of "uncle blocks" safer and easier
  * Note that blocks 0 and 1 do not contain operations (but if for some reason they do, they won't be indexed: please open an issue!)
  * please consider them as dummy blocks: if you need reliable data for blocks 0 and 1, please open an issue
  * requires indexing the chain from block 0 (can no longer index from any arbitrary block)
- beta: 🆕 indexing bigmaps
  * new table: `bigmap`
  * to read bigmaps, cf. SQL functions `src/db-schema/bigmaps.sql`
- beta: 🆕 indexing FA1.2 tokens (activate with `--tokens-support`), of course they come with new tables (cf. `src/db-schema/tokens.sql`).
- new tables: ⚠️ `double_endorsement_evidence`, ⚠️ `double_baking_evidence` (cf. `src/db-schema/chain.sql`)
- new columns (cf. `src/db-schema/chain.sql`)
  * ⚠️  `block_hash` has been added to a number of tables
  * ⚠️  primary keys have changed
  * ⚠️  foreign keys have changed (most will now trigger ⚠️ cascade⚠️  deletion⚠️  when need be)
  * overall this gives higher confidence in data integrity and speeds up dramatically the cascade-deletion of blocks (necessary to treat reorgs — a.k.a. rejected blocks or uncle blocks)
  * cf. `src/db-schema/chain.sql`
- ⚠️  new columns (`internal smallint`) in a few tables to deal with "internal manager operations", which were not properly recorded in prior versions (all versions ≤ 7.0.1) and would result in corrupted data! (about 0.1% of manager operations would end up corrupted)
- ⚠️  add table `operation_sender_and_receiver`, ⚠️  drop `operation_alpha.{sender,receiver}`
- ⚠️  table `balance` has new column `internal` and new `non null` constraints, and has dropped foreign references
- dropping support for carthagenet
  * if you still need support for carthagenet, open an issue, it can be put back if need be
- 🆕 fine tuning: now you have the possibility to choose that some data not be stored in the DB by overwriting some SQL functions
  * cf. `src/db-schema/feign-recording.sql`
  * Please try not to end up with an incompatible set of functions.
- (⚠️  not for the casual user) 🆕 CLI option: `--force-from` to tell the indexer to start indexing in the "past relative to the highest block already recorded in the database"

## 7.0.1 - 2020-12-27
- bug fix: some file descriptors were leaking (i.e. not being deleted in time) when using block cache files (too many file descriptors being open would cause the process to crash)
- update to compile and link with Tezos 8 (we were already using a beta version of Tezos 8 but a few breaking changes have happened nonetheless)

## 7.0.0 - 2020-12-17
- add column `fee` to table `origination`
- add columns `fee` and `consumed_milligas` to table `delegation`
- add table `reveal`
- ability to read blocks from disk cache (which needs to be separately generated)
- ability to fetch blocks from node to build disk cache (quite slow for now though)
- fill the `contract_balance` table in another thread (with the possibility to do it in another process)


## 6.2.0 - 2020-12-08
- (mainnet only) automatically fetch default scripts for scriptless pre-babylon KT contracts
  - this new feature should work by simply stopping any v6 and resuming with this version
- fix/optimization: remove some useless (duplicated) SQL queries

## 6.1.1 - 2020-12-02
- add `--db-schema` and `--rejected-blocks-depth`
  - `--db-schema` prints the default full DB schema and exists
  - `--rejected-blocks-depth` allows to specify how far in the past to look for rejected blocks (default is 100, and 0 means not to look at all)

## 6.1.0 - 2020-11-30
- add support for edonet
- drop support for ebetanet

## 6.0.0 - 2020-11-26
- fix deletion of rejected (aka uncle blocks)
  - this was causing the indexer to poorly crash in case a rejected block would actually be more recently indexed than the non-rejected block of the same level

## 5.1.0 - 2020-11-26 **HOTFIX**
- fix rejected block (aka uncle block) deletion

## 5.0.1 - 2020-11-25
- add `--ebetanet` option

## 5.0.0 - 2020-11-25
- support for proto 8 (ebetanet) (note that edonet does not exist yet, it will be supported at a later time)
  - block_alpha.voting_period is now of type jsonb instead of int
    - for now it's just storing integers in jsonb, but in the future it will likely store more complex data
    - for now you may use a double cast to get value as a number:
      - `select ((voting_period->0)::text)::numeric from block_alpha limit 1;`
      - `select ((voting_period->0)::text)::int from block_alpha limit 1;`

## 4.0.4 - 2020-11-26 **HOTFIX** (should've been 4.1.0)
- fix rejected block (aka uncle block) deletion

## 4.0.3 - 2020-11-23
- fix mezos queries for getting operation data

## 4.0.2 - 2020-11-19
- fix: use this if previous v4.x.y failed to compile due to `version.ml` being wrongly generated

## 4.0.1 - 2020-11-18
- fix: delete rejected blocks sequentially (instead of risking doing it concurrently)

## 4.0.0 - 2020-11-18
- fix: manager numbers (the operation ID column was missing)
- add (or activate) more indexes
- record minimal log into database
- add `--version` to make it easier to know which version is running
- add unique constraint to `autoid` columns
- add SQL functions to delete blocks
- rejected blocks are now automatically deleted right after being detected
- new option `--keep-rejected-blocks` to prevent automatic deletion of rejected blocks: if you use this flag, rejected blocks will be marked, but not deleted
- re-fix `--verbosity` option
- move `contract_legacy` view to heavy database scheme option (because that view is a lot too slow)
- add `src/db-schema/drop_tables.sql` to make emptying DB easier

## 3.1.1 - 2020-11-12
- start indexing (almost) immediately (instead of waiting for the next baked block from the node)
- <s>fix `--verbosity` option</s> (commit is missing)
- fix SQL function contracts3 for mezos

## 3.1.0 - 2020-11-09
- automatically detect protocol for mempool indexing (requires updating the SQL schema; does not change SQL tables)

## 3.0.2 - 2020-11-06
- <s>new `--verbosity` option</s> (broken feature, fixed in 3.1.1)

## 3.0.1 - 2020-11-06
- delete SQL view `tx_full`

## 3.0.0 - 2020-11-05

**This release contains quite a significant amount of changes.
Please wait a few days after its release before using it, to see if bug fixes follow shortly or not.
But feel free to try it immediately and report bugs (if any)!**

### Changes requiring to re-index the whole chain
- keep all balance updates in new `contract_balance` table
  - update of related SQL functions accordingly
- keep history in `contract` table (Warning: major semantics change if you've been using that table)
  - update of related SQL functions accordingly
- change primary key from address to (address, block_hash) to allow history and simpler semantics
- index "seed nonce revelation" operations
- **in the PG SQL database, numbers encoded in an little-endian hexadecimal data are now directly stored as SQL numbers: this improves human readability when browsing the contents of the database! But it also breaks any reader that would still expect to read HEX data!**

### Other changes
- snapshot blocks can now be indexed concurrently to the watch process
- indexing snapshot blocks is now running forever (instead of doing a single cycle)
- move all DB schema files to src/db-schema
- fix typo: `s/db.*scheme/db.*schema/g`
- mark rejected (aka "uncle" or "forked") blocks during watch process, at each level

## 2.3.1 - 2020-11-03
- support automatic protocol transition during `watch` (i.e., no need to stop and restart the indexer anymore)
- support mainnet's transition to protocol 7 during bootstrap

## 2.3.0 - 2020-11-01
- add indexing of mempool

## 2.2.3 - 2020-10-29
- revert v2.2.2 and re-fix it

## 2.2.2 - 2020-10-28
- fix an issue for delphinet (a foreign key that makes the indexer crash on block 4)

## 2.2.1 - 2020-10-27
- fix: mezos SQL function contracts3

## 2.2.0 - 2020-10-27
- fix `contract` table's upsert!

## 2.0.0 - 2020-10-26

### Changes requiring to re-index the whole chain
- store consumed_milligas instead of consumed_gas (because of future transition to PsDELPH1)
- add indexing of (big_map_diff, consumed_gas, storage_size, paid_storage_size_diff) for originations
- store Z numbers as PG numeric instead of little-endian hex in PG char(64)
- add full indexing of endorsements (cf. `endorsment` table)
- **fix**: update block hash for re-assigned operations
  - happens only when "uncle blocks" happen and existing operations are reassigned to a new block

### Other changes
- add src/mezos-db/mempool.sql for indexing mempool operations
  - mempool operations will be indexed by mezos, not by tezos-indexer at first; that might be moved to tezos-indexer in the future
- reverse order of operations returned by some mezos functions: new order order is from newest to oldest
  - some SQL function names are changed consequently
- minor fix: add 2 missing indexes on addresses in table `operation_alpha`
- cosmetic change: some refactoring (code simplification)
- store all json data as Postgres `jsonb` type instead of text or json

## 1.0.2 - 2020-09-18
- lots of refactoring to make the code easier to maintain
- support for transitioning from PsCARTHA to PsDELPH1
- a lot more logs (getting close to 100% coverage)
- better exit codes
- better exit-on-error messages
- add a few FIXME flags for future improvements

## 1.0.1 - 2020-09-16
- add automatic processing speed measurements in debug mode
- example: `# 1000 blocks processed in 23.054222 seconds (2602.560188/min) - 3000 since 58 seconds ago (3104.616143/min on average)`

## 1.0.0 - 2020-09-15
- [major version bump] DB alteration: spendable and delegatable flags in table `contract` are now nullable
- logs:
  - better: more structured, more parsable, more coherent
  - more: the goal is to have logs for every DB insertion or update
  - block hashes: print full hash for block creations, short hash for in-block operations
- makefile:
  - different rules for getting different versions of the SQL DB scheme, customize your DB by giving privilege to performance or to space usage, get more utility queries (from Mezos)
- add optional SQL index on autoid columns (only the one for operation_alpha's autoid column is activated by default)
- upgrade DB scheme (basic) testing using docker


## 0.9.1
- skipped: became 1.0.0

## 0.9 - 2020-09-11
- add option `--debug` (`--verbose` and `--debug` replace `--debug`), verbose doesn't imply debug, debug doesn't imply verbose
- refactoring logs
  - DB updates/inserts are logged with prefix `> `
  - debug logs are prefixed with `# `
- improve Makefile

## 0.8.1 - 2020-09-10
- fix indexing of snapshot blocks for delphinet
- factorize indexing snapshot blocks code using MPP

## 0.8 - 2020-09-10
- use Tezos v7.4
- support for delphinet
- add options `--delphinet`, `--mainnet`, `--carthagenet`

## 0.7
- add table `addresses` to factorize tz1 and KT1 addresses in the DB
- add columns `sender` and `receiver` in table `operation_alpha`
  - these columns allows to get basic information without looking into more specialized tables (e.g., table `tx`) and also give information about operations that don't have specialized tables

## 0.6
- add Makefile rules to generate SQL statements
- import db.sql from mezos (so that synchronising mezos with tezos-indexer becomes easier)

## 0.5
- [requires reindexing] fix table `implicit`

## 0.4
- indexing of `entrypoint`
- fix indexing of `big_map_diff`


## 0.3
- add autoid in operation_alpha

## 0.2
- more indexed information
- add basic DB scheme testing procedure using docker

## 0.1.9
- use tezos 7.3
  - upgrade dune build
  - upgrade .opam files
  - update Dockerfile


*Prior versions require too much archeology to be reported in this changelog.*
