[[_TOC_]]
# The Nomadic Labs `tezos-indexer`

## What is an "indexer"

Say you want to see the last 10 transactions for an account.
If you use [tezos RPCs](https://tezos.gitlab.io/alpha/rpc.html#rpcs-full-description)
to get the information, you'll have to query the blockchain's
blocks one by one, starting from the latest, until you find those 10 transactions.
If you wanted all transactions for that account, you'd have to browse the entire
blockchain.

The job of an "indexer" is to make such information easy and fast to get, and how it does
that is by *indexing* those information, hence its name.

Indexers are the backbones of block explorers, dApps, and wallets.

## How it works

This indexer connects to a running `tezos-node` in `archive mode`, queries blocks,
and puts them into a database.
We're using [PostgreSQL](https://www.postgresql.org/) to handle the database, a
popular engine, so that you can access the data using virtually any programming
language you want.

The source files for the database schema is available in
[src/db-schema](https://gitlab.com/nomadic-labs/tezos-indexer/blob/master/src/db-schema/)
but to get the full schema, you should run the following command:
```
make db-schema-all-default
```

or, if you have the binary, the following command will output the DB schema:
```
tezos-indexer --db-schema
```

or, from tezos-indexer v9.3.3's docker image:
```
docker run -it --rm registry.gitlab.com/nomadic-labs/tezos-indexer/tezos-indexer:v9.3.3 --db-schema
```
(replace v9.3.3 by the version you want to use)


## Tezos network supports

`tezos-indexer` supports the following networks:
* mainnet (≥v8.1.8)
* carthagenet (<v8.0.0 or ≥v9.1.0)
* delphinet
* edonet (≥v6.1.0 and <v8.1.7)
* edo2net (≥v8.1.8)
* ebetanet (v6.0.0 only)
* florencenobanet or florencenet with PsFLoren
* any network (private or sandbox) that uses mainnet's protocols and protocol transitions (including protocol PsFLoren and protocol transition from PtEdo2Zk to PsFLoren for ≥v9.1.0)

Support for abandoned or disavowed protocols (e.g., PtEdoTez) is not provided. But if you need those for some reason, please get in touch.

## Getting the data (i.e., running the indexer)

### `tezos-node` running in mode archive

You'll need a `tezos-node` running with `--history-mode=archive`, otherwise you
won't have anything to index!

And, unless you give the information on the command line,
you need to create an appropriate `~/.tezos-client/config` file,
which may look like this:

```
{ "base_dir": "/home/username/.tezos-client",
  "node_addr": "tezos-node-address",
  "node_port": 8732,
  "tls": false,
  "web_port": 8080,
  "confirmations": 0 }
```
Note that "`web_port`" is not used (you may give it any integer value, it won't matter).

*Once your indexer is fully synchronized with your node, you may
abandon the archive mode (`--history-mode=archive`) and use other
mode, although it's not recommended.*

### Set up a PostgreSQL database

First, install `postgresql` and create a database. It is recommended that you use Postgres 13, or 12.
Older versions may work but they're no longer tested.

If you need to create a DB user, `createuser username` may be what you want,
and perhaps even `createuser username -dw`.

Give DB ownership to the user running the indexer (or at least, the necessary read/write permissions).

So it should be something like after you have installed PostGres *and*
it's running.
```
createdb name_of_your_database -O username
```

If you decide to set up a password, it's up to you to provide it
via environment variable `PGPASSWORD` or by any other means that work.
*Some configurations of Postgres will require you to have a non-empty password. It's all up to you (or your Postgresql administrator).*

### Build the indexer

It's highly recommended that you simply use the
[Docker images built by the Gitlab CI](https://gitlab.com/nomadic-labs/tezos-indexer/container_registry/842779),
or built by yourself using the provided [Dockerfile](https://gitlab.com/nomadic-labs/tezos-indexer/-/blob/master/Dockerfile).

If you're okay with using docker images, skip to the next section.

However if you still want to proceed, here's how:

#### First, build tezos

Warning: it is quite straightforward when it works, but can easily become difficult.

The official documentation is there: https://tezos.gitlab.io/introduction/howtoget.html#install-tezos-dependencies

#### Second, get a copy of tezos-indexer

```
git clone https://gitlab.com/nomadic-labs/tezos-indexer.git
cd tezos-indexer
make tezos # this clones a patched version of tezos
```

#### Third, install tezos-indexer dependencies and build

```
make install-opam-deps
make
```

### Get the database schema

A simple way to get the schema is:
```
make db-schema-all-default | psql
```

If there's any error, `psql` will exit.

There are other ways to generate the DB schema, but they're undocumented.

All database schema source code is in `src/db-schema/`.


### Run the indexer for the blockchain

Release v9.1.0 made the discovery of protocols automatic, so you don't
need to specify which network you're using. Once you have started
indexing a network, that database will refuse to index other
networks. In other words, use one database per network.


Run the indexer using a docker image:
```
docker run -it --rm registry.gitlab.com/nomadic-labs/tezos-indexer/tezos-indexer:v9.3.3 --verbose --db=postgresql://my-pg-server.example.com/name_of_your_database
```

Run the indexer using your own binary:
```
./tezos-indexer --verbose --db=postgresql://my-pg-server.example.com/name_of_your_database
```

Note that although by default the indexer will try to connect to and use `postgresql://localhost/chain`,
you should probably not rely on any default values.

You should drop the `--verbose` option in order to gain speed or if you don't need the logs, or use `--verbosity=1` if you want to get some minimal logs.

Regarding mainnet, when protocol 5 (PsBabyM1) became active, all contracts were assigned a script (before protocol 5, it was optional).
Those scripts are now automatically added right after the indexer is done indexing blocks of protocol 4. That may take some time during which no block is being indexed and the node is being queried for those scripts.

For faster bootstrapping, which can be very handy for mainnet, cf. `utils/run-tezos-indexer-multicore.bash`.


To check where the indexer is at, you could do something like this:
```
$ psql name_of_your_database <<< 'select count(*) from c.block;'
 count
--------
 474987
(1 row)
```
If ever this gives you an error, it means you have issues with your Postgres
installation.

### Run the indexer for the mempool (new and young feature)

We have a feature that allows to record (with minimal indexing) the contents of the mempool.
When activated, this feature deactivates the indexing of the blockchain.
To index both the blockchain and the mempool, you need to run two instances
of `tezos-indexer`. Preferably, they should share the same database, so that
you may write more powerful requests to retrieve data. However, you may
run just one of them: you do not need to index the blockchain to index
the mempool, nor do you need to index the mempool to index the blockchain.

The mempool indexer is quite simple and minimal for now. The database schema is mainly in
`src/db-schema/mempool.sql` but to get the full database schema required
for indexing the mempool, you may run `make db-schema-mempool-solo`.

#### The mempool indexer works for protocols 7 (PsDELPH1), 8 (PtEdo2Zk) and 9 (PsFLoren)

```
./tezos-indexer --mempool-only
```

Note that it is expected that your node is synchronized with the blockchain.

### Building a block cache for the indexer

If you run `./tezos-indexer --help`, you'll see that the indexer may use a disk cache for getting the blocks
from a cache stored on disk instead of making RPCs to the node.
Doing so may allow you to use a slow (and/or distant) node. And even with a fast node, this might still help speed up the bootstrap.
Also, those *block cache* files can be stored in an HDD and it will not be extremely slow!
Each block cache file stores 1000 blocks. For the mainnet chain, at time of writing, you may
create about 1400+ cache files. But you may also use a partial cache.

To build the cache, you should use the `utils/get.bash` script (open it — it's quite simple — and make sure it points to the right URL for the node).

### Issues

Please feel free to [open an issue](https://gitlab.com/nomadic-labs/tezos-indexer/-/issues).

### Precisions

* The "bootstrap" consists in processing the blocks that are known to
already exist: when you start the indexer, it retrieves the current
level (L) from the tezos node, and it'll process all the blocks it
doesn't have between block 0 and L (note that blocks 0 and 1 are expected not to contain any operations).

* When you launch the indexer, the first thing it will do is to build
the list of snapshot blocks (a table that associates cycles with the
selected snapshot blocks).
Then it will bootstrap.
Then it will watch.

* "Watching" means the indexer gets the blocks tezos-node feeds it.

* Blockchain reorganisations: when they happen, all blocks from lowest level of rejected blocks are deleted and indexing resumes from there.


Performance
-----------

On a Intel NUC 10, with a Core i7-10710U, 16GB RAM DDR4-2666MHz and a very fast NVME
SSD, indexing the first 1.32 M blocks of mainnet may take as little as
1h30min if you use `utils/run-tezos-indexer-multicore.bash`.
That includes all the data coming directly from blocks, and FA1.2 tokens. That does not include data coming from contexts (such as context-level contract balances).

That scripts makes the indexer write into the DB with a O(1)
complexity for any given block instead of O(log n) where n is the
quantity of already indexed blocks, and allows you to take advantage
of multicore architectures by running multiple indexers, each treating a segment of the blockchain.

If your node is slow and/or far, it's highly recommended that you build a cache.

#### Distance to DB engine

The distance from the indexer to the DB engine can have quite a huge impact on the speed of the bootstrap process.

With v9+, the distance to the DB engine can however be compensated by running multiple indexers (each indexing a segment of the blockchain).

#### Blocks per SQL transaction

`--sql-rate` allows to specify the number of blocks you want to be in a single SQL transaction. Default is 1. And 0 means that you don't want SQL transactions at all.
It would seem that a number between 300 and 1000 allows some speed up, compared to 1.
Increasing that number likely makes increase the memory footprint.
Also, if the indexer process is killed (on purpose or by accident), having a larger number will result on average on larger loss of data:
the processed blocks that haven't been committed in the current SQL transaction will be dropped.
Having `--sql-rate=0` is not recommended because it means that in case a block is only partially indexed, interrupting the process will leave a partially indexed block in the DB.

#### Skipping non-required data

Since v8, SQL write queries are all in SQL functions. Those functions may be replaced by dummy functions if you want some of the writes to be skipped. Or by some functions of your craft to perform differently.
You may pick what you need from `src/db-schema/feign-recording.sql`. Be sure to pick a set that functionally makes sense!
For instance, if table A references table B, and you skip the writes into table A without skipping writes into table B, it will probably crash.


#### Verbosity

Logs obtained by using `--debug --verbose --verbosity=10` are maximal.
They have a cost. Bootstrapping will be slower with those options than without any option.
If you want minimal non-nil logs you should use one of:
- `--verbose --verbosity=1`
- `--debug --verbosity=1`
- `--debug --verbose --verbosity=1`

#### Versions of postgresql

- we use postgresql 13 (and sometimes 12)
- anterior versions might work but are not supported

Hardware requirements
---------------------

### Storage

Version 9 changes the SQL schema to compact the data, so the first 1.32 million blocks of mainnet take less than 50 GB on the disk (instead of about more than 200GB with v8).

Indexing a live blockchain means that the quantity of data just keeps increasing.

Of course, you'll need more space if you also index the mempool (old mempool data is not automatically deleted, but you can easily write a script that purges old mempool data or use a hook to the mempool table).

That doesn't count the space you may need for the node, or for caching the blocks.


### RAM

Recommended minimum is 8GB.
It will likely run with less than 4GB, or even less if the node and/or the postgresql server are not on the same machine.

We run our tests on machines equiped with 16GB, 32GB or 64GB of RAM.

But then, of course, the more you have the merrier, since your system will have more room for caching.

### CPU

Any reasonable 2GHz+ CPU should be fine, preferably multicore.

However v9 can now take advantage of multicore CPUs for the bootstrap phase.


##  Work in progress / Future work

cf. https://gitlab.com/nomadic-labs/tezos-indexer/-/issues


Misc commands that might be helpful
-----------------------------------

Peek at the latest blocks
```bash
$ psql name_of_your_database <<< 'select * from c.block order by level desc limit 10;'
```

Reset the chain database
```bash
# as root
$ su - postgres -c 'dropdb name_of_your_database && createdb name_of_your_database -O username'
```

## Contributions

Bug reports and merge requests are welcome.

## Issues or questions

You're very welcome to [raise issues if you have any](https://gitlab.com/nomadic-labs/tezos-indexer/issues) and/or to [submit merge requests](https://gitlab.com/nomadic-labs/tezos-indexer/merge_requests).

## Contact

You're welcome to create issues or merge requests.
You may also contact [Philippe Wang](@philippewang.info) by other means (email, slack,
messenger, sms, telegram, linkedin, etc.) as long as it works.
You may also contact any member of Nomadic Labs.
And if you want your name added here, please submit a merge request! ;)

# `tezos-indexer --help`

The output of `tezos-indexer --help` follows:

<!-- --help -- do not manually edit this or after this line -->
```
NAME
       tezos-indexer - Store and index a Tezos blockchain into a Postgres
       database.

SYNOPSIS
       tezos-indexer [OPTION]... 

       --help[=FMT] (default=auto)
           Show this help in format FMT. The value FMT must be one of `auto',
           `pager', `groff' or `plain'. With `auto', the format is `pager` or
           `plain' whenever the TERM env var is `dumb' or undefined.

OPTIONS
       --block-cache-directory=VAL (absent=./cache/)
           Directory from which to read cached blocks.

       --bootstrap-upto=VAL (absent=-1)
           Bootstrap until it's over or until given level is reached
           (whichever comes first), then exit. Negative values mean no limit.

       --contract-balances-only
           Only fill balances in table contract_balances.

       --db=URL (absent=postgresql://localhost/chain)
           Database to connect to.

       --db-schema
           Print default full DB schema and exit.

       --db-schema-multicore
           Print "multicore" full DB schema and exit. Do not forget to
           convert to default schema after bootstrap is over.

       --debug
           Debug mode.

       --first-block=VAL (absent=2)
           Specify level of first alpha block

       --force-from=VAL (absent=0)
           Force indexing from specified level. The purpose of this option is
           to start indexing from a level different from the latest indexed
           block. The first purpose is to be able to easily reindex blocks
           that have already been indexed, but which for some reason might
           need an update. You may index from an arbitrary high level only if
           you are also using `--no-watch` and the DB is in multicore mode.
           Also, you cannot start indexing from a level that doesn't exist. A
           negative value is considered as an offset (towards the past) from
           the highest block recorded in the DB.

       --mempool-only
           Index mempool ONLY: every other indexing activity will be
           disabled. You may run those in another process.

       --no-check-db-schema-version
           Do not check DB schema version (not recommended)

       --no-contract-balances
           Don't fill balances in table contract_balances.

       --no-snapshots
           Deactivate snapshot blocks.

       --no-watch
           No watch: exit once bootstrap is done.

       --rejected-blocks-depth=VAL (absent=100)
           How far in the past to look for rejected blocks (happens at every
           new block during the watch process). Default value is 100, and 0
           means not to look at all.

       --skip-prebaby-contracts
           Skip indexing pre-babylon contracts

       --snapshots
           Get and update snapshot blocks ONLY (forever).

       --sql-rate=VAL (absent=1)
           Number of blocks per SQL transaction, during bootstrap. Use 0 to
           deactivate SQL transactions.

       --tezos-client-dir=PATH (absent=/home/phil/.tezos-client)
           Where Tezos client config resides

       --tezos-url=URL
           URL of a running Tezos node. Will use http://localhost:8732 if no
           URL is specified and --tezos-client-dir isn't used either. Note
           that --tezos-url has precedence over --tezos-client-dir.

       --threads=VAL (absent=1)
           Number of threads for bootstrapping mode (>1 is experimental,
           requires DB in multicore mode, and implies --no-watch).

       --tokens-support
           Enable FA1.2 tokens indexing.

       --use-disk-cache
           Use disk cache for reading blocks.

       --verbose
           Verbose mode.

       --verbosity=VAL (absent=5)
           Verbosity level (requires --verbose and/or --debug, otherwise will
           have no effect). 0: some logs. 1: block level. 2: operation level.
           3: sub-operation level. 4: more logs. 5: even more logs!

       --version
           Print version and exit.

       --watch-hook=VAL
           A command to execute after a new block is indexed during the watch
           process. If a reorganization happens, the hook will be executed
           after the reorganization is over.

```
