-- Open Source License
-- Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

SELECT 'mezos_tokens.sql' as file;

CREATE OR REPLACE FUNCTION get_token_contracts ()
RETURNS TABLE(address char(36))
AS $$
select address(c.address_id)
from t.contract c
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_token_transfer (address varchar, token varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
caller char,
tz_amount bigint,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
action_source char,
action_destination char,
action_amount numeric,
action_callback char
)
AS $$
with a as (select address_id(address) as ddress)
select 'transfer',
       oa.autoid, -- id
       b.level, -- level
       b.timestamp, -- timestamp
       block_hash(b.hash_id),  -- block
       op.hash, -- hash
       address, -- caller
       t.amount, -- tz_amount
       t.fee, -- fees
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       address(tktx.source_id), -- source (tktx)
       address(tktx.destination_id), -- destination (tktx)
       tktx.amount, -- amount (tktx)
       null -- callback (tktx)
from
       C.operation_alpha oa,
       C.operation op,
       T.operation tkop,
       T.transfer tktx,
       C.block b,
       C.manager_numbers m,
       C.tx t,
       a
where
       (tktx.source_id = a.ddress OR tktx.destination_id = a.ddress)
       and tkop.token_address_id = address_id(token)
       and tkop.operation_id = t.operation_id
       and oa.autoid = t.operation_id
       and op.hash_id = oa.hash_id
       and b.hash_id = op.block_hash_id
       and m.operation_id = oa.autoid
       and tkop.kind = 'transfer'
       and tktx.operation_id = tkop.operation_id
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION get_token_approve (address varchar, token varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
caller char,
tz_amount bigint,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
action_source char,
action_destination char,
action_amount numeric,
action_callback char
)
AS $$
with a as (select address_id(address) as ddress)
select 'approve',
       oa.autoid, -- id
       b.level, -- level
       b.timestamp, -- timestamp
       block_hash(b.hash_id),  -- block
       op.hash, -- hash
       address, -- caller
       t.amount, -- tz_amount
       t.fee, -- fees
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       address(tkapp.address_id), -- source (tkapp)
       null, --
       tkapp.amount, -- amount (tkapp)
       null --
from
       C.operation_alpha oa,
       C.operation op,
       T.operation tkop,
       T.approve tkapp,
       C.block b,
       C.manager_numbers m,
       C.tx t,
       a
where
       (tkapp.address_id = a.ddress OR tkop.caller_id = a.ddress)
       and tkop.token_address_id = token_address_id
       and tkop.operation_id = t.operation_id
       and t.operation_id = oa.autoid
       and op.hash_id = oa.hash_id
       and b.hash_id = op.block_hash_id
       and m.operation_id = oa.autoid
       and tkop.kind = 'approve'
       and tkapp.operation_id = tkop.operation_id
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION get_token_get_balance (address varchar, token varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
caller char,
tz_amount bigint,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
action_source char,
action_destination char,
action_amount numeric,
action_callback char
)
AS $$
with a as (select address_id(address) as ddress)
select 'getBalance',
       oa.autoid, -- id
       b.level, -- level
       b.timestamp, -- timestamp
       block_hash(b.hash_id),  -- block
       op.hash, -- hash
       address, -- caller
       t.amount, -- tz_amount

       t.fee, -- fees
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       address(tkbal.address_id), -- address (tkbal)
       null, --
       cast(null as numeric), --
       address(tkbal.callback_id) -- amount (tkbal)
from
       C.operation_alpha oa,
       C.operation op,
       T.operation tkop,
       T.get_balance tkbal,
       C.block b,
       C.manager_numbers m,
       C.tx t,
       a
where
       (tkbal.address_id = a.ddress OR tkop.caller_id = a.ddress)
       and tkop.token_address_id = address_id(token)
       and t.operation_id = oa.autoid
       and op.hash_id = oa.hash_id
       and b.hash_id = op.block_hash_id
       and m.operation_id = oa.autoid
       and tkop.operation_id = t.operation_id
       and tkop.kind = 'getBalance'
       and tkbal.operation_id = tkop.operation_id
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION get_token_get_allowance (address varchar, token varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
caller char,
tz_amount bigint,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
action_source char,
action_destination char,
action_amount numeric,
action_callback char
)
AS $$
with a as (select address_id(address) as ddress)
select 'getAllowance',
       oa.autoid, -- id
       b.level, -- level
       b.timestamp, -- timestamp
       block_hash(b.hash_id),  -- block
       op.hash, -- hash
       address, -- caller
       t.amount, -- tz_amount
       t.fee, -- fees
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       address(tkalw.source_id), -- source (tkalw)
       address(tkalw.destination_id), -- destination (tkalw)
       cast(null as numeric), --
       address(tkalw.callback_id) -- amount (tkalw)
from
       C.operation_alpha oa,
       C.operation op,
       T.operation tkop,
       T.get_allowance tkalw,
       C.block b,
       C.manager_numbers m,
       C.tx t,
       a
where
       (tkalw.source_id = a.ddress
       OR tkalw.destination_id = a.ddress
       OR tkop.caller_id = a.ddress)
       and tkop.token_address_id = address_id(token)
       and t.operation_id = oa.autoid
       and op.hash_id = oa.hash_id
       and b.hash_id = op.block_hash_id
       and m.operation_id = oa.autoid
       and tkop.operation_id = t.operation_id
       and tkop.kind = 'getAllowance'
       and tkalw.operation_id = tkop.operation_id
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION get_token_get_total_supply (address varchar, token varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
caller char,
tz_amount bigint,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
action_source char,
action_destination char,
action_amount numeric,
action_callback char
)
AS $$
with a as (select address_id(address) as ddress)
select 'getTotalSupply',
       oa.autoid, -- id
       b.level, -- level
       b.timestamp, -- timestamp
       block_hash(b.hash_id),  -- block
       op.hash, -- hash
       address, -- caller
       t.amount, -- tz_amount
       t.fee, -- fees
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       null, --
       null, --
       cast(null as numeric), --
       address(tkts.callback_id) -- amount (tkbal)
from
       C.operation_alpha oa,
       C.operation op,
       T.operation tkop,
       T.get_total_supply tkts,
       C.block b,
       C.manager_numbers m,
       C.tx t,
       a
where
       (tkop.caller_id = a.ddress)
       and tkop.token_address_id = address_id(token)
       and t.operation_id = oa.autoid
       and op.hash_id = oa.hash_id
       and b.hash_id = op.block_hash_id
       and m.operation_id = oa.autoid
       and tkop.operation_id = t.operation_id
       and tkop.kind = 'getTotalSupply'
       and tkts.operation_id = tkop.operation_id
$$ LANGUAGE SQL;
