-- Open Source License
-- Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
-- Copyright (c) 2021 Rémy El Sibaïe <remy@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.



CREATE OR REPLACE FUNCTION get_alpha_infos (op_id bigint)
  RETURNS TABLE(
    hash character,
    id smallint,
    block_hash char,
    op_timestamp timestamp,
    level integer
  ) as
  $$
  select o.hash, oa.id, bh.hash, b.timestamp, b.level
  from c.operation_alpha oa
  join c.operation o on oa.hash_id = o.hash_id
  join c.block_hash bh on oa.block_hash_id = bh.hash_id
  join c.block b on bh.hash_id = b.hash_id
  where oa.autoid = op_id
$$ language sql;

CREATE OR REPLACE FUNCTION get_address (id bigint)
  RETURNS TABLE(hash character(36)) as
$$
  select address from c.addresses where address_id = id
$$ language sql;

-- Such a value contains specific informations about the manager operation
-- kind should be 'transaction', 'delegation', 'reveal', 'origination'
DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'manager_data') THEN
      CREATE TYPE manager_data AS (
        operation_id bigint
      , kind text
      , source_hash character
      , fee bigint
      , status smallint
      , data jsonb
    );
    END IF;
END $$;


-- Returns all the transfers where one address from the given set is
-- source or destination
CREATE OR REPLACE FUNCTION get_tx_no_token(address_hashes character[])
  RETURNS setof manager_data as $$

  select tr.operation_id, 'transaction', coalesce(src.hash, adds.address)
  , tr.fee
  , tr.status
  , jsonb_build_object
  ( 'amount', tr.amount
  , 'token', false
  , 'destination', coalesce(dest.hash, adds.address)
  , 'parameters', tr.parameters
  , 'entrypoint', tr.entrypoint
  )
  from unnest(address_hashes) hash
  join c.addresses adds on adds.address = hash
  join c.tx tr on address_id = source_id or destination_id = address_id
  left join get_address(destination_id) dest on address_id = source_id
  left join get_address(source_id) src on address_id = destination_id
  -- Are filtered out from transfers the one fetched
  -- from token transfers. It allows the union all
  where not exists (select 1 from t.transfer where operation_id = tr.operation_id)

  $$ language sql;


-- Returns all the token transfers where one address from the given set is
-- source or destination
CREATE OR REPLACE FUNCTION get_tx_token(address_hashes character[])
  RETURNS setof manager_data as $$

  select tk.operation_id, 'transaction', coalesce(src.hash, adds.address)
  , tr.fee
  , tr.status
  , jsonb_build_object
  ( 'amount', tk.amount
  , 'token', true
  , 'destination', coalesce(dest.hash, adds.address)
  , 'contract', tk_add.hash
  )
  from unnest(address_hashes) hash
  join c.addresses adds on adds.address = hash
  join t.transfer tk on address_id = source_id or destination_id = address_id
  join t.operation t_op using (operation_id)
  join get_address(t_op.token_address_id) tk_add on true
  join c.tx tr using (operation_id)
  left join get_address(tk.destination_id) dest on address_id = tk.source_id
  left join get_address(tk.source_id) src on address_id = tk.destination_id
$$ language sql;

-- Returns all the reveals where one address from the given set is the source
CREATE OR REPLACE FUNCTION get_all_reveals(address_hashes character[])
  RETURNS setof manager_data as $$

  select r.operation_id, 'reveal', adds.address, r.fee, r.status, null::jsonb
  from unnest(address_hashes) hash
  join c.addresses adds on adds.address = hash
  join c.reveal r on address_id = source_id

$$ language sql;

-- Returns all the delegations where one address from the given set is the source
-- or the delegate pkh
CREATE OR REPLACE FUNCTION get_all_delegations(address_hashes character[])
  RETURNS setof manager_data as $$

  select r.operation_id, 'delegate', coalesce(src.hash, adds.address)
  , r.fee
  , r.status
  , jsonb_build_object('delegate', coalesce(pkh.hash, adds.address))
  from unnest(address_hashes) hash
  join c.addresses adds on adds.address = hash
  join c.delegation r on address_id = source_id or address_id = pkh_id
  left join get_address(pkh_id) pkh on address_id = source_id
  left join get_address(source_id) src on address_id = pkh_id

$$ language sql;

-- Returns all the originations where one address from the given set is the source
CREATE OR REPLACE FUNCTION get_all_originations(address_hashes character[])
  RETURNS setof manager_data as $$

  select r.operation_id, 'origination', adds.address, r.fee
  , r.status
  , jsonb_build_object
    ( 'contract', kt.hash
    , 'storage_size', r.storage_size
    , 'paid_storage_size_diff', r.paid_storage_size_diff
  )
  from unnest(address_hashes) hash
  join c.addresses adds on adds.address = hash
  join c.origination r on address_id = source_id
  left join get_address(r.k_id) kt on true

$$ language sql;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'full_operation') THEN
      CREATE TYPE full_operation AS (
        hash character
        , id smallint
        , block_hash char
        , op_timestamp timestamp
        , level integer
        , kind text
        , source_hash character
        , status char
        , fee bigint
        , data jsonb
        , counter numeric
        , gas_limit numeric
        , storage_limit numeric
      );
    END IF;
END $$;

CREATE OR REPLACE FUNCTION get_mempool_token_data(data jsonb)
  RETURNS jsonb as $$

  select data->'parameters'->'value'->'args'->1->'args';

$$ language sql;

CREATE OR REPLACE FUNCTION get_mempool_token_dest(data jsonb)
  RETURNS text as $$

  select get_mempool_token_data(data)->0->>'string';

$$ language sql;

CREATE OR REPLACE FUNCTION get_mempool_token_amount(data jsonb)
  RETURNS text as $$

  select get_mempool_token_data(data)->1->>'int';

$$ language sql;

CREATE OR REPLACE FUNCTION mempool_oa_to_data(destination character, dest_is_token bool, data jsonb)
  RETURNS jsonb as $$

  select case
         when data->>'kind' = 'transaction' and dest_is_token then
           jsonb_build_object
           ( 'amount', get_mempool_token_amount(data)
           , 'token', true
           , 'destination', get_mempool_token_dest(data)
           )
         when data->>'kind' = 'transaction' and not dest_is_token then
           jsonb_build_object
           ( 'amount', cast(data->>'amount' as bigint)
           , 'token', false
           , 'destination', destination
           , 'parameters', data->'parameters'->>'value'
           , 'entrypoint', data->'parameters'->>'entrypoint'
           )
         when data->>'kind' = 'delegation' then jsonb_build_object ()
         when data->>'kind' = 'reveal' then jsonb_build_object ()
         when data->>'kind' = 'origination' then jsonb_build_object ()
         else jsonb_build_object ()
  end

$$ language sql;

CREATE OR REPLACE FUNCTION get_manager_operations_on_mempool(address_hashes character[], only_kinds character[])
  RETURNS setof full_operation as $$

  select oa.hash, oa.id
  , null as block_hash
  , to_timestamp(first_seen_timestamp)::timestamp without time zone
  , first_seen_level
  , oa.operation_alpha->>'kind'
  , oa.source
  , status::char
  , cast(oa.operation_alpha->>'fee' AS bigint)
  , mempool_oa_to_data(destination, tkc.address_id is not null, oa.operation_alpha)
  , cast(oa.operation_alpha->>'counter' AS numeric)
  , cast(oa.operation_alpha->>'gas_limit' AS numeric)
  , cast(oa.operation_alpha->>'storage_limit' AS numeric)
  from unnest(address_hashes) addr
  join m.operation_alpha oa on destination = addr or source = addr
  left join c.addresses adds on adds.address = oa.destination
  left join t.contract tkc on adds.address_id = tkc.address_id
  where cardinality(only_kinds) = 0 or oa.operation_alpha->>'kind' = any(only_kinds)

$$ language sql;

-- Returns every manager operation where the given set of address is either
-- source of destination. This is basically a gathering of all functions above.
-- The previous functions return disjoint sets, so union all is pretty efficient there.
-- The current function also gather common infos, like manager numbers, ids and so on.
CREATE OR REPLACE FUNCTION get_manager_operations(address_hashes character[], only_kinds character[], lim int, ofs int)
  RETURNS setof full_operation as $$

  select * from
    ((select infos.*, r.kind, r.source_hash, status(r.status), r.fee, r.data
           , mn.counter, mn.storage_limit, mn.gas_limit
      from (
        (select * from get_tx_no_token(address_hashes) txs
           where cardinality(only_kinds) = 0 or 'transaction' = any(only_kinds)
           order by operation_id limit lim offset ofs)
         union all
        (select * from get_tx_token(address_hashes)
         where cardinality(only_kinds) = 0 or 'transaction' = any(only_kinds)
         order by operation_id
          limit lim offset ofs)
         union all
        (select * from get_all_delegations(address_hashes)
         where cardinality(only_kinds) = 0 or 'delegation' = any(only_kinds)
          order by operation_id
          limit lim offset ofs)
         union all
        (select * from get_all_reveals(address_hashes)
         where cardinality(only_kinds) = 0 or 'reveal' = any(only_kinds)
         order by operation_id
          limit lim offset ofs)
         union all
        (select * from get_all_originations(address_hashes)
         where cardinality(only_kinds) = 0 or 'origination' = any(only_kinds)
         order by operation_id
          limit lim offset ofs)
      ) r
      join C.manager_numbers mn on mn.operation_id = r.operation_id
      join get_alpha_infos(r.operation_id) infos on true
      order by infos.op_timestamp
       limit lim offset ofs)
     union all
    (select * from get_manager_operations_on_mempool(address_hashes, only_kinds) mmo
     where not exists (select 1 from c.operation op where op.hash = mmo.hash)
           order by op_timestamp limit lim)
  ) r
  order by op_timestamp limit lim offset ofs
$$ language sql;

  -- select count(*)
  --   from get_manager_operations(
  --     ARRAY['tz1MirJ1tHvjTt8CjxH9nox4XSmm2Le5AQQj']
  --     , ARRAY['delegation']::text[], 100, 10
  --   );

  -- explain analyze select * from get_manager_operations_on_mempool(ARRAY['tz1MirJ1tHvjTt8CjxH9nox4XSmm2Le5AQQj'], Array[]::text[]) mmo
  --  where not exists (select 1 from c.operation op where op.hash = mmo.hash)
  --  order by op_timestamp limit 10;

  -- explain analyze select op_timestamp, source_hash, data
  --   from get_manager_operations(ARRAY['tz1UBwnNSFrvbPmtVT4vHCsba3mjMGtKhpu2', 'tz1Rrc4XieKdAqhLkYHGJUeJGGjZWMfx7qgC'
  --                                     , 'tz1MirJ1tHvjTt8CjxH9nox4XSmm2Le5AQQj']
  --                               , ARRAY[]::text[], 10, 0
  --   );

  -- tz1MirJ1tHvjTt8CjxH9nox4XSmm2Le5AQQj


-- drop function get_tx_no_token;
-- drop function get_tx_token;
-- drop function get_address;
-- drop function get_alpha_infos;
-- drop function get_all_reveals;
-- drop function get_all_delegations;
-- drop function get_all_originations;
-- drop function get_manager_operations_on_mempool;
-- drop function get_manager_operations;
-- drop function mempool_oa_to_data;
-- drop function get_mempool_token_dest;
-- drop function get_mempool_token_data;
-- drop function get_mempool_token_amount;
-- drop type manager_data;
-- drop type full_operation;
