-- Open Source License
-- Copyright (c) 2019-2021 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-----------------------------------------------------------------------------
-- Naming conventions:
-- - for functions:
--   * I.table -> insert into table
--   * U_table -> update table
--   * IU_table -> insert or update table (aka upsert)
--   * u_concept -> update more than one table
--   * B_action -> action on bigmaps
--   * BEWARE: upper/lower cases for prefixes are only for aesthetic purposes!
--     Function names are case-insensitive!
-----------------------------------------------------------------------------

SELECT 'chain_functions.sql' as file;


CREATE OR REPLACE FUNCTION I.chain(c char)
RETURNS void
AS $$
BEGIN
insert into C.chain(hash) values (c)
on conflict do nothing;
if (select count(*) from C.chain) <> 1
then
  raise 'You are trying to index a chain on a other chain‘s database.';
end if;
end;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION I.block_hash_aux(bh char, bhid int)
RETURNS int
AS $$
insert into C.block_hash values (bh, bhid) on conflict do nothing returning hash_id;
$$ language SQL;

CREATE OR REPLACE FUNCTION I.block(bh char, l int, p int, pr char, t timestamp, vp smallint, m char, f char, c char)
RETURNS int
AS $$
DECLARE bhid int := null;
BEGIN
bhid := (select coalesce(I.block_hash_aux(bh, l), (select hash_id from C.block_hash where hash = bh)));
if bhid is null --SEQONLY
then --SEQONLY
  bhid := (select coalesce(I.block_hash_aux(bh, -l), (select hash_id from C.block_hash where hash = bh))); --SEQONLY
  if bhid is null --SEQONLY
  then --SEQONLY
    bhid := (select coalesce(I.block_hash_aux(bh, -l+(random()*10000)::int), (select hash_id from C.block_hash where hash = bh))); --SEQONLY
    if bhid is null --SEQONLY
    then --SEQONLY
      raise 'Failed to record block %', bh; --SEQONLY
    end if; --SEQONLY
  end if; --SEQONLY
end if; --SEQONLY
insert into C.block values (bhid, l, p, block_hash_id(pr), t, vp, m, f, c) --SEQONLY
on conflict do nothing --SEQONLY
; --SEQONLY
insert into C.block values (l, l, p, l-1, t, vp, m, f, c);--MULTICORE
return bhid;
END;
$$ LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION I.block0(bh char, l int, p int, pr char, t timestamp, vp smallint, m char, f char, c char)
RETURNS int
AS $$
-- pr is unused but it might be better if we make sure that (pr=bh)
insert into C.block_hash values (bh, l) on conflict do nothing;
-- the following insert will fail if bh≠pr, unless for some reason pr was inserted before
insert into C.block values (block_hash_id(bh), l, p, block_hash_id(pr), t, vp, m, f, c)
on conflict do nothing --conflict
;
select block_hash_id(bh);
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION confirm_block(bhid int, depth smallint)
RETURNS void
AS $$
update C.block set indexing_depth = depth where hash_id = bhid; --SEQONLY
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION record_log (msg text)
RETURNS void
AS $$
insert into indexer_log values (CURRENT_TIMESTAMP, '', '', msg) on conflict do nothing;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION max_level()
RETURNS int
AS $$
select level from C.block
order by level desc limit 1;
$$ LANGUAGE SQL stable;

CREATE OR REPLACE FUNCTION I.operation_aux(h char, b int, hi bigint)
RETURNS bigint
AS $$
insert into C.operation (hash, block_hash_id, hash_id)
values (h, b, hi)
on conflict do nothing
returning hash_id;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION I.operation(h char, b int, hi bigint)
RETURNS bigint
AS $$
DECLARE r bigint := null;
BEGIN
r := I.operation_aux(h, b, hi);
if r is not null
then
  return r;
else
  r := (select hash_id from C.operation where hash = h and block_hash_id = b);
  return r;
end if;
END
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION I.operation(h char, b int, hi bigint) --MULTICORE
RETURNS bigint --MULTICORE
AS $$ --MULTICORE
insert into C.operation (hash, block_hash_id, hash_id) --MULTICORE
values (h, b, hi) --MULTICORE
returning hash_id; --MULTICORE
$$ LANGUAGE SQL; --MULTICORE


CREATE OR REPLACE FUNCTION I.block_alpha (bhid int, baker bigint, level_position int, cycle int, cycle_position int, voting_period jsonb, voting_period_position int, voting_period_kind smallint, consumed_milligas numeric)
returns void
as $$
insert into C.block_alpha
values (bhid, baker, level_position, cycle, cycle_position, voting_period, voting_period_position, voting_period_kind, consumed_milligas)
on conflict do nothing --CONFLICT
;
$$ language sql;


CREATE OR REPLACE FUNCTION I.opalpha (ophid bigint, opid smallint, opkind smallint, bhid int, i smallint, a bigint)
returns void
as $$
insert into C.operation_alpha(hash_id, id, operation_kind, block_hash_id, internal, autoid)
values (ophid, opid, opkind, bhid, i, a)
on conflict do nothing --CONFLICT
$$ language sql;


CREATE OR REPLACE FUNCTION I.deactivated (pkhid bigint, bhid int)
returns void
as $$
insert into C.deactivated (pkh_id, block_hash_id) values (pkhid, bhid)
on conflict do nothing  --CONFLICT
$$ language sql;


CREATE OR REPLACE FUNCTION I.activate (opaid bigint, pkhid bigint, ac char)
returns void
as $$
insert into C.activation(operation_id, pkh_id, activation_code)
values (opaid, pkhid, ac)
on conflict do nothing; --CONFLICT
$$ language sql;


CREATE OR REPLACE FUNCTION I.proposal (opaid bigint, i bigint, s bigint, period int, proposal char)
RETURNS VOID
AS $$
insert into C.proposals values (proposal, i) on conflict do nothing;
insert into C.proposal values (opaid, s, period, proposal_id(proposal))
on conflict do nothing --CONFLICT
;
insert into C.operation_sender_and_receiver values (opaid, s, null)
on conflict do nothing; --CONFLICT
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION I.proposal2 (opaid bigint, i bigint, s bigint, period int, proposal char)
RETURNS VOID
AS $$
-- the only difference with I.proposal is that this one does not create an entry in C.operation_sender_and_receiver because we know there already is one
insert into C.proposals values (proposal, i) on conflict do nothing;
insert into C.proposal values (opaid, s, period, proposal_id(proposal))
on conflict do nothing --CONFLICT
;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION I.ballot (opaid bigint, i bigint, s bigint, period int, proposal char, ballot one_ballot)
RETURNS VOID
AS $$
-- in non-multicore mode, there's no vote for proposals that are unknown
-- in multicore mode, we can be recording ballots for proposals that haven't been recorded yet
insert into C.proposals values (proposal, i) on conflict do nothing; --MULTICORE
insert into C.ballot
values (opaid, s, period, proposal_id(proposal), ballot)
on conflict do nothing --CONFLICT
;
insert into C.operation_sender_and_receiver values (opaid, s, null)
on conflict do nothing; --CONFLICT
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION I.double_endorsement(opaid bigint, baker_id bigint, offender_id bigint, xop1 text, xop2 text)
RETURNS void
AS $$
DECLARE err text;
BEGIN
  BEGIN
    INSERT INTO C.double_endorsement_evidence (operation_id, op1, op2) VALUES (opaid, xop1::jsonb, xop2::jsonb)
    ON CONFLICT DO NOTHING --CONFLICT
    ;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
        INSERT INTO C.double_endorsement_evidence (operation_id, op1_, op2_) VALUES (opaid, xop1::json, xop2::json)
        ON CONFLICT DO NOTHING --CONFLICT
        ;
      ELSE
        RAISE EXCEPTION 'Error: %', err;
      END IF;
  END;
  INSERT INTO C.operation_sender_and_receiver VALUES (opaid, baker_id, offender_id)
  ON CONFLICT DO NOTHING  --CONFLICT
  ;
END;
$$ LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION I.double_baking(opaid bigint, xbh1 text, xbh2 text, baker_id bigint, offender_id bigint)
RETURNS void
AS $$
DECLARE err text;
BEGIN
  BEGIN
    INSERT INTO C.double_baking_evidence (operation_id, bh1, bh2) VALUES (opaid, xbh1::jsonb, xbh2::jsonb)
    ON CONFLICT DO NOTHING --CONFLICT
    ;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
        INSERT INTO C.double_baking_evidence (operation_id, bh1_, bh2_) VALUES (opaid, xbh1::json, xbh2::json)
        ON CONFLICT DO NOTHING --CONFLICT
        ;
      ELSE
        RAISE EXCEPTION 'Error: %', err;
      END IF;
  END;
  INSERT INTO C.operation_sender_and_receiver VALUES (opaid, baker_id, offender_id)
  ON CONFLICT DO NOTHING --CONFLICT
  ;
END;
$$ LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION I.manager_numbers (opaid bigint, counter numeric, gas_limit numeric, storage_limit numeric)
returns void
as $$
insert into C.manager_numbers
values (opaid, counter, gas_limit, storage_limit)
on conflict do nothing --CONFLICT
;
$$ language sql;


CREATE OR REPLACE FUNCTION I.endorsement(opaid bigint, level int, del bigint, sl smallint[])
returns void
as $$
insert into C.endorsement values (opaid, level, del, sl)
on conflict (operation_id) do nothing --CONFLICT
;
insert into C.operation_sender_and_receiver values (opaid, del, null)
on conflict do nothing --CONFLICT
;
$$ language sql;

CREATE OR REPLACE FUNCTION I.endoslot(opaid bigint, del bigint, sl smallint[], level int, slot smallint)
returns void
as $$
insert into C.endorsement values (opaid, level, del, sl, slot)
on conflict (operation_id) do nothing --CONFLICT
;
insert into C.operation_sender_and_receiver values (opaid, del, null)
on conflict do nothing --CONFLICT
;
$$ language sql;


CREATE OR REPLACE FUNCTION I.seed_nonce (opaid bigint, sender_id bigint, baker_id bigint, l int, n char)
returns void
as $$
insert into C.seed_nonce_revelation (operation_id, level, nonce)
values (opaid, l, n)
on conflict do nothing --CONFLICT
;
insert into C.operation_sender_and_receiver values (
  opaid
, sender_id
, baker_id
)
on conflict do nothing --CONFLICT
;
$$ language sql;


CREATE OR REPLACE FUNCTION I.snapshot (c int, l int)
returns void
as $$
insert into C.snapshot
values (c, l)
on conflict do nothing;
$$ language sql;


-- This is only for updating the script on mainnet after transitioning to Babylon
CREATE OR REPLACE FUNCTION U.script (kid bigint, xscript text, bhid int, xstrings text[])
RETURNS void
AS $$
DECLARE err text;
BEGIN
  UPDATE C.contract_script set script = xscript::jsonb, block_hash_id = bhid, strings = xstrings where address_id = kid and (script is null AND script_ is null);
  EXCEPTION WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
    IF err = 'unsupported Unicode escape sequence'
    THEN
      UPDATE C.contract_script set script_ = xscript::json, block_hash_id = bhid, strings = xstrings where address_id = kid and (script is null AND script_ is null);
    ELSE
      RAISE EXCEPTION 'Error: %', err;
    END IF;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION G.scriptless_contracts (lowest bigint)
-- RELEVANT ONLY FOR MAINNET
returns table (address char, address_id bigint)
as $$
select address(address_id), address_id
from C.contract_script s
where s.script is null and address_id > lowest
order by address_id asc
limit 100;
$$ language sql stable;


CREATE OR REPLACE FUNCTION U.c_bs (xaddress bigint, bhid int, xbalance bigint, xscript text, xstrings text[])
RETURNS void
AS $$
DECLARE err text;
BEGIN
  UPDATE C.contract_balance SET balance = xbalance WHERE block_hash_id = bhid AND address_id = xaddress;
  IF xscript IS NOT NULL
  THEN
    IF (SELECT block_hash_id FROM C.contract_script WHERE address_id = xaddress AND (script IS NOT NULL OR script_ IS NOT NULL) LIMIT 1) IS NULL
    THEN
      BEGIN
        INSERT INTO C.contract_script VALUES(xaddress, xscript::jsonb, bhid, xstrings)
        ON CONFLICT DO NOTHING;
        EXCEPTION WHEN OTHERS THEN
          GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
          IF err = 'unsupported Unicode escape sequence'
          THEN
            INSERT INTO C.contract_script (address, script_, block_hash_id, strings) VALUES(xaddress, xscript::json, bhid, xstrings)
            ON CONFLICT DO NOTHING;
          ELSE
            RAISE EXCEPTION 'Error: %', err;
          END IF;
      END;
    END IF;
  END IF;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION U.c_bs2 (xaddress bigint, bh char, xbalance bigint, xscript jsonb, xstrings text[])
returns void
as $$
declare bhid int;
begin
bhid := (select block_hash_id(bh));
if bhid is null
then
  return;
end if;
update C.contract_balance set balance = xbalance where block_hash_id = bhid and address_id = xaddress;

if xscript is not null
then
  if (select block_hash_id from C.contract_script where address_id = xaddress and script = xscript limit 1) is null
  then
    insert into C.contract_script values(xaddress, xscript, bhid, xstrings)
    on conflict do update set script = xscript, strings = xstrings;
  end if;
end if;
end;
$$ language plpgsql;
-- this alternative version of U.c_bs serves to insert balances that are close to head's level, such that in case of a reorganization happening, it'll safely update balances: if the block was deleted because of a reorganization, the update will do nothing because the function takes "block hashes" instead of "block hash ids". This version is not systematically used because it's much slower to deal with those hashes: additional data (char vs int) and id lookup from hash.


CREATE OR REPLACE FUNCTION I.c_bs (xaddress bigint, bhid int, xbalance bigint, xblock_level int, xscript jsonb, xstrings text[])
returns void
as $$
begin
insert into C.contract_balance (address_id, block_hash_id, balance, block_level)
values (xaddress, bhid, xbalance, xblock_level)
on conflict do nothing --CONFLICT
;
if xscript is not null
then
  if (select block_hash_id from C.contract_script where address_id = xaddress and script = xscript limit 1) is null
  then
    insert into C.contract_script values(xaddress, xscript, bhid, xstrings)
    on conflict (address_id) do update set script = xscript, strings = xstrings;
  end if;
end if;
end;
$$ language plpgsql;


CREATE OR REPLACE FUNCTION H.c_bs (xaddress bigint, bhid int, xblock_level int)
returns void
as $$
begin
insert into C.contract_balance (address_id, block_hash_id, block_level)
values (xaddress, bhid, xblock_level)
on conflict do nothing --CONFLICT
;
end;
$$ language plpgsql;


CREATE OR REPLACE FUNCTION G.balanceless_contracts (p int, lim int)
returns table(address char, address_id bigint, block_hash char, block_hash_id int, block_level int) -- block_level is for logs
as $$
select address(address_id) as address, address_id, block_hash(block_hash_id) as block_hash, block_hash_id, block_level
from C.contract_balance
where balance is null
and block_level >= (select level
                    from c.block
                    where proto = p::smallint
                    order by level asc
                    limit 1)
and block_level <= (select level
                    from c.block
                    where proto = p::smallint
                    order by level desc
                    limit 1)
limit lim;
$$ language sql stable;



-- `g text` and not `g jsonb` because sometimes g receives PG-unsupported json
CREATE OR REPLACE FUNCTION I.tx (opaid bigint, xsource_id bigint, xdestination_id bigint, xfee bigint, xamount bigint, xparameters text, xstorage text, xconsumed_milligas numeric, xstorage_size numeric, xpaid_storage_size_diff numeric,  xentrypoint char, xnonce int, xstatus smallint, errors jsonb, xstrings text[])
RETURNS void
AS $$
DECLARE err text; jsonb_parameters jsonb; jsonb_storage jsonb; json_parameters json; json_storage json;
BEGIN
  BEGIN
    jsonb_parameters := xparameters::jsonb;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
         jsonb_parameters := null;
         json_parameters := xparameters::json;
      ELSE
        RAISE EXCEPTION 'I.tx: Error: %', err;
      END IF;
  END;
  BEGIN
    jsonb_storage := xstorage::jsonb;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
         jsonb_storage := null;
         json_storage := xstorage::json;
      ELSE
        RAISE EXCEPTION 'I.tx: Error: %', err;
      END IF;
  END;
  INSERT INTO C.tx (operation_id,  source_id,  destination_id,  fee,  amount,       parameters,       storage,     parameters_,     storage_,  consumed_milligas,  storage_size,  status,  paid_storage_size_diff,  entrypoint,  nonce, error_trace,  strings
  , uri, contracts --SEQONLY
  )
  VALUES           (       opaid, xsource_id, xdestination_id, xfee, xamount, jsonb_parameters, jsonb_storage, json_parameters, json_storage, xconsumed_milligas, xstorage_size, xstatus, xpaid_storage_size_diff, xentrypoint, xnonce,      errors, xstrings
  , (select array_agg(I.uri(element)) from unnest(xstrings) as element where char_length(element) < 2048 AND (element ~ '[a-zA-Z][a-zA-Z]*://..*' or element ~ 'tezos-storage:.*')) --SEQONLY
  , (select array_agg(I.address(element::char(36),opaid)) from unnest(xstrings) as element where (element like 'tz%' or element like 'KT%') and octet_length(element) >= 36) --SEQONLY
  )
  ON CONFLICT DO NOTHING --CONFLICT
  ;
  INSERT INTO C.operation_sender_and_receiver VALUES (opaid, xsource_id, xdestination_id)
  ON CONFLICT DO NOTHING --CONFLICT
  ;
END;
$$ LANGUAGE PLPGSQL;


-- create or replace function foo() returns text
-- AS $$
-- DECLARE x text;
-- BEGIN
-- select '{"error" : "\u0000"}'::jsonb;
-- EXCEPTION WHEN OTHERS THEN
--   GET STACKED DIAGNOSTICS x = PG_EXCEPTION_CONTEXT;
-- return x;
-- END $$ language plpgsql;


CREATE OR REPLACE FUNCTION I.origination (opaid bigint, source bigint, k bigint, consumed_milligas numeric, storage_size numeric, paid_storage_size_diff numeric, fee bigint, nonce int, preorigination_id bigint, xscript json, delegate_id bigint, credit bigint, manager_id bigint, bhid int, status smallint, errors jsonb, xstrings text[])
returns void
as $$
begin
insert into C.origination
values
(opaid, source, k, consumed_milligas, storage_size, paid_storage_size_diff, fee, nonce, preorigination_id, delegate_id, credit, status, errors)
on conflict do nothing --CONFLICT
;
-- If you don't need scripts, you might want to remove the following insertion:
if xscript is not null and k is not null
then
  insert into C.contract_script (address_id, script, block_hash_id, strings
    , uri, contracts --SEQONLY
  ) values (k, xscript, bhid, xstrings
    , (select array_agg(I.uri(element)) from unnest(xstrings) as element where char_length(element) < 2048 AND (element ~ '[a-zA-Z][a-zA-Z]*://..*' or element ~ 'tezos-storage:.*')) --SEQONLY
    , (select array_agg(I.address(element::char(36),opaid)) from unnest(xstrings) as element where (element like 'tz%' or element like 'KT%') and octet_length(element) >= 36) --SEQONLY
  )
  on conflict (address_id) do update set script = xscript, block_hash_id = bhid --CONFLICT
  ;
end if;
-- The immediate following insertion might be useless, depending on your needs:
if manager_id is not null
then
insert into C.manager values (opaid, manager_id)
on conflict do nothing--CONFLICT
;
end if;
insert into C.operation_sender_and_receiver
values (opaid, source, k)
on conflict do nothing --CONFLICT
;
end
$$ language plpgsql;


CREATE OR REPLACE FUNCTION I.delegation (opaid bigint, source bigint, pkh bigint, gas numeric, f bigint, n int, status smallint, errors jsonb)
returns void
as $$
insert into C.delegation values (opaid, source, pkh, gas, f, n, status, errors)
on conflict do nothing --CONFLICT
;
insert into C.operation_sender_and_receiver values (opaid, source, pkh)
on conflict do nothing --CONFLICT
;
$$ language sql;


CREATE OR REPLACE FUNCTION I.reveal (opaid bigint, source bigint, pk char, gas numeric, f bigint, n int, status smallint, errors jsonb)
returns void
as $$
insert into C.reveal values (opaid, source, pk, gas, f, n, status, errors)
on conflict do nothing --CONFLICT
;
insert into C.operation_sender_and_receiver values (opaid, source, null)
on conflict do nothing --CONFLICT
;
$$ language sql;


CREATE OR REPLACE FUNCTION I.balance_o (opaid bigint, bal smallint, k bigint, cy int, di bigint, id int)
returns void
as $$
insert into C.balance_updates_op (operation_id, balance_kind, contract_address_id, cycle, diff, id)
values (opaid, bal, k, cy, di, id)
on conflict do nothing --CONFLICT
;
$$ language sql;


CREATE OR REPLACE FUNCTION I.balance_b (bhid int, bal smallint, k bigint, cy int, di bigint, id int)
returns void
as $$
insert into C.balance_updates_block (block_hash_id, balance_kind, contract_address_id, cycle, diff, id)
values (bhid, bal, k, cy, di, id)
on conflict do nothing --CONFLICT
;
$$ language sql;



CREATE OR REPLACE FUNCTION mark_rejected_blocks(howfar int)
returns table(hash char, level integer) as $$
update C.block b
set rejected = true
where not b.rejected
and b.level < (select level from C.block order by level desc limit 1)
and b.level > ((select level from C.block order by level desc limit 1) - howfar)
and (select count(*) from C.block x where x.level > b.level and x.predecessor_id = b.hash_id and not x.rejected) = 0
returning block_hash(b.hash_id), b.level
$$ language sql;



CREATE OR REPLACE FUNCTION balance_at_level(x varchar, lev int)
RETURNS TABLE(bal bigint)
AS $$
select coalesce(
  (SELECT C.balance
   FROM C.contract_balance c, C.block b
   WHERE address_id = address_id(x)
   and C.block_level <= lev
   and C.block_hash_id = b.hash_id
   order by C.block_level desc limit 1
  ),
  0) as bal
$$ LANGUAGE SQL stable;
-- SELECT balance_at_level('tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93', 1000000);


CREATE OR REPLACE FUNCTION delete_one_operation (xoperation_hash varchar)
returns  void
as $$
select record_log(concat('delete from C.operation where hash = ', xoperation_hash)) where xoperation_hash is not null;
delete from C.operation where hash = xoperation_hash;
$$ language sql;


CREATE OR REPLACE FUNCTION delete_one_block (x varchar)
returns varchar
as $$
select record_log(concat('delete from C.block_hash where hash = ', x)) where x is not null;
delete from C.block_hash where x is not null and hash = x;
select x;
$$ language SQL;


CREATE OR REPLACE FUNCTION delete_blocks_from_level (l int)
returns int
as $$
select record_log(concat(concat('delete blocks from level ', l::text),concat(' where highest level is ', (select hash_id::text from C.block order by level desc limit 1))));
delete from C.block_hash where hash_id < 0;
delete from C.block_hash where hash_id in (select hash_id from C.block where level >= l);
select record_log(concat('new highest block: ', (select hash_id::text from C.block order by level desc limit 1)));
select hash_id from C.block order by level desc limit 1;
$$ language SQL;


CREATE OR REPLACE FUNCTION delete_rejected_blocks ()
returns integer
as $$
select record_log('delete_rejected_blocks()') where (select count(*) from C.block b where b.rejected) > 0;
select delete_one_block(block_hash(b.hash_id)) as hash from C.block b where b.rejected;
select level from C.block order by level desc limit 1;
$$ language SQL;


CREATE OR REPLACE FUNCTION delete_rejected_blocks_and_above ()
returns integer
as $$
select record_log('delete_rejected_blocks_and_above()');
select delete_one_block(block_hash(b.hash_id)) from C.block b where b.level = (select bb.level from C.block bb where bb.rejected order by level asc limit 1);
select level from C.block order by level desc limit 1;
$$ language SQL;
