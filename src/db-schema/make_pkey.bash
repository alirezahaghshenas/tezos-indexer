#!/bin/bash

IFS=;
grep -E '^--PKEY' | sed -e "s/--PKEY //" | sed -e 's/--.*//' | while read l ; do
    IFS=';'
    set -- $l
    IFS=' '
    pkey=$(echo $1)
    table=$(echo $2)
    source=$(echo $3)
    cat <<EOF
select 'creating $pkey';
DO \$\$
BEGIN
    ALTER TABLE $table
      ADD CONSTRAINT $pkey
      PRIMARY KEY ($source);
EXCEPTION
WHEN SQLSTATE '42P16' THEN RETURN;
END;
\$\$;
EOF
    IFS=;
done


# --WHEN OTHERS THEN RAISE 'Error %', SQLSTATE;
