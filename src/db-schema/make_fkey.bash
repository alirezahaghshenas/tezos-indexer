#!/bin/bash

IFS=;
grep -E '^--FKEY' | sed -e "s/--FKEY //" | sed -e 's/--.*//' | while read l ; do
    IFS=';'
    set -- $l
    IFS=' '
    fkey=$(echo $1)
    table=$(echo $2)
    source=$(echo $3)
    target=$(echo $4)
    action=$(echo $5)
    cat <<EOF
select 'creating $fkey';
DO \$\$
BEGIN
    ALTER TABLE $table
      ADD CONSTRAINT $fkey
      FOREIGN KEY ($source) REFERENCES $target
      ON DELETE $action;
EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END;
\$\$;
EOF
    IFS=;
done
