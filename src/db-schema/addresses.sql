-- Open Source License
-- Copyright (c) 2020 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

SELECT 'addresses.sql' as file;

-- table of all existing addresses, so their storage can be factorized here

CREATE TABLE IF NOT EXISTS C.addresses (
 address char(36),
 address_id bigint not null unique,
 primary key(address)
);
CREATE INDEX IF NOT EXISTS addresses_autoid on C.addresses using btree (address_id);
-- no need to create index on address because it's the primary key

-- If you need to know if an address is for an implicit contract (tz...) or an originated contract (KT...), the fastest way is likely to use < and > comparisons.
-- select * from C.addresses where address < 't' ; -- returns all originated contracts
-- select * from C.addresses where address > 't' ; -- returns all implicit contracts
-- That should perform a lot faster than something like
-- select * from C.addresses where address like 'tz%' ; -- returns all implicit contracts
-- because the contents of the `address` column are sorted in an (btree) index.

CREATE OR REPLACE FUNCTION address_id(a char)
returns bigint
as $$
select address_id from C.addresses where address = a;
$$ language sql stable;


CREATE OR REPLACE FUNCTION I.address_aux(a char, id bigint)
returns bigint
as $$
insert into C.addresses values(a, id) on conflict do nothing returning address_id;
$$ language SQL;

CREATE OR REPLACE FUNCTION I.address(a char, id bigint)
returns bigint
as $$
DECLARE r bigint := null;
BEGIN
r := (select address_id from C.addresses where address = a);
if r is not null
then
  return r;
else
  r := (select I.address_aux(a, id));
  if r is null
  then
    r := (select address_id from C.addresses where address = a);
    if r is null then
      r := (select I.address_aux(a, -id));
    end if;
  else
    if a < 't' then insert into C.contract_script(address_id)values(r) on conflict do nothing; end if; --SEQONLY
    return r;
  end if;
  if r is null
  then
    r := (select address_id from C.addresses where address = a);
    loop
      if r is null then
        r := (select I.address_aux(a, (select address_id + 1::bigint from c.addresses order by address_id desc limit 1)));
      else
        return r;
      end if;
    end loop;
  else
    if a < 't' then insert into C.contract_script(address_id)values(r) on conflict do nothing; end if; --SEQONLY
    return r;
  end if;
  if r is null
  then
    raise 'Failed to record address % % % %', a, r, (select address from c.addresses where address = a), (select address_id::text from c.addresses where address = a);
  else
    if a < 't' then insert into C.contract_script(address_id)values(r) on conflict do nothing; end if; --SEQONLY
    return r;
  end if;
end if;
END
$$ language plpgsql;

-- CREATE OR REPLACE FUNCTION I.address(a char, id bigint) --MULTICORE
-- returns bigint --MULTICORE
-- as $$ --MULTICORE
-- select address_id from C.addresses where address = a; --MULTICORE
-- $$ language sql stable; --MULTICORE



CREATE OR REPLACE FUNCTION address(id bigint)
returns char
as $$
select address from C.addresses where address_id = id;
$$ language sql stable;

CREATE OR REPLACE FUNCTION address(id bigint[])
returns char[]
as $$
select array_agg(address) from C.addresses where address_id in (select unnest(id));
$$ language sql stable;

CREATE TABLE IF NOT EXISTS C.uri (
 address text,
 address_id int not null unique,
 primary key(address)
);
CREATE INDEX IF NOT EXISTS uri_address_id on C.uri using btree (address_id);

CREATE OR REPLACE FUNCTION uri(id bigint)
returns text
as $$
select address from C.uri where address_id = id;
$$ language sql stable;

CREATE OR REPLACE FUNCTION uri(id int[])
returns text[]
as $$
select array_agg(address) from C.uri where address_id in (select unnest(id));
$$ language sql stable;

CREATE OR REPLACE FUNCTION uri_id(i text)
returns int
as $$
select address_id from C.uri where address = i;
$$ language sql stable;



CREATE OR REPLACE FUNCTION I.uri_aux(a text, id integer)
returns integer
as $$
insert into C.uri values(a, id) on conflict do nothing returning address_id;
$$ language SQL;

CREATE OR REPLACE FUNCTION I.uri(a text)
returns integer
as $$
DECLARE r integer := null;
BEGIN
r := (select address_id from C.uri where address = a);
if r is not null
then
  return r;
else
  r := (select I.uri_aux(a, coalesce((select address_id+1 from c.uri order by address_id desc limit 1), 0)));
  if r is null
  then
    r := (select address_id from C.uri where address = a);
    if r is null then
      r := (select I.uri_aux(a, coalesce((select address_id-1 from c.uri order by address_id asc limit 1), 0)));
    end if;
  else
    return r;
  end if;
  if r is null
  then
    r := (select address_id from C.uri where address = a);
    if r is null then
      raise 'Failed to record URI address % % % %', a, r, (select address from C.uri where address = a), (select address_id::text from C.uri where address = a);
    end if;
  else
    return r;
  end if;
  if r is null
  then
    raise 'Failed to record URI address % % % %', a, r, (select address from C.uri where address = a), (select address_id::text from C.uri where address = a);
  else
    return r;
  end if;
end if;
END
$$ language plpgsql;
