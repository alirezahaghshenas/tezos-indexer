-- Open Source License
-- Copyright (c) 2018-2021 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

SELECT 'mainnet_block_validation.sql' as file;

CREATE OR REPLACE FUNCTION epoch_time_stamp (block_level int)
RETURNS int
AS $$
select extract(epoch from timestamp)::int from c.block where level = block_level;
$$ LANGUAGE SQL STABLE;


-- Disclaimer: The information provided on whether a block is final is
-- based on the analysis performed
-- [here](https://blog.nomadic-labs.com/emmy-in-the-partial-synchrony-model.html).
-- We emphasize that this analysis is based on a certain model of the
-- blockchain and of an attacker, which may not faithfully represent
-- the reality. In particular, the model assumes reasonable bounds on
-- the communication delays, namely that blocks and operations are
-- diffused within 30 seconds. The attacker model is that of a baker
-- not following the rules of the protocol, that is, it may double
-- bake and double endorse as it finds best in order to fork the
-- chain. Said otherwise, the attacker model does not capture an
-- attacker capable of disturbing the network by blocking or delaying
-- messages between honest participants. Finally, the underlying
-- computations are with respect to the security threshold of 10^-8
-- (that is, the chances of being wrong about a block being final are
-- one in 2 centuries) and of an attacker with at most 20% of the
-- total active stake.


-- The following function is when the optimal time between blocks is 60 seconds.
-- This function is relevant for mainnet using Emmy+ consensus algorithm.
CREATE OR REPLACE FUNCTION mainnet_block_is_final (block_level int)
RETURNS bool
AS $$
DECLARE
  ct int := (SELECT epoch_time_stamp(block_level));
BEGIN
  IF ((SELECT level FROM c.block ORDER BY level DESC LIMIT 1) - block_level) <  3 THEN RETURN false; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  3)) - ct) <=  227 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  4)) - ct) <=  455 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  5)) - ct) <=  683 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  6)) - ct) <=  903 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  7)) - ct) <= 1107 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  8)) - ct) <= 1327 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  9)) - ct) <= 1539 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 10)) - ct) <= 1759 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 11)) - ct) <= 1987 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 12)) - ct) <= 2215 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 13)) - ct) <= 2279 THEN RETURN true; END IF;
  RETURN (select mainnet_block_is_final(block_level + 1));
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION mainnet_block_is_final_pure (block_level int)
RETURNS bool
AS $$
WITH ct AS (SELECT epoch_time_stamp(block_level) as ts)
SELECT
  ((SELECT level FROM c.block ORDER BY level DESC LIMIT 1) - block_level) >= 3
  AND (
     ((SELECT epoch_time_stamp(block_level +  3)) - ct.ts) <=  227
  OR ((SELECT epoch_time_stamp(block_level +  4)) - ct.ts) <=  455
  OR ((SELECT epoch_time_stamp(block_level +  5)) - ct.ts) <=  683
  OR ((SELECT epoch_time_stamp(block_level +  6)) - ct.ts) <=  903
  OR ((SELECT epoch_time_stamp(block_level +  7)) - ct.ts) <= 1107
  OR ((SELECT epoch_time_stamp(block_level +  8)) - ct.ts) <= 1327
  OR ((SELECT epoch_time_stamp(block_level +  9)) - ct.ts) <= 1539
  OR ((SELECT epoch_time_stamp(block_level + 10)) - ct.ts) <= 1759
  OR ((SELECT epoch_time_stamp(block_level + 11)) - ct.ts) <= 1987
  OR ((SELECT epoch_time_stamp(block_level + 12)) - ct.ts) <= 2215
  OR ((SELECT epoch_time_stamp(block_level + 13)) - ct.ts) <= 2279
  OR (SELECT mainnet_block_is_final_pure(block_level + 1)))
FROM ct
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION mainnet_block_is_final_pure_direct (block_level int)
RETURNS bool
AS $$
SELECT
  ((SELECT level FROM c.block ORDER BY level DESC LIMIT 1) - block_level) >= 3
  AND (
     ((SELECT epoch_time_stamp(block_level +  3)) - (SELECT epoch_time_stamp(block_level))) <=  227
  OR ((SELECT epoch_time_stamp(block_level +  4)) - (SELECT epoch_time_stamp(block_level))) <=  455
  OR ((SELECT epoch_time_stamp(block_level +  5)) - (SELECT epoch_time_stamp(block_level))) <=  683
  OR ((SELECT epoch_time_stamp(block_level +  6)) - (SELECT epoch_time_stamp(block_level))) <=  903
  OR ((SELECT epoch_time_stamp(block_level +  7)) - (SELECT epoch_time_stamp(block_level))) <= 1107
  OR ((SELECT epoch_time_stamp(block_level +  8)) - (SELECT epoch_time_stamp(block_level))) <= 1327
  OR ((SELECT epoch_time_stamp(block_level +  9)) - (SELECT epoch_time_stamp(block_level))) <= 1539
  OR ((SELECT epoch_time_stamp(block_level + 10)) - (SELECT epoch_time_stamp(block_level))) <= 1759
  OR ((SELECT epoch_time_stamp(block_level + 11)) - (SELECT epoch_time_stamp(block_level))) <= 1987
  OR ((SELECT epoch_time_stamp(block_level + 12)) - (SELECT epoch_time_stamp(block_level))) <= 2215
  OR ((SELECT epoch_time_stamp(block_level + 13)) - (SELECT epoch_time_stamp(block_level))) <= 2279
  OR (SELECT mainnet_block_is_final_pure_direct(block_level + 1)))
$$ LANGUAGE SQL STABLE;



-- The following function is when the optimal time between blocks is 30 seconds.
-- This is meant to be used for edo2net and florencenet.
-- Other testnets might require different values.
-- This function is relevant for the above testnets using Emmy+ consensus algorithm.
CREATE OR REPLACE FUNCTION testnet_block_is_final (block_level int)
RETURNS bool
AS $$
DECLARE
  ct int := (SELECT epoch_time_stamp(block_level));
BEGIN
  IF ((SELECT level FROM c.block ORDER BY level DESC LIMIT 1) - block_level) <  3 THEN RETURN false; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  3)) - ct) <=  113 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  4)) - ct) <=  227 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  5)) - ct) <=  341 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  6)) - ct) <=  451 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  7)) - ct) <=  553 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  8)) - ct) <=  663 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  9)) - ct) <=  769 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 10)) - ct) <=  879 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 11)) - ct) <=  993 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 12)) - ct) <= 1107 THEN RETURN true; END IF;
  RETURN (select testnet_block_is_final(block_level + 1));
END;
$$ LANGUAGE PLPGSQL;


CREATE TABLE IF NOT EXISTS minimal_time_between_blocks (
  t int
);

CREATE OR REPLACE FUNCTION minimal_time_between_blocks()
RETURNS int
AS $$
SELECT extract(epoch FROM timestamp)::int - epoch_time_stamp(level - 1) AS t
FROM c.block
WHERE level > 1 AND level <= 100 -- increase this number if you need to
ORDER BY t ASC
limit 1;
$$ LANGUAGE SQL STABLE;

-- Use above functions if you know which one, otherwise the following one is generic, and was tested with edo2net, florencenet (no BA) and mainnet:
CREATE OR REPLACE FUNCTION block_is_final (block_level int)
RETURNS bool
AS $$
DECLARE m int := (select t from minimal_time_between_blocks);
BEGIN
  IF (SELECT level FROM c.block ORDER BY level DESC LIMIT 1) < 100 -- we want minimum 100 blocks in the chain but you can modify this value at your own risks if you're using a non-tested net
  THEN
    RETURN false;
  END IF;
  IF m is null
  THEN
    m := (SELECT minimal_time_between_blocks());
    INSERT INTO minimal_time_between_blocks VALUES(m);
  END IF;
  IF m = 30
  THEN
    SELECT testnet_block_is_final (block_level);
  ELSE
    SELECT mainnet_block_is_final (block_level);
  END IF;
END;
$$ LANGUAGE PLPGSQL;
