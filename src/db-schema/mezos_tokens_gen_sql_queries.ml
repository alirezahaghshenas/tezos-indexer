(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019-2020 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let print_select =
  Printf.printf
    "(select * from get_token_%s(address, token) where id < lastid \
     order by id desc limit lim)\n"
let print_select_from =
  Printf.printf
    "(select * from get_token_%s(address, token) where id < lastid \
     and \"timestamp\" <= \"from\" \
     order by id desc limit lim)\n"
let print_select_downto =
  Printf.printf
    "(select * from get_token_%s(address, token) where id < lastid \
     and \"timestamp\" >= downto \
     order by id desc limit lim)\n"
let print_select_from_downto =
  Printf.printf
    "(select * from get_token_%s(address, token) where id < lastid \
     and \"timestamp\" <= \"from\" \
     and \"timestamp\" >= downto \
     order by id desc limit lim)\n"

let _ =
  List.iter (fun (print_select, suffix, param) ->
    List.iter (fun (l, alias) ->
        let fn =
          match alias with
            Some al -> "_token_" ^ al
          | None ->
            List.fold_left
              (fun r e -> Printf.sprintf "%s_%s" r e)
              "_token"
              l
            ^ ""
        in
        let print = print_string in
        print "CREATE OR REPLACE FUNCTION get_ops";
        print fn;
        print suffix;
        print "(address varchar, token varchar";
        print param;
        print ", lastid integer, lim integer";
        print ")
RETURNS TABLE(
type text,
id bigint,
level int,
\"timestamp\" timestamp,
block char,
hash char,
caller char,
tz_amount bigint,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
action_source char,
action_destination char,
action_amount numeric,
action_callback char
)
AS $$
(";
        begin
          let rec loop = function
            | [] -> ()
            | (("transfer"
               | "approve"
               | "get_balance"
               | "get_allowance"
               | "get_total_supply")
               as e)::next ->
              print_select e;
              if next <> [] then print " union ";
              loop next
            | _ -> assert false
          in loop l
        end;
        print ")\n";
        if List.length l > 1 then
          print "order by id desc limit lim\n";
        print "$$ language sql;\n";
  )
  [
      ([ "transfer"; "approve"; "get_balance"; "get_allowance"; "get_total_supply" ],
       Some "all_operations")
    ; ([ "transfer"; "approve" ], None)
    ; ([ "transfer" ], None)
    ; ([ "approve" ], None)
    ]
    )
    [
      print_select, "", "";
      print_select_from, "_from", ", \"from\" timestamp";
      print_select_downto, "_downto", ", downto timestamp";
      print_select_from_downto, "_from_downto", ", \"from\" timestamp, downto timestamp";
    ]
