-- Open Source License
-- Copyright (c) 2018-2021 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

SELECT 'mezos.sql' as file;

-- DROP FUNCTION contracts2(character varying);

CREATE OR REPLACE FUNCTION contracts2 (x varchar)
RETURNS TABLE(k char, bal bigint, operation_hash char)
AS $$
SELECT x, coalesce((SELECT balance FROM C.contract_balance WHERE address_id = address_id(x) order by block_level desc limit 1), 0), null
UNION ALL
SELECT
  address(k_id)
, coalesce((SELECT balance FROM C.contract_balance WHERE address_id = k_id order by block_level desc limit 1), 0)
, operation_hash_alpha(operation_id)
FROM C.origination o
WHERE o.source_id = address_id(x)
$$ LANGUAGE SQL stable;


CREATE OR REPLACE FUNCTION latest_balance_by_id (x bigint)
RETURNS TABLE(bal bigint)
AS $$
select coalesce((
SELECT C.balance
FROM C.contract_balance c, C.block b
WHERE C.address_id = x
  and C.block_hash_id = b.hash_id
order by C.block_level desc limit 1
), 0) as bal
$$ LANGUAGE SQL stable;


CREATE OR REPLACE FUNCTION latest_balance (x varchar)
RETURNS TABLE(bal bigint)
AS $$
select latest_balance_by_id(address_id(x)) as bal;
$$ LANGUAGE SQL stable;


-- DROP FUNCTION contracts3;

CREATE OR REPLACE FUNCTION contracts3 (x varchar)
RETURNS TABLE(k char, bal bigint, operation_hash char, delegate char, storage jsonb)
AS $$
SELECT x, (select latest_balance (x)), null, null, null
UNION ALL
SELECT
  address(k_id),
  (select latest_balance_by_id(k_id)),
  operation_hash_alpha(operation_id),
  address(delegate_id),
  (select c.script->'storage' as storage from C.contract_script c where C.address_id = k_id)
FROM C.origination o
WHERE o.source_id = address_id(x)
$$ LANGUAGE SQL stable;


CREATE OR REPLACE FUNCTION manager (x varchar)
RETURNS TABLE(pkh char)
AS $$
select coalesce(
(select address(tx.source_id)
from C.tx tx
where tx.destination_id = address_id(x)
limit 1),
(select address(o.source_id)
from C.origination o
where o.k_id = address_id(x))
)
$$ LANGUAGE SQL stable;



CREATE OR REPLACE FUNCTION get_reveal (address varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
internal smallint,
nonce int,
public_key char,
amount bigint,
destination char,
"parameters" jsonb,
entrypoint char,
contract_address char,
delegate char,
status char,
error_trace jsonb
)
AS $$
select
       'reveal', -- type
       oa.autoid as id, -- id
       b.level, -- level
       b.timestamp, -- timestamp
       block_hash(b.hash_id),  -- block
       op.hash, -- hash
       address, -- source
       r.fee, -- fees
       m.counter,
       m.gas_limit,
       m.storage_limit,
       oa.id,
       oa.internal, -- internal
       r.nonce, -- nonce
       r.pk, -- public_key (reveal)
       cast(null as bigint), -- amount (tx)
       null, -- destination (tx)
       null::jsonb, -- parameters (tx)
       null, -- entrypoint (tx)
       null, -- contract_address (origination)
       null  -- delegate (delegation)
       , status(status)
       , error_trace
from
   C.operation_alpha oa,
   C.block b,
   C.manager_numbers m,
   C.reveal r,
   C.operation op
where
    r.source_id = address_id(address)
AND r.operation_id = oa.autoid
AND op.hash_id = oa.hash_id
AND b.hash_id = op.block_hash_id
AND m.operation_id = oa.autoid
$$ LANGUAGE SQL stable;
-- select * from get_reveal('tz1NKR6nBuLPxSGnFBBTXWLtD2Dt5UAYPWXo') limit 10;
-- select * from get_reveal('tz1LbSsDSmekew3prdDGx1nS22ie6jjBN6B3') limit 10;


CREATE OR REPLACE FUNCTION get_transaction (address varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
internal smallint,
nonce int,
public_key char,
amount bigint,
destination char,
"parameters" jsonb,
entrypoint char,
contract_address char,
delegate char,
status char,
error_trace jsonb
)
AS $$
select 'transaction', oa.autoid, b.level, b.timestamp, block_hash(b.hash_id), op.hash, address(t.source_id), t.fee,
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       oa.internal,
       t.nonce,
       null,  -- address (reveal)
       t.amount, -- amount (tx)
       address(t.destination_id), -- destination (tx)
       t."parameters", -- parameters (tx)
       t.entrypoint, -- entrypoint (tx)
       null, -- contract_address (origination)
       null -- delegate (delegation)
       , status(status)
       , error_trace
from C.operation_alpha oa, C.tx t, C.manager_numbers m
, C.operation op
, C.block b
where
    (address_id(address) = t.destination_id or address_id(address) = t.source_id)
AND oa.autoid = t.operation_id
AND op.hash_id = oa.hash_id
AND m.operation_id = oa.autoid
AND b.hash_id = op.block_hash_id
$$ LANGUAGE SQL stable;
-- select * from get_transaction('tz1NKR6nBuLPxSGnFBBTXWLtD2Dt5UAYPWXo') limit 10;
-- select * from get_transaction('tz1LbSsDSmekew3prdDGx1nS22ie6jjBN6B3') limit 10;


CREATE OR REPLACE FUNCTION get_origination (address varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
internal smallint,
nonce int,
public_key char,
amount bigint,
destination char,
"parameters" jsonb,
entrypoint char,
contract_address char,
delegate char,
status char,
error_trace jsonb
)
AS $$
select
  'origination'
, oa.autoid
, b.level
, b.timestamp
, block_hash(b.hash_id)
, op.hash
, address(o.source_id)
, o.fee
, m.counter
, m.gas_limit
, m.storage_limit
, oa.id
, oa.internal
, o.nonce
, null
, cast(null as bigint) -- amount (tx)
, null -- destination (tx)
, null::jsonb -- parameters (tx)
, null -- entrypoint (tx)
, address(o.k_id) -- contract_address (origination)
, null -- delegate (delegation)
, status(status)
, error_trace
from C.operation_alpha oa, C.manager_numbers m, C.origination o, C.block b, C.operation op
where
    (address_id(address) = o.source_id or address_id(address) = o.k_id)
and o.operation_id = oa.autoid
AND oa.hash_id = op.hash_id
AND m.operation_id = oa.autoid
and op.block_hash_id = b.hash_id
$$ LANGUAGE SQL stable;
-- select * from get_origination('tz1NKR6nBuLPxSGnFBBTXWLtD2Dt5UAYPWXo') limit 10;


CREATE OR REPLACE FUNCTION get_delegation (address varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
internal smallint,
nonce int,
public_key char,
amount bigint,
destination char,
"parameters" jsonb,
entrypoint char,
contract_address char,
delegate char,
status char,
error_trace jsonb
)
AS $$
select
  'delegation'
, oa.autoid
, b.level
, b.timestamp
, block_hash(b.hash_id)
, op.hash
, address(d.source_id)
, d.fee
, m.counter
, m.gas_limit
, m.storage_limit
, oa.id
, oa.internal
, d.nonce
, null -- public_key (revelation)
, cast(null as bigint) --amount (tx)
, null -- destination (tx)
, null::jsonb -- parameters (tx)
, null -- entrypoint (tx)
, null -- contract_address (origination)
, address(pkh_id) -- delegate (delegation)
, status(status)
, error_trace
from C.operation_alpha oa, C.manager_numbers m, C.delegation d, C.block b, C.operation op
where
   (address_id(address) = d.pkh_id or address_id(address) = d.source_id)
AND d.operation_id = oa.autoid
AND m.operation_id = oa.autoid
AND oa.hash_id = op.hash_id
AND op.block_hash_id = b.hash_id
$$ LANGUAGE SQL stable;
-- select * from get_delegation('tz1hGaDz45yCG1AbZqwS653KFDcvmv6jUVqW') limit 10;;
-- select * from get_delegation('tz1LbSsDSmekew3prdDGx1nS22ie6jjBN6B3') limit 10;


CREATE OR REPLACE FUNCTION get_operations (address varchar, lastid bigint, lim integer)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
internal smallint,
nonce int,
public_key char,
amount bigint,
destination char,
"parameters" jsonb,
entrypoint char,
contract_address char,
delegate char,
status char,
error_trace jsonb
)
AS $$
((select * from get_delegation(address) where id < lastid order by id desc limit lim)
union
(select * from get_origination(address) where id < lastid order by id desc limit lim)
union
(select * from get_transaction(address) where id < lastid order by id desc limit lim)
union
(select * from get_reveal(address)  where id < lastid order by id desc limit lim)
)
order by id desc limit lim
$$ language sql stable;


-- drop function get_operations ;
-- drop function get_transaction ;
-- drop function get_reveal ;
-- drop function get_origination ;
-- drop function get_delegation ;
