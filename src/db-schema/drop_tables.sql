-----------------------------------------------------------------------------
-- DO NOT BLINDLY RUN THE CONTENTS OF THIS FILE
-----------------------------------------------------------------------------

-- If ever this file is blindly run, it should only consume time without
-- having any actual effect on the database: everything should be inside SQL
-- transactions that are rolled back no matter what.

-----------------------------------------------------------------------------
-- BETTER NOT TO SYSTEMATICALLY DROP THESE TABLES
-----------------------------------------------------------------------------
BEGIN;
drop table C.addresses cascade; -- can cause lots of issues if not properly dropped
drop table indexer_log cascade; -- should remain small
drop table C.chain cascade; -- tiny table
ROLLBACK;
END;

-----------------------------------------------------------------------------
-- CHAIN TABLES
-----------------------------------------------------------------------------
BEGIN;
-- git grep 'CREATE TABLE.*C\.' src/db-schema/|sed -e 's/.* C\./drop table C./' -e 's/(/;/'
drop table C.bigmap ;
drop table C.chain ;
drop table C.block_hash ;
drop table C.block ;
drop table C.operation ;
drop table C.operation_alpha ;
drop table C.operation_sender_and_receiver ;
drop table C.proposals ;
drop table C.proposal ;
drop table C.ballot ;
drop table C.double_endorsement_evidence ;
drop table C.double_baking_evidence ;
drop table C.manager_numbers ;
drop table C.activation ;
drop table C.endorsement ;
drop table C.seed_nonce_revelation ;
drop table C.block_alpha ;
drop table C.deactivated ;
drop table C.contract ;
drop table C.contract_balance ;
drop table C.tx ;
drop table C.origination ;
drop table C.delegation ;
drop table C.reveal ;
drop table C.balance_updates_block ;
drop table C.balance_updates_op ;
drop table C.snapshot ;
drop table C.delegated_contract ;
ROLLBACK;
END;

-----------------------------------------------------------------------------
-- MEMPOOL TABLES
-----------------------------------------------------------------------------
BEGIN;
-- git grep 'CREATE TABLE.*M\.' src/db-schema/|sed -e 's/.* M\./drop table M./' -e 's/(/;/'
drop table M.op_branch ;
drop table M.operation_alpha ;
ROLLBACK;
END;

-----------------------------------------------------------------------------
-- TOKENS TABLES
-----------------------------------------------------------------------------
-- git grep 'CREATE TABLE.*T\.' src/db-schema/|sed -e 's/.* T\./drop table T./' -e 's/(/;/'
drop table T.contract ;
drop table T.balance ;
drop table T.operation ;
drop table T.transfer ;
drop table T.approve ;
drop table T.get_balance ;
drop table T.get_allowance ;
drop table T.get_total_supply ;
