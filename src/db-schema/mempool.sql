-- Open Source License
-- Copyright (c) 2020 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

-- DB schema for operations in the mempool, so you may track the life of an operation

SELECT 'mempool.sql' as file;

DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'mempool_op_status') THEN
    CREATE TYPE mempool_op_status AS ENUM ('applied', 'refused', 'branch_refused', 'unprocessed', 'branch_delayed');
  END IF;
END
$$;


CREATE TABLE IF NOT EXISTS M.op_branch (
  hash char(51) not null,
  branch char(51) not null,
  primary key(hash)
);


CREATE TABLE IF NOT EXISTS M.operation_alpha (
  hash char(51) not null,
  first_seen_level int not null,
  first_seen_timestamp double precision not null,
  last_seen_level int not null,
  last_seen_timestamp double precision not null,
  status mempool_op_status not null,
  id smallint not null,
  -- index of op in contents_list
  operation_kind smallint not null,
  -- 0: Endorsement
  -- 1: Seed_nonce_revelation
  -- 2: double_endorsement_evidence
  -- 3: Double_baking_evidence
  -- 4: Activate_account
  -- 5: Proposals
  -- 6: Ballot
  -- 7: Manager_operation { operation = Reveal _ ; _ }
  -- 8: Manager_operation { operation = Transaction _ ; _ }
  -- 9: Manager_operation { operation = Origination _ ; _ }
  -- 10: Manager_operation { operation = Delegation _ ; _ }
  source char(36),
  -- sender
  destination char(36),
  -- receiver, if any
  operation_alpha jsonb,
  autoid SERIAL, -- this field should always be last
  primary key(hash, id, status),
  -- foreign key(source) references C.addresses(address) on delete cascade,
  -- foreign key(destination) references C.addresses(address) on delete cascade,
  foreign key(hash) references M.op_branch(hash) on delete cascade
);
CREATE INDEX IF NOT EXISTS mempool_operations_hash on M.operation_alpha(hash);
CREATE INDEX IF NOT EXISTS mempool_operations_status on M.operation_alpha(status); --OPT
CREATE INDEX IF NOT EXISTS mempool_operations_source on M.operation_alpha(source);
CREATE INDEX IF NOT EXISTS mempool_operations_destination on M.operation_alpha(destination);
CREATE INDEX IF NOT EXISTS mempool_operations_kind on M.operation_alpha(operation_kind);

-- foreign keys operation_alpha_source_fkey and operation_alpha_destination_fkey cause too much trouble

DROP FUNCTION IF EXISTS M.make_address_id;

DO $$
BEGIN
alter table m.operation_alpha drop constraint operation_alpha_source_fkey;
exception when others then return;
END $$;

DO $$
BEGIN
alter table m.operation_alpha drop constraint operation_alpha_destination_fkey;
exception when others then return;
END $$;


CREATE OR REPLACE FUNCTION M.I_operation_alpha (
  xbranch char(51),
  xlevel int,
  xhash char(51),
  xstatus mempool_op_status,
  xid smallint,
  xoperation_kind smallint,
  xsource char(36),
  xdestination char(36),
  xseen_timestamp double precision,
  xoperation_alpha jsonb
)
returns void
as $$
  insert into M.op_branch (hash, branch) values (xhash, xbranch) on conflict do nothing;
  insert into M.operation_alpha (
      hash
    , first_seen_level
    , first_seen_timestamp
    , last_seen_level
    , last_seen_timestamp
    , status
    , id
    , operation_kind
    , source
    , destination
    , operation_alpha
  ) values (
      xhash
    , xlevel
    , xseen_timestamp
    , xlevel
    , xseen_timestamp
    , xstatus
    , xid
    , xoperation_kind
    , xsource
    , xdestination
    , xoperation_alpha
  ) on conflict (hash, id, status)
  do update set
    last_seen_level = xlevel
  , last_seen_timestamp = xseen_timestamp
  where M.operation_alpha.hash = xhash
    and M.operation_alpha.id = xid
    and M.operation_alpha.status = xstatus
$$ language sql;
