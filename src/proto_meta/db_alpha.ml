(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(**#X ifdef __META1__ open Tezos_raw_protocol_(**# get PROTO#**)X#**)
(**#X ifdef __META2__ open Tezos_protocol_(**# get PROTO #**) open Protocol X#**)
(**#X ifdef __META3__ open Tezos_protocol_(**# get PROTO #**) open Protocol X#**)

open Alpha_context

module Utils = struct

  let extract_strings_and_bytes_from_expr expr =
    Verbose.Script.extract_strings_and_bytes (Tezos_micheline.Micheline.root expr)

  let extract_strings_and_bytes script =
    match script with
    | Script.{ code ; storage } ->
      match Data_encoding.force_decode code, Data_encoding.force_decode storage with
      | None, None -> None
      | Some _, None -> assert false
      | None, Some _ -> assert false
      | Some code, Some storage ->
        match
          extract_strings_and_bytes_from_expr storage,
          extract_strings_and_bytes_from_expr code
        with
        | None, None -> None
        | Some l, Some l2 -> let module S = Set.Make(String) in
          Some(
            List.fold_left (fun r e -> S.add e r)
              (List.fold_left (fun r e -> S.add e r) S.empty l)
              l2
            |> S.elements
          )
        | Some l, None
        | None, Some l -> Some l

  let extract_strings_and_bytes_from_lazy_expr le =
    match Data_encoding.force_decode le with
    | None -> None
    | Some e -> extract_strings_and_bytes_from_expr e

end

(**# ifdef __META3__
module Contract = struct
  include Contract
  (* type big_map_diff = Lazy_storage.diffs *)
  (* type lazy_storage_diff = Lazy_storage.diffs *)
end
#**)
(**# else
module Contract = struct
   include Contract
   (* type lazy_storage_diff = unit *)
end
#**)


open Caqti_type.Std
open Db_shell


let cycle =
  let open Cycle in
  custom
    ~encode:(fun a -> Ok (to_int32 a))
    ~decode:(fun a -> Ok (add root (Int32.to_int a)))
    int32

let voting_period =
  custom
    ~encode:(fun a -> Ok (Data_encoding.Json.construct Voting_period.encoding a))
    ~decode:(fun a -> Ok (Data_encoding.Json.destruct Voting_period.encoding a))
    json

let voting_period_kind =
  let open Voting_period in
  custom
    ~encode:(function
        | Proposal       -> Ok 0
(**# ifndef __META4__
        | Testing_vote   -> Ok 1
        | Testing        -> Ok 2
        | Promotion_vote -> Ok 3
#**)
(**# ifdef __META3__
        | Adoption       -> Ok 4
#**)
(**# ifdef __META4__
      | Exploration    -> Ok 5
      | Cooldown       -> Ok 6
      | Promotion      -> Ok 7
   #**)
      )
    ~decode:(function
        | 0 -> Ok Proposal
(**# ifndef __META4__
        | 1 -> Ok Testing_vote
        | 2 -> Ok Testing
        | 3 -> Ok Promotion_vote
 #**)
(**# ifdef __META3__
        | 4 -> Ok Adoption
   #**)
(**# ifdef __META4__
      | 5 -> Ok Exploration
      | 6 -> Ok Cooldown
      | 7 -> Ok Promotion
   #**)

        | _ -> assert false
      )
    int

let to_caqti_error = function
  | Ok a -> Ok a
  | Error _ ->
    Format.eprintf "to_caqti_error failed\n%!";
    Stdlib.exit 1

let k =
  custom
    ~encode:(fun a -> Ok (Contract.to_b58check a))
    ~decode:(fun a -> to_caqti_error (Contract.of_b58check a))
    string

let tez =
  custom
    ~encode:(fun a -> Ok (Tez.to_mutez a))
    ~decode:begin fun a ->
      match Tez.of_mutez a with
      | None -> Error "tez"
      | Some t -> Ok t
    end
    int64

let opaid = int64
type opaid = int64

let pg_array_of_string_list =
  let rec loop b = function
    | [] -> ()
    | e::(_::_ as tl) ->
      Printf.bprintf b "%S," e;
      loop b tl
    | [e] ->
      Printf.bprintf b "%S" e
  in
  fun l ->
    let b = Buffer.create 64 in
    Printf.bprintf b "{";
    loop b l;
    Printf.bprintf b "}";
    Buffer.contents b


let pg_array_of_int_list =
  let rec loop b = function
    | [] -> ()
    | e::(_::_ as tl) ->
      Printf.bprintf b "%d," e;
      loop b tl
    | [e] ->
      Printf.bprintf b "%d" e
  in
  function
  | [] -> "null"
  | l ->
    let b = Buffer.create 64 in
    Printf.bprintf b "{";
    loop b l;
    Printf.bprintf b "}";
    Buffer.contents b

let int_list_from_string s =
  String.sub s 1 (String.length s - 2)
  |> String.split_on_char ','
  |> List.map (fun e -> String.trim e |> int_of_string)

(* let _ = int_list_from_string "{1,2,3 , 4 }" *)

let slots =
  custom
    ~encode:(fun a -> Ok (pg_array_of_int_list a))
    ~decode:(fun a -> Ok (int_list_from_string a))
    string

let text_array =
  custom
    ~encode:(fun a -> Ok (pg_array_of_string_list a))
    ~decode:(fun _ -> assert false)
    string


let custom_of_encoding encoding =
  custom
    ~encode:(fun a -> Ok (Data_encoding.Json.construct encoding a))
    ~decode:(fun a -> Ok (Data_encoding.Json.destruct encoding a))
    json

let _custom_to_string_of_encoding encoding =
  custom
    ~encode:(fun a -> Ok (Format.asprintf "%a" Data_encoding.Json.pp (Data_encoding.Json.construct encoding a)))
    ~decode:(fun _ -> assert false)
    string

let custom_to_string_of_pp pp =
  custom
    ~encode:(fun a -> Ok (Format.asprintf "%a" pp a))
    ~decode:(fun _ -> assert false)
    string

let nonce =
  custom_of_encoding Nonce.encoding
(* let nonce encoding =
 *   custom
 *     ~encode:(fun a -> Ok (Data_encoding.Json.construct Nonce.encoding a))
 *     ~decode:(fun a -> Ok (Data_encoding.Json.destruct Nonce.encoding a))
 *     json *)

let lazy_expr =
  custom
    ~encode:(fun a ->
        match Script_repr.force_decode a with
        | Ok (v (**# ifndef __PROTO10__ , _gas_cost #**)) ->
          Ok (Data_encoding.Json.construct Script_repr.expr_encoding v)
        | _ -> assert false)
    ~decode:(fun _ -> assert false)
    json

(**# ifdef __PROTO1__
(* let big_map_diff =
 *   custom
 *     ~encode:(function [] -> Ok `Null | _ -> assert false)
 *     ~decode:(function `Null -> Ok [] | _ -> assert false)
 *     json *)
(* let lazy_storage_diff =
 *   custom
 *     ~encode:(function () -> Ok `Null)
 *     ~decode:(function `Null -> Ok () | _ -> assert false)
 *     json *)
#**)(**# elseifdef __PROTO8__
(* let big_map_diff =
 *   custom
 *     ~encode:(function () -> Ok `Null)
 *     ~decode:(function `Null -> Ok () | _ -> assert false)
 *     json *)
(* let lazy_storage_diff =
 *   custom_of_encoding Lazy_storage.encoding *)
#**)(**# else
(* let big_map_diff =
 *   custom_of_encoding Contract.big_map_diff_encoding *)
(* let lazy_storage_diff =
 *   custom
 *     ~encode:(function () -> Ok `Null)
 *     ~decode:(function `Null -> Ok () | _ -> assert false)
 *     json *)
#**)

let script =
  custom_of_encoding Script.encoding
(* let script =
 *   let encoding =
 *     let open Data_encoding in
 *     def "scripted.contracts_decoded"
 *     @@ conv
 *       (fun Script.{code; storage} ->
 *          match Script_repr.force_decode code, Script_repr.force_decode storage with
 *          | Error _, _  -> assert false
 *          | _, Error _  -> assert false
 *          | Ok (code, _), Ok (storage, _) ->
 *            Format.printf ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> %a %a\n%!"
 *              Data_encoding.Json.pp (Data_encoding.Json.construct Script_repr.expr_encoding code)
 *              Data_encoding.Json.pp (Data_encoding.Json.construct Script_repr.expr_encoding storage)
 *            ;
 *            code, storage
 *       )
 *       (fun _ -> assert false)
 *       (obj2 (req "code" Script_repr.expr_encoding) (req "storage" Script_repr.expr_encoding))
 *   in
 *   custom_of_encoding encoding *)


(* let script_expr_hash =
 *   custom_to_string_of_encoding (Tezos_raw_protocol_(\**# get PROTO#**\).Script_expr_hash.encoding) *)

let script_expr_hash =
  custom_to_string_of_pp (Tezos_raw_protocol_(**# get PROTO#**).Script_expr_hash.pp)

let script_expr =
  custom_of_encoding Script.expr_encoding

(**# ifdef __META4__ module Delegate = Receipt #**)

let balance =
  custom
    ~encode:begin function
      | Delegate.Contract _ -> Ok 0
      | Rewards _ -> Ok 1
      | Fees _ -> Ok 2
      | Deposits _ -> Ok 3
    end
    ~decode:(fun _ -> assert false)
    int

let balance_update =
  custom
    ~encode:begin function
      | Delegate.Credited t -> Ok (Tez.to_mutez t)
      | Debited t -> Ok (Int64.neg (Tez.to_mutez t))
    end
    ~decode:begin fun t ->
      let positive = t > 0L in
      match Tez.of_mutez (Int64.abs t) with
      | None -> Error "of_mutez"
      | Some t ->
          if positive then Ok (Delegate.Credited t)
          else Ok (Debited t)
    end
    int64


module Block_alpha_table = struct
  let insert =
    query_zero_or_one
      (tup3
         (tup4 bhid kid int32 cycle)
         (tup4 int32 voting_period int32 voting_period_kind) milligas)
      unit
      "select I.block_alpha(?,?,?,?,?,?,?,?,?::numeric)"
end

module Operation_alpha_table = struct
  let insert =
    query_zero_or_one
      (tup2 (tup4 ophid int int bhid) (tup2 int opaid))
      unit
      "select I.opalpha($1,$2,$3,$4,$5,$6)"
end


module Manager_numbers = struct
  let insert =
    query_zero_or_one
      (tup4 opaid z z z)
      unit
      "select I.manager_numbers(?,?::numeric,?::numeric,?::numeric)"
end

module Contract_table = struct
  let update_script =
    query_zero_or_one
      (tup4 kid script bhid (option text_array))
      unit
      "select U.script($1,$2,$3,$4)"

  let get_scriptless_contracts =
    query_zero_or_more
      kid
      (tup2 k kid)
      "select * from G.scriptless_contracts($1)"
end

module Contract_balance_table = struct

  let update =
    query_zero_or_one
      (tup4 kid bhid tez (option script))
      unit
      "select U.c_bs($1,$2,$3,$4,null)"

  let update_via_bh =
    query_zero_or_one
      (tup4 kid bh tez (option script))
      unit
      "select U.c_bs2($1,$2,$3,$4,null)"

  let insert_balance_full =
    query_zero_or_one
      (tup3 (tup4 kid bhid tez bl) (option script) (option text_array))
      unit
      "select I.c_bs($1,$2,$3,$4,$5,$6)"

  let pre_insert_balance =
    query_zero_or_one
      (tup3 kid bhid bl)
      unit
      "select H.c_bs($1,$2,$3)"

  let get_balanceless_contracts =
    query_zero_or_more
      (tup2 int int) (* proto, limit *)
      (tup2 (tup2 k kid) (tup3 bh bhid bl))
          "select address,address_id,block_hash,block_hash_id,block_level \
           from G.balanceless_contracts($1,$2)"
end


module Balance_table = struct
  type t = (Delegate.balance * Delegate.balance_update) list

  type table = {
    bhid: bhid ;
    opaid: opaid option ;
    balance: Delegate.balance ;
    k: kid option ;
    cycle: Cycle.t option ;
    diff: Delegate.balance_update ;
    id : int; (* balance update "index" (or "counter") within the current list of balance updates -
                 this is to make sure we allow legitimate duplicates
                 and reject illegitimate duplicates *)
  }

  let table_o = (* from operations *)
    custom
      ~encode:begin fun { bhid = _ ; opaid ; id ;
                          balance ; k ; cycle ; diff } ->
        let opaid =
          match opaid with
          | Some opaid -> opaid
          | None -> assert false in
        Ok (opaid, (balance, k, cycle, diff), id)
      end
      ~decode:begin fun _ -> assert false  end
      (tup3
         opaid
         (tup4 balance (option kid) (option cycle) balance_update)
         int)

  let table_b = (* from blocks *)
    custom
      ~encode:begin fun { bhid ; opaid ; id ;
                          balance ; k ; cycle ; diff } ->
        assert (opaid = None);
        Ok (bhid, (balance, k, cycle, diff), id)
      end
      ~decode:begin fun _ -> assert false end
      (tup3
         bhid
         (tup4 balance (option kid) (option cycle) balance_update)
         int)

  let insert_o =
    query_zero_or_one
      table_o
      unit
      "select I.balance_o(?,?,?,?,?,?)"

  let insert_b =
    query_zero_or_one
      table_b
      unit
      "select I.balance_b(?,?,?,?,?,?)"

  let update ~get_kid ?(tally=incr) ?opaid conn ~bhid (t: t) =
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    let counter = ref 0 in
    Lwt_list.map_s (fun (balance, diff) ->
        tally counter;
        match balance with
        | Delegate.Contract k ->
          get_kid k >>= fun k ->
          Lwt.return
            { k = Some k ; bhid ; opaid ;
              balance ; cycle = None ; diff ; id = !counter }
        | Rewards (pkh, cycle)
        | Fees (pkh, cycle)
        | Deposits (pkh, cycle) ->
          let k = Contract.implicit_contract pkh in
          get_kid k >>= fun k ->
          Lwt.return
            { k = Some k ; bhid ; opaid  ;
              balance ; cycle = Some cycle; diff ; id = !counter }
      ) t
    >>= fun l ->
    match opaid with
    | Some _ ->
      with_transaction_ignore conn insert_o l
    | None ->
      with_transaction_ignore conn insert_b l

end

type operation_status = int
let opstatus = int

type error_list = string
let error_list = string


module Origination_table = struct
  type t = {
    opaid : opaid;
    src: kid ;
    k: kid option;
    consumed_gas : Fpgas.t option;
    storage_size : Z.t option;
    paid_storage_size_diff : Z.t option;
    fee : Tez.t ;
    nonce : int option;
    preorigination_id : kid option;
    script : Script.t option;
    delegate_id : kid option;
    credit : Tez.t;
    manager_id : kid option;
    bhid : bhid;
    status : operation_status;
    error_list : string option;
    strings : string list option;
  }

  let encoding =
    custom
      ~encode:(fun { opaid ; src ; k ; consumed_gas ; storage_size ;
                     paid_storage_size_diff ; fee ; nonce
                   ; preorigination_id ; script ; delegate_id ; credit
                   ; manager_id
                   ; bhid
                   ; status
                   ; error_list
                   ; strings
                   } ->
                Ok (( (opaid, src, k, consumed_gas)
                    , (storage_size, paid_storage_size_diff, fee, nonce)
                    , (preorigination_id, script, delegate_id, credit)
                    , (manager_id, bhid, status, error_list))
                   , strings))
      ~decode:(fun _ -> assert false)
      (tup2
         (tup4
            (tup4 opaid kid (option kid) (option milligas))
            (tup4 (option z) (option z) tez (option int))
            (tup4 (option kid) (option script) (option kid) tez)
            (tup4 (option kid) bhid opstatus (option error_list))
         )
         (option text_array)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.origination($1,$2,$3,$4::numeric,\
       $5::numeric,\
       $6::numeric,\
       $7,\
       $8,\
       $9,$10,$11,$12,$13,$14,$15,$16,$17)"


end

module Tx_table = struct
  type t = {
    opaid : opaid;
    source : kid ;
    destination : kid ;
    fee : Tez.t ;
    amount : Tez.t ;
    parameters : Script.lazy_expr option ;
    storage : Script.lazy_expr option ;
    consumed_gas : Fpgas.t option ;
    storage_size : Z.t option ;
    paid_storage_size_diff : Z.t option ;
    entrypoint : string option ;
    nonce : int option ;
    status : operation_status ;
    error_list : error_list option;
    strings : string list option;
  }

  let encoding =
    custom
      ~encode:begin fun { opaid ; source ; destination ;
                          fee ; amount ; parameters ; storage ;
                          consumed_gas ; storage_size ; paid_storage_size_diff ;
                          entrypoint ; nonce ;
                          status ;
                          error_list ;
                          strings ;
                        } ->
        Ok (
          (opaid, source, destination, fee)
        , (amount, parameters, storage, consumed_gas)
        , (storage_size, paid_storage_size_diff, entrypoint, nonce)
        , (status, error_list, strings)
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup4
         (tup4 opaid kid kid tez)
         (tup4 tez (option lazy_expr) (option lazy_expr) (option milligas))
         (tup4 (option z) (option z) (option string) (option int))
         (tup3 opstatus (option error_list) (option text_array)))

  let insert =
    query_zero_or_one
      encoding
      unit
          "select I.tx($1,$2,$3,$4,$5,$6,$7,$8::numeric,\
           $9::numeric,$10::numeric,$11,$12,$13,$14,$15)"

end

module Delegation_table = struct
  let insert =
    query_zero_or_one
      (tup2
         (tup4 opaid kid (option kid) (option milligas))
         (tup4 tez (option int) opstatus (option error_list)))
      unit
      "select I.delegation($1,$2,$3,$4,$5,$6,$7,$8)"
end


module Reveal_table = struct
  let insert =
    query_zero_or_one
      (tup2 (tup4 opaid kid pk (option milligas)) (tup4 tez (option int) opstatus (option error_list)))
      unit
      "select I.reveal($1,$2,$3,$4,$5,$6,$7,$8)"
end


module Addresses = struct
  let insert =
    query_zero_or_one
      (tup2 k kid)
      kid
      "select I.address(?,?)"
end

let eoperation = custom_of_encoding Alpha_context.Operation.encoding

module Endorsement_table = struct
  type t = {
    opaid : opaid ;
    level : bl ;
    delegate_id : kid ;
    slots : int list ;
  }

  let encoding =
    custom
      ~encode:begin fun { opaid; level; delegate_id; slots; } ->
        Ok (opaid, level, delegate_id, slots)
      end
      ~decode:begin fun _ -> assert false
      end
      (tup4 opaid bl kid slots)

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.endorsement($1,$2,$3,$4)"

  (**# ifdef __META4__
  type with_slot = { (* protos 9 & 10 only *)
    opaid : opaid ;
    delegate_id : kid ;
    slots : int list ;
    level : bl ;
    slot : int ;
  }

  let encoding_with_slot =
    custom
      ~encode:begin fun { opaid; delegate_id; slots; level; slot } ->
        Ok (opaid, delegate_id, slots, (level, slot))
      end
      ~decode:begin fun _ -> assert false
      end
      (tup4 opaid kid slots (tup2 bl int))

  let insert_with_slot =
    query_zero_or_one
      encoding_with_slot
      unit
      "select I.endoslot($1,$2,$3,$4,$5)"
     #**)

end

let proto_hash = custom_to_string_of_pp Protocol_hash.pp

module Proposals_table = struct
  type t = {
    opaid    : opaid;
    i        : int64;
    source   : kid;
    period   : int32;
    proposal : Protocol_hash.t;
  }
  let encoding =
    custom
      ~encode:begin fun { opaid; i; source; period; proposal; } ->
        Ok ((opaid, i), source, period, proposal)
      end
      ~decode:begin fun _ -> assert false end
      (tup4 (tup2 opaid int64) kid int32 proto_hash)
  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.proposal($1,$2,$3,$4,$5)"
  let insert2 =
    query_zero_or_one
      encoding
      unit
      "select I.proposal2($1,$2,$3,$4,$5)"
end

module Ballot_table = struct
  type t = {
    opaid    : opaid;
    i        : int64;
    source   : kid;
    period   : int32;
    proposal : Protocol_hash.t;
    ballot   : Alpha_context.Vote.ballot;
  }

  let ballot =
    custom
      ~encode:(fun a -> Ok Vote.(match a with Yay -> "yay" | Nay -> "nay" | Pass -> "pass"))
      ~decode:(fun a -> Ok Vote.(match a with "yay" -> Yay | "nay" -> Nay | "pass" -> Pass
                                            | _ -> assert false))
      string
  let encoding =
    custom
      ~encode:begin fun { opaid; i; source; period; proposal; ballot; } ->
        Ok (
          opaid, i,
          (source, period, proposal, ballot)
        )
      end
      ~decode:(fun _ -> assert false)
      (tup3
         opaid int64
         (tup4 kid int32 proto_hash ballot)
      )
  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.ballot($1,$2,$3,$4,$5,$6)"
end

module Double_endorsement_evidence_table = struct
  type t = {
    opaid : opaid;
    baker_id : kid;
    offender_id : kid;
    op1 : Alpha_context.Operation.packed;
    op2 : Alpha_context.Operation.packed;
  }
  let encoding =
    custom
      ~encode:begin fun { opaid; baker_id; offender_id; op1; op2; } ->
        Ok (opaid, (baker_id, offender_id), op1, op2)
      end
      ~decode:begin fun _ -> assert false end
      (tup4 opaid (tup2 kid kid) eoperation eoperation)
  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.double_endorsement($1,$2,$3,$4,$5)"
end

module Double_baking_evidence_table = struct
  let bheader = custom_of_encoding Block_header.encoding

  type t = {
    opaid : opaid;
    bh1 : Block_header.t;
    bh2 : Block_header.t;
    baker_id : kid;
    offender_id : kid;
  }
  let encoding =
    custom
      ~encode:begin fun { opaid; bh1; bh2; baker_id; offender_id; } ->
        Ok (
          (opaid, bh1, bh2, (baker_id, offender_id))
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup4 opaid bheader bheader (tup2 kid kid))
  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.double_baking($1,$2,$3,$4,$5)"
end


module Mempool_operations = struct
  type status =
    | Applied | Refused | Branch_refused | Unprocessed | Branch_delayed

  (* let operation_kind_of_int = function
   * | 0 -> "endorsement"
   * | 1 -> "seed_nonce_revelation"
   * | 2 -> "double_endorsement_evidence"
   * | 3 -> "double_baking_evidence"
   * | 4 -> "activate_account"
   * | 5 -> "proposals"
   * | 6 -> "ballot"
   * | 7 -> "reveal"
   * | 8 -> "transaction"
   * | 9 -> "origination"
   * | 10 -> "delegation"
   * | _ -> assert false *)
  (* let int_of_operation_kind = function
   * | "endorsement" -> 0
   * | "seed_nonce_revelation" -> 1
   * | "double_endorsement_evidence" -> 2
   * | "double_baking_evidence" -> 3
   * | "activate_account" -> 4
   * | "proposals" -> 5
   * | "ballot" -> 6
   * | "reveal" -> 7
   * | "transaction" -> 8
   * | "origination" -> 9
   * | "delegation" -> 10
   * | _ -> assert false *)

  type t = {
    branch: Block_hash.t;
    op_hash: Operation_hash.t;
    status: status;
    id: int;
    operation_kind: int;
    source: Signature.public_key_hash option;
    destination: Contract.t option;
    seen: float;
    json_op: string Lazy.t;
    context_block_level : int32;
  }

  let string_of_status = function
    | Applied -> "applied"
    | Refused -> "refused"
    | Branch_refused -> "branch_refused"
    | Unprocessed -> "unprocessed"
    | Branch_delayed -> "branch_delayed"
  let status_of_string = function
    | "applied" -> Some Applied
    | "refused" -> Some Refused
    | "branch_refused" -> Some Branch_refused
    | "unprocessed" -> Some Unprocessed
    | "branch_delayed" -> Some Branch_delayed
    | _ -> None


  let sql_encoding =
    custom
      ~encode:(fun {
          branch;
          op_hash;
          status;
          id;
          operation_kind;
          source;
          destination;
          seen;
          json_op;
          context_block_level;
        } -> Ok (branch,
                 context_block_level,
                 (op_hash,
                  string_of_status status,
                  id,
                  operation_kind),
                 (source,
                  destination,
                  seen,
                  Lazy.force json_op)))
      ~decode:(function
          | (branch,
             context_block_level,
             (op_hash,
              ("applied"|"refused"|"branch_refused"|"branch_delayed"|"unprocessed"
               as status),
              id,
              operation_kind),
             (source,
              destination,
              seen,
              json_op)) ->
            Ok {
              branch;
              context_block_level;
              op_hash;
              status =
                (match status_of_string status with
                 | Some s -> s
                 | _ -> assert false);
              id;
              operation_kind;
              source;
              destination;
              seen;
              json_op = lazy json_op;
            }
          | _ -> Error ("status_of_string"))
      (tup4
         bh
         int32
         (tup4 oph string int int)
         (tup4 (option pkh) (option k) float string))

  let insert =
    query_zero_or_one
      sql_encoding
      unit
      "select M.I_operation_alpha(?,?,?,?,?,?,?,?,?,?)"

end

module Seed_nonce_revelation_table = struct

  type t = {
    opaid : opaid;
    sender_id : kid;
    baker_id : kid;
    level : bl;
    nonce : Nonce.t
  }

  let encoding =
    custom
      ~encode:(fun { opaid; sender_id; baker_id; level; nonce } ->
          Ok (opaid, (sender_id, baker_id), level, nonce))
      ~decode:(fun _ -> assert false)
      (tup4 opaid (tup2 kid kid) bl nonce)
  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.seed_nonce($1,$2,$3,$4,$5)"

end


module Bigmap = struct
  let old_insert =
    query_zero_or_one
      (tup4
         (tup4
            script_expr
            script_expr_hash
            (option script_expr)
            bhid)
         (tup4
            bl
            kid
            kid
            int)
         opaid
         (option text_array)
      )
      unit
      "select B.update(null::bigint,$1,$2,$3,$4,$5,$6,$7,$8,$9,$10)"

  let update =
    query_zero_or_one
      (tup3
         (tup4
            z
            script_expr
            script_expr_hash
            (option script_expr))
         (tup4
            bhid
            bl
            kid
            kid)
         (tup3
            int
            opaid
            (option text_array)
         ))
      unit
      "select B.update($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)"

  let clear =
    query_zero_or_one
      (tup4
         z
         (tup4
            bhid
            bl
            kid
            kid)
         int
         opaid)
      unit
      "select B.clear($1,$2,$3,$4,$5,$6,$7)"

  let alloc =
    query_zero_or_one
      (tup4
         (tup4
            z
            script_expr
            script_expr
            bhid)
         (tup4
            bl
            kid
            kid
            int)
         opaid
         (option string)
      )
      unit
      "select B.alloc($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)"

  let copy =
    query_zero_or_one
      (tup4
         (tup2 z z)
         (tup4
            bhid
            bl
            kid
            kid)
         int
         opaid)
      unit
      "select B.copy($1,$2,$3,$4,$5,$6,$7,$8)"

end

module Tokens = struct

  module Contract_table = struct
    type t = {
      address : kid ;
      bhid : bhid;
    }

    let encoding =
      custom
        ~encode:(fun { address ; bhid } -> Ok (address, bhid))
        ~decode:(fun (address, bhid)-> Ok { address ; bhid })
        (tup2 kid bhid)

    let insert =
      query_zero_or_one
        encoding
        unit
        "select T.I_contract($1,$2)"

    let is_token =
      query_one
        kid
        bool
        "select T.is_token($1)"
  end

  module Balance_table = struct
    type t = {
      token_address : kid ;
      address : kid ;
      amount : int ;
    }

    let encoding =
      custom
        ~encode:(fun { token_address ; address ; amount } ->
            Ok (token_address, address, amount))
        ~decode:(fun (token_address, address, amount)->
            Ok { token_address; address ; amount })
        (tup3 kid kid int)

    let upsert =
      query_zero_or_one
        encoding
        unit
        "select T.I_balance($1,$2,$3)"
  end

  module Operation_table = struct

    type kind = Transfer | Approve | GetBalance | GetAllowance | GetTotalSupply

    type t = {
      opaid : opaid ;
      token_address : kid ;
      caller : kid ;
      kind : kind ;
    }

    let kind_encoding =
      custom
        ~encode:(function
              Transfer -> Ok "transfer"
            | Approve -> Ok "approve"
            | GetBalance -> Ok "getBalance"
            | GetAllowance -> Ok "getAllowance"
            | GetTotalSupply -> Ok "getTotalSupply")
        ~decode:(function
              "transfer" -> Ok Transfer
            | "approve" -> Ok Approve
            | "getBalance" -> Ok GetBalance
            | "getAllowance" -> Ok GetAllowance
            | "getTotalSupply" -> Ok GetTotalSupply
            | s -> Error ("unknown kind of token operation: " ^ s))
        string

    let encoding =
      custom
        ~encode:(fun { opaid ;
                       token_address ; caller ; kind } ->
                  Ok (opaid, token_address, caller, kind))
        ~decode:(fun _ -> assert false)
        (tup4 opaid kid kid kind_encoding)

    let insert =
      query_zero_or_one
        encoding
        unit
        "select T.I_operation($1,$2,$3,$4)"
  end

  module Transfer_table = struct

    type t = {
      opaid : opaid ;
      source : kid ;
      destination : kid ;
      amount : Z.t ;
    }

    let encoding =
      custom
        ~encode:(fun { opaid ;
                       source ; destination ; amount } ->
                  Ok ((opaid, source, destination, amount)))
        ~decode:(fun _ -> assert false)
        (tup4 opaid kid kid z)

    let insert =
      query_zero_or_one
        encoding
        unit
        "select T.I_transfer($1,$2,$3,$4)"

  end

  module Approve_table = struct

    type t = {
      opaid : opaid ;
      address : kid ;
      amount : Z.t ;
    }

    let encoding =
      custom
        ~encode:(fun { opaid ;
                       address ; amount } ->
                  Ok (opaid, address, amount))
        ~decode:(fun _ -> assert false)
        (tup3 opaid kid z)

    let insert =
      query_zero_or_one
        encoding
        unit
        "select T.I_approve($1,$2,$3)"

  end

  module Get_balance_table = struct

    type t = {
      opaid : opaid ;
      address : kid ;
      callback : kid ;
    }

    let encoding =
      custom
        ~encode:(fun { opaid ;
                       address ; callback } ->
                  Ok (opaid, address, callback))
        ~decode:(fun _ -> assert false)
        (tup3 opaid kid kid)

    let insert =
      query_zero_or_one
        encoding
        unit
        "select T.I_get_balance($1,$2,$3)"

  end

  module Get_allowance_table = struct

    type t = {
      opaid : opaid ;
      source : kid ;
      destination : kid ;
      callback : kid ;
    }

    let encoding =
      custom
        ~encode:(fun { opaid ;
                       source ; destination ; callback } ->
                  Ok (opaid, source, destination, callback))
        ~decode:(fun _ -> assert false)
        (tup4 opaid kid kid kid)

    let insert =
      query_zero_or_one
        encoding
        unit
        "select T.I_get_allowance($1,$2,$3,$4)"

  end

  module Get_total_supply_table = struct

    type t = {
      opaid : opaid ;
      callback : kid ;
    }

    let encoding =
      custom
        ~encode:(fun { opaid ; callback } ->
                  Ok (opaid, callback))
        ~decode:(fun _ -> assert false)
        (tup2 opaid kid)

    let insert =
      query_zero_or_one
        encoding
        unit
        "select T.I_get_total_supply($1,$2)"

  end

end

module Activation_table = struct
  let insert =
    query_zero_or_one
      (tup3 opaid kid string)
      unit
      "select I.activate(?,?,?)"
end
