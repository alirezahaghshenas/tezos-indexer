(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let proto = "(**# get PROTO #**)"

(**#X ifdef __META1__ open Tezos_raw_protocol_(**# get PROTO#**)X#**)
(**#X else open Tezos_protocol_(**# get PROTO #**) open Protocol X#**)

(* Notes: protocol transition files, including `chain_db.ml`, are generated and
   placed in the `bin_indexer` folder, where they use `bin_indexer/dune`,
   while the other `chain_db.ml` files are placed in protocol-specific
   folders, each having its own `dune` file.
   That's the first reason why transition files require different `open`s.

   Then, with protocol 5, the architecture of the modules changed. That's the
   second reason why `open`s are different for the two categories of protocols
   (before 5, and from 5).
*)


open Alpha_context

open Db_shell
open (**#X ifdef __TRANSITION__ Tezos_indexer_(**# get PROTO#**). X#**)Db_alpha
open (**#X ifdef __TRANSITION__ Tezos_indexer_(**# get PROTO#**). X#**)Db_alpha.Utils

module BS = Tezos_indexer_lib.BS.(**# get BS #**)
module Misc = Tezos_indexer_lib.Misc

module Client = Tezos_client_(**# get PROTO #**)

class type rpc_context = (**#X ifdef __META1__ Client.Alpha_client_context.full
     X#**)(**#X else Client.Protocol_client_context.full X#**)

(**# ifdef __META4__ module Delegate = Receipt #**)

module Script_utils = struct

  let bump_pairs =
    let rec bump_pairs =
      let open Tezos_micheline.Micheline in
      let open Michelson_v1_primitives in
      function
      | ( String _
        | Bytes _
        | Int _) as x -> x
      | Seq (loc, l) ->
        Seq (loc, List.map bump_pairs l)
      | Prim (loc, T_pair, l, annots) ->
        Prim (loc, T_pair, List.map bump_pairs (bump l), annots)
      | Prim (loc, i, l, annots) ->
        Prim (loc, i, List.map bump_pairs l, annots)
    and bump l =
      let open Tezos_micheline.Micheline in
      let open Michelson_v1_primitives in
      List.map
        (function
          | ( String _
            | Bytes _
            | Int _) as x -> [x]
          | Seq (loc, l) ->
            [Seq (loc, List.map bump_pairs l)]
          | Prim (_loc, T_pair, l, _annots) ->
            l
          | Prim _ as e ->
            [e]
        )
        l
      |> List.flatten
    and f r = (* run until fix point *)
      let x = bump_pairs r in
      if r = x then
        r
      else
        f x
    in
    f

  let _ = bump_pairs

  let rec compare_script x y =
    let open Tezos_micheline.Micheline in
    (* let open Michelson_v1_primitives in *)
    match x, y with
    | Int (_, a), Int (_, b) ->
      compare a b
    | String (_, a), String (_, b) ->
      compare a b
    | Bytes (_, a), Bytes (_, b) ->
      compare a b
    | Prim (loc, a, b, c), Prim (_, d, e, f) ->
      begin match compare (a, c) (d, f) with
      | 0 -> compare_script (Seq (loc, b)) (Seq (loc, e))
      | x -> x
      end
    | Seq (_, []), Seq (_, []) -> 0
    | Seq (loc1, a::l1), Seq (loc2, b::l2) ->
      begin match compare_script a b with
      | 0 -> compare_script (Seq (loc1, l1)) (Seq (loc2, l2))
      | x -> x
      end
    | _ -> compare x y


  let compare_script x y =
    if compare_script x y = 0 then
      0
    else
      compare_script (bump_pairs x) (bump_pairs y)

  let _ = compare_script

end
open Script_utils


let bigmap_debug = 0

module Verbose = struct
  include Tezos_indexer_lib.Verbose
  open Utils

  module PP = struct
    type contract = Contract.t
    let contract = Contract.pp
    type tez = Tez.t
    let tez = Tez.pp
    type nonce = Nonce.t
    let nonce out n =
      Format.fprintf out "%a"
        Data_encoding.Json.pp
        (Data_encoding.Json.construct Nonce.encoding n)
  end

  module Debug = struct
    include DebugF(PP)

    module Script = struct
      let rec pp_canonical out node =
        Format.fprintf out "%a" pp (Tezos_micheline.Micheline.root node)
      and pp out node =
        match node with
        | Tezos_micheline.Micheline.Int (_loc, z) -> Format.fprintf out "%s" (Z.to_string z)
        | String (_loc, string) -> Format.fprintf out "%S" string
        | Bytes (_loc, bytes) -> Format.fprintf out "%S" (Bytes.to_string bytes)
        | Prim (_loc, prim, node_list, (annot:string list)) ->
          let p =
            (**#X ifdef __META_23__
               (**# ifdef __TRANSITION__ let open Client in #**)
               (Michelson_v1_printer.ocaml_constructor_of_prim prim)
               X#**)
            (**# else
               (Michelson_v1_primitives.string_of_prim prim)
               #**)
          in
          if node_list = [] then
            Format.fprintf out "%s" p
          else
            begin
              Format.fprintf out "%s(" p;
              print_node_list out node_list;
              Format.fprintf out ")"
            end;
          if annot <> [] then Format.fprintf out "[annots:";
          List.iter (fun e -> Format.fprintf out "%S" e) annot;
          if annot <> [] then Format.fprintf out "]";
        | Seq (_loc, node_list) ->
          if node_list = [] then
            Format.fprintf out "{empty_seq}"
          else
            begin
              Format.fprintf out "{";
              print_node_list out node_list;
              Format.fprintf out "}"
            end
      and print_node_list out node_list =
        let print_sep : unit -> unit = match node_list with
          | _::_::_ -> fun () -> Format.fprintf out ";"
          | _ -> ignore
        in
        List.iter (fun e ->
            Format.fprintf out "%a" pp e;
            print_sep ()
          ) node_list

      let rec pp_strings_and_bytes out = function
        | Tezos_micheline.Micheline.Int (_, _) -> ()
        | String (_, string) -> Format.fprintf out "$S=%S" string
        | Bytes (_, bytes) ->
          begin
            let ascii = Hex.show (`Hex (Bytes.to_string bytes)) in
            Format.fprintf out "$H=%S" ascii;
            match Contract.of_b58check (Bytes.to_string bytes) with
            | Ok _ ->
              Format.fprintf out "$C=%S" ascii
            | Error e ->
              Format.fprintf out "$B=%S(%a)" (Bytes.to_string bytes)
                pp_print_error (Obj.magic e)
          end
        | Seq (_, node_list)
        | Prim (_, _, node_list, _) ->
          List.iter (pp_strings_and_bytes out) node_list
      let pp_strings_and_bytes_canonical out node = pp_strings_and_bytes out (Tezos_micheline.Micheline.root node)
      let pp_strings_and_bytes_script out script =
        match (script : Alpha_context.Script.t option) with
        | None -> Format.fprintf out "None"
        | Some { code ; storage } ->
          match Data_encoding.force_decode code, Data_encoding.force_decode storage with
          | None, None -> ()
          | Some _, None -> assert false
          | None, Some _ -> assert false
          | Some code, Some storage ->
            Format.fprintf out "code=%a storage=%a"
              pp_strings_and_bytes_canonical code
              pp_strings_and_bytes_canonical storage


      let print_script_and_diffs ~script ~diffs =
        (* This debug (slash "reverse engineering") function is not meant to remain here a long time,
           at least not as it was first implemented! *)
        (* FIXME: DEBUG: eventually this function will be fully removed *)
        if false then
        let () = match (script : Alpha_context.Script.t option) with
          | None -> ()
          | Some { code ; storage } ->
            (* ignore (storage: Michelson_v1_primitives.prim Tezos_micheline.Micheline.canonical Data_encoding.lazy_t); *)
            (* ignore ((Data_encoding.force_decode storage): Michelson_v1_primitives.prim Tezos_micheline.Micheline.canonical option); *)
            match Data_encoding.force_decode code, Data_encoding.force_decode storage with
            | None, None -> ()
            | Some _, None -> assert false
            | None, Some _ -> assert false
            | Some code, Some storage ->
              Format.printf "\n--------begin print script--------------------\n";
              Format.printf "\n--------print code--------------------\n";
              Format.printf "%a" pp_canonical code;
              Format.printf "\n--------print storage--------------------\n";
              Format.printf "%a" pp_canonical storage;
              Format.printf "\n--------end print script--------------------\n%!";
        in
        (
          print_endline "============================= print big map diff ======================";
          begin
            (**# ifdef __PROTO1__ let diffs = ignore diffs ; None in #**)
            (**# ifdef __META3__ let module Contract_diff = Contract.Legacy_big_map_diff in
                let diffs = Option.map (fun diffs -> (Contract_diff.of_lazy_storage_diff diffs :> Contract_diff.item list)) diffs in #**)
            (*       ignore (diffs:(**#X ifndef __META3__ (**# ifndef __TRANSITION__ Db_alpha.Contract.big_map_diff #**)(**# else _ #**) X#**)(**# else _ #**) option); *)
            match diffs with
            (**# ifdef __PROTO1__ | _ -> () #**)
            (**# elseifdef __META1__
                | None -> ()
                | Some l ->
               (List.iter: ('a -> unit) -> 'a list -> unit) (fun Contract.{ diff_key; diff_key_hash; diff_value } ->
                ignore (diff_key:Script_repr.expr);
                ignore (diff_key_hash:Script_expr_hash.t);
                ignore (diff_value:Script_repr.expr option);
                ignore (Option.iter (fun e -> Format.printf "%a" pp_canonical e) diff_value)
               ) l
               #**)
            (**#X else
                | None -> ()
                | Some l ->
               (List.iter: ('a -> unit) -> 'a list -> unit) (function
                    | Contract(**# ifdef __META3__ .Legacy_big_map_diff #**).Update { big_map ; diff_key; diff_key_hash; diff_value } ->
               print_endline "Update";
               ignore
                        (big_map, diff_key, diff_key_hash, diff_value);
                Option.iter (Format.printf "%a" pp_canonical) diff_value;
               print_endline "\n------end Update-------------"
                    | Clear big_map ->
               print_endline "Clear";
               ignore
                        big_map
                    | Copy (**# ifdef __PROTO7__ ({ src = b1 ; dst = b2 ; }) #**)(**# elseifdef __META3__ ({ src = b1 ; dst = b2 ; }) #**)(**# else (b1, b2) #**)->
               print_endline "Copy";
               ignore
                        (b1, b2)
                    | Alloc { big_map ; key_type ; value_type } ->
               print_endline "Alloc";
                     ignore
                        (big_map, key_type, value_type);
               Format.printf "---key_type-------------------------\n";
               Format.printf "%a" pp_canonical key_type;
               Format.printf "\n--value_type--------------------------\n";
               Format.printf "%a" pp_canonical value_type;
               Format.printf "\n----------------------------\n";
               match script with
               | None -> ()
               | Some script ->
                 let open Tezos_micheline.Micheline in
                 let open Michelson_v1_primitives in
                 let storage_type = Option.map root @@ Data_encoding.force_decode script.Alpha_context.Script.code in
                 Option.iter (function
                   | Seq (_, (Prim (_, K_parameter, _, _))::(Prim (_, K_storage, storage_type, _))::_code::[]) ->
                     begin match storage_type with
                     | Prim (_, T_pair, (Prim (_, T_big_map, bm_type, bm_annots) :: _), _annots) :: [] ->
                       print_endline "\nbm_type";
                       List.iter (Format.printf "%a" pp) bm_type;
                       print_endline "\nvalue_type";
                       Format.printf "%a" pp_canonical value_type;
                       List.iter (Format.printf "\nbig_map_annot:%s\n") bm_annots;
                       begin match bm_type with
                       |  [_; b] ->
                         print_endline "\nyyyyyyyyyyyyyyyy";
                         Format.printf "%a" pp b;
                         print_endline "\nyyyyyyyyyyyyyyyy";
                         Format.printf "%a" pp_canonical value_type;
                         print_endline "\nyyyyyyyyyyyyyyyy";
                         print_endline "yyyyyyyyyyyyyyyy";
                         Format.printf "%a" pp @@ bump_pairs b;
                         print_endline "\nyyyyyyyyyyyyyyyy";
                         Format.printf "%a" pp @@ bump_pairs @@ root value_type;
                         print_endline "\nyyyyyyyyyyyyyyyy";
                         (* assert (0 = compare_script b @@ root value_type) *)
                       | _ -> print_endline "zzzzzzzzzzzzzzzzz"; assert false
                       end
                     | _ -> (* assert false *) ()
                     end
                   | Tezos_micheline.Micheline.Int (_loc, _z)  as e ->
                     print_endline "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
                     Format.printf "%a" pp e
                   | String (_loc, _string)  as e ->
                     print_endline "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
                     Format.printf "%a" pp e
                   | Bytes (_loc, _bytes)  as e ->
                     print_endline "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
                     Format.printf "%a" pp e
                   | Seq (_loc, _node_list)  as e ->
                     print_endline "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
                     Format.printf "%a" pp e
                   | Prim (_loc, _prim, _node_list, (_annot:string list)) as e ->
                     print_endline "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
                     Format.printf "%a" pp e
                  )
                  storage_type
                    ) l
               X#**)
          end
          : unit
        )

    end (* of module Script *)

  end (* of module Debug *)

  module Log = struct
    module Log = Tezos_indexer_lib.Verbose.Log(PP)
    include Log

    let string_of_voting_period_kind = function
      | Voting_period.Proposal -> "proposal"
      (**# ifndef __META4__
      | Testing_vote           -> "testing_vote"
      | Testing                -> "testing"
      | Promotion_vote         -> "promotion_vote" #**)
(**# ifdef __META3__
      | Adoption       -> "adoption"
   #**)
(**# ifdef __META4__
      | Exploration    -> "exploration"
      | Cooldown       -> "cooldown"
      | Promotion      -> "promotion"
   #**)


    let _ = string_of_voting_period_kind

    let balance_update ?force ?op level bh balance_updates =
      List.iter
        (fun (balance, diff) ->
           let bt, k, cycle =
             match balance with
             | Delegate.Contract k -> "delegate", k, None
             | Rewards (pkh, cycle) -> "rewards", Contract.implicit_contract pkh, Some cycle
             | Fees (pkh, cycle) -> "fees", Contract.implicit_contract pkh, Some cycle
             | Deposits (pkh, cycle) -> "deposits", Contract.implicit_contract pkh, Some cycle
           in
             printf ?force ~vl:4
               "event=update block_level=%ld block=%a type=balance \
                contract=%a%a balance_kind=%s%a diff=%Ld"
               level
               Block_hash.pp_short bh
               Contract.pp k
               (fun out -> function
                  | None -> ()
                  | Some (op_hash, id, 0) ->
                    Format.fprintf out " op_hash=%a id=%d" Operation_hash.pp op_hash id
                  | Some (op_hash, id, internal) ->
                    Format.fprintf out " op_hash=%a id=%d internal=%d" Operation_hash.pp op_hash id internal)
               op
               bt
               (pp_option ~name:"cycle" ~pp:Cycle.pp) cycle
               (match diff with
                | Delegate.Debited v -> Int64.neg (Tez.to_mutez v)
                | Credited v -> Tez.to_mutez v
               )
        )
        balance_updates


  end

end (* of module Verbose *)


(* SQL queries have a huge cost compared to keeping things inside the process,
   therefore it should help to have quite a large maximum size for the cache.
   Emptying the cache when it's full is not a great solution, but it's simple.
   Otherwise we'd have to keep track of the recently looked-up addresses
   (not the "recently recorded" ones).
*)
let cache_size = 10_000l (* this is mainly a number of blocks *)

module Cache_cache (S: sig type t val size : int end) = struct
  let _ = assert (S.size > 0)
  let cache = Array.make S.size ((Obj.magic 0) : S.t) (* FIXME: maybe use a safe dummy value instead? *)
  let i = ref (-1)
  let add e = i := (succ !i) mod S.size; Array.set cache !i e
  let get_elements () = cache
end

let record_address =
  (* Simple cache-based optimisation to avoid putting the same address over and
     over in the address table. The size of this cache is limited by the size of
     [cache_size] blocks: the cache is reset at each [cache_size] blocks. *)
  let module T = struct type t = Contract.t * int64 let size = Int32.to_int cache_size end in
  let module C = Cache_cache (T) in
  let max_level = ref 0l in
  let module M = Stdlib.Map.Make (Contract) in
  let map = ref M.empty in
  let add e id = map := M.add e id !map ; C.add (e, id) in
  let get e = M.find_opt e !map in
  fun ~__LINE__ ?(multithreaded=false) ~addr_counter ?bhid:_ level pool conn ?k ?pkh () ->
    let k =
      match k, pkh with
      | Some k, None -> k
      | None, Some pkh -> Contract.implicit_contract pkh
      | None, None
      | Some _, Some _ -> assert false
    in
    if level > !max_level then begin
      if !max_level = 0l then map := M.empty
      else Array.iter (fun (e, id) -> add e id) (C.get_elements());
      max_level := Int32.add cache_size level;
    end;
    match get k with
    | Some id ->
      Verbose.Debug.address_already_recorded_recently ~__LINE__ level k;
      C.add ((k:Contract.t), (id:int64));
      Lwt.return id
    | None ->
      Verbose.Debug.record_address ~__LINE__ level k;
      begin
        if not multithreaded then
          let module Conn = (val conn : Caqti_lwt.CONNECTION) in
          Conn.find_opt
            Addresses.insert
            (k, addr_counter ())
        else
          Caqti_lwt.Pool.use
            (fun dbh ->
               let module Conn = (val dbh : Caqti_lwt.CONNECTION) in
               Conn.find_opt
                 Addresses.insert
                 (k, addr_counter ())
            )
            pool
      end
      >>= caqti_or_fail ~__LOC__
      >>= function
      | None ->
        Verbose.Log.eprintf ~force:true "Recording of contract address %a failed!" Contract.pp k;
        Stdlib.exit 1
      | Some id ->
        add k id;
        Lwt.return (id:int64)


module Tokens = struct
  let check_contract cctxt ~chain ~block ~contract () =
    let cctxt = (cctxt : #rpc_context :> rpc_context) in
    (**#X ifdef __META3__
    let res =
      Client.Client_proto_fa12.contract_has_fa12_interface
        cctxt ~chain ~block ~contract () in
    X#**)
    (**#X elseifdef __PROTO7__
    let res =
      Client.Client_proto_fa12.contract_has_fa12_interface
        cctxt ~chain ~block ~contract () in
    X#**)
    (**#X elseifdef __PROTO6__
    let res =
      Client.Client_proto_fa12.contract_has_fa12_interface
        cctxt ~chain ~block ~contract () in
    X#**)
    (**#X else
    let res = (fun _ _ _ _ -> fail "not implemented")
       cctxt chain block contract in
    X#**)
    res

  type action =
    (**#X ifdef __META3__ Client.Client_proto_fa12.action = X#**)
    (**#X elseifdef __PROTO7__ Client.Client_proto_fa12.action = X#**)
    (**#X elseifdef __PROTO6__ Client.Client_proto_fa12.action = X#**)
    | Transfer of Contract.t * Contract.t * Z.t
    | Approve of Contract.t * Z.t
    | Get_allowance of Contract.t * Contract.t * (Contract.t * string option)
    | Get_balance of Contract.t * (Contract.t * string option)
    | Get_total_supply of (Contract.t * string option)

  let _transfer src dst z = Transfer (src, dst, z)
  let _approve addr z = Approve (addr, z)
  let _get_allowance src dst cb = Get_allowance (src, dst, cb)
  let _get_balance addr cb = Get_balance (addr, cb)
  let _get_total_supply cb = Get_total_supply cb

  let action_of_data entrypoint data : (action, unit) result =
    let action entrypoint =
    (**#X ifdef __META34__
      let open Tezos_client_(**# get PROTO #**) in
      match Script_repr.force_decode data with
      | exception exn ->
       Printf.eprintf "Exception\n%!"; raise exn
      | Error _ -> Error ()
      | Ok (expr, _) ->
       Format.printf "Action: %s: %a\n%!" entrypoint Michelson_v1_printer.print_expr expr;
        Tezos_micheline.Micheline.root expr
        |> Client.Client_proto_fa12.action_of_expr ~entrypoint
        |> begin function
           | Error _ -> Error ()
           | Ok v -> Ok v
           end
    X#**)
    (**#X elseifdef __PROTO7__
      match Script_repr.force_decode data with
      | Error _ -> Error ()
      | Ok (expr, _) ->
        Tezos_micheline.Micheline.root expr
        |> Client.Client_proto_fa12.action_of_expr ~entrypoint
        |> begin function
           | Error _ -> Error ()
           | Ok v -> Ok v
           end
    X#**)
    (**#X elseifdef __PROTO6__
      match Script_repr.force_decode data with
      | Error _ -> Error ()
      | Ok (expr, _) ->
        Tezos_micheline.Micheline.root expr
        |> Client.Client_proto_fa12.action_of_expr ~entrypoint
        |> begin function
           | Error _ -> Error ()
           | Ok v -> Ok v
           end
    X#**)
    (**#X else (fun _ _ -> Error ()) entrypoint data  X#**)
    in
    match entrypoint with
    | Some etp -> action etp
    | None -> Error ()

  let store_action ~opaid ~addr_counter ~bhid ~contract_id ~caller_id block_level contract caller op_hash op_id action conn pool =
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    let op kind =
      Tokens.Operation_table.{
        opaid;
        token_address = contract_id;
        caller = caller_id;
        kind;
      }
    in
    match action with
    | Transfer (source, destination, amount) ->
      Verbose.Log.token_transfer
        block_level contract op_hash op_id caller source destination amount;
      Tokens.(Conn.find_opt Operation_table.insert (op Operation_table.Transfer))
      >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:source () >>= fun source_id ->
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:destination () >>= fun destination_id ->
      Tokens.Transfer_table.(
        Conn.find_opt insert
          { opaid;
            source = source_id;
            destination = destination_id;
            amount;
          })
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    | Approve (address, amount) ->
      Verbose.Log.token_approve
        block_level contract op_hash op_id caller address amount;
      Tokens.(Conn.find_opt Operation_table.insert (op Operation_table.Approve))
      >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:address () >>= fun address_id ->
      Tokens.Approve_table.(
        Conn.find_opt insert
          { opaid;
            address = address_id;
            amount;
          })
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    | Get_balance (address, (callback, _)) ->
      Verbose.Log.token_get_balance
        block_level contract op_hash op_id caller address callback;
      Tokens.(Conn.find_opt Operation_table.insert (op Operation_table.GetBalance))
      >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:address () >>= fun address_id ->
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:callback () >>= fun callback_id ->
      Tokens.Get_balance_table.(
        Conn.find_opt insert
          { opaid;
            address = address_id;
            callback = callback_id;
          })
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    | Get_allowance (source, destination, (callback, _)) ->
      Verbose.Log.token_get_allowance
        block_level contract op_hash op_id caller source destination callback;
      Tokens.(Conn.find_opt Operation_table.insert (op Operation_table.GetAllowance))
      >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:source () >>= fun source_id ->
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:destination () >>= fun destination_id ->
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:callback () >>= fun callback_id ->
      Tokens.Get_allowance_table.(
        Conn.find_opt insert
          { opaid;
            source = source_id;
            destination = destination_id;
            callback = callback_id;
          })
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    | Get_total_supply (callback, _) ->
      Verbose.Log.token_get_total_supply
        block_level contract op_hash op_id caller callback;
      Tokens.(Conn.find_opt Operation_table.insert (op Operation_table.GetTotalSupply))
      >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:callback () >>= fun callback_id ->
      Tokens.Get_total_supply_table.(
        Conn.find_opt insert
          { opaid;
            callback = callback_id;
          })
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit

  let store_token_op ~opaid ~addr_counter
      ~bhid ~source_id ~contract_id ~cctxt ~bh
      block_level contract source op_hash op_id entrypoint data conn pool =
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    begin
      if not !Config.multicore_mode then
        Conn.find Tokens.Contract_table.is_token contract_id
        >>= caqti_or_fail ~__LOC__
      else
        check_contract
          cctxt ~chain:cctxt#chain ~block:(`Hash (bh, 0)) ~contract ()
        >>= function Ok () -> Lwt.return_true | _ -> Lwt.return_false
    end >>= function
    | true ->
      begin
        match action_of_data entrypoint data with
        | Error _ -> return ()
        | Ok action ->
          store_action ~opaid ~addr_counter ~caller_id:source_id ~contract_id
            ~bhid block_level contract source op_hash op_id action conn pool
      end
    | false -> return ()

  let store_token_contract ~addr_counter ~chain ~block_level ~bhid bh cctxt contract conn pool =
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    check_contract
      cctxt ~chain ~block:(`Hash (bh, 0)) ~contract ()
    >>= function
    | Ok () ->
      Verbose.Log.token_contract block_level contract bh bhid;
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:contract () >>= fun contract_id ->
      Tokens.Contract_table.(Conn.find_opt insert {address = contract_id; bhid})
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    | Error _ -> return_unit

end



let get_balance_from_cctxt cctxt bh k =
  Alpha_services.Contract.info (* CCTX *)  (* FIXME: is there any lighter call?! *)
    cctxt ((`Main, `Hash ((bh:Block_hash.t), 0)):Block_services.chain * Block_services.block) k >>=? fun
    { (**#X ifdef __META1__ manager = _ ; spendable = _ ; X#**)
      balance ; delegate = _ ; counter = _ ; script = _ ;
    } ->
  return balance

let record_contract_balance conn (cctxt:#Tezos_rpc.RPC_context.simple) ~bhid ~bh ~level ~k ~kid ~db_max_level : _ Lwt.t =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  get_balance_from_cctxt cctxt bh k >>= function
  | Ok (balance) ->
    Verbose.Log.contract_update_balance ~bh ~level ~k ~balance ();
    (if Int32.add level 10l < db_max_level then
       Conn.find_opt Contract_balance_table.update (kid, bhid, balance, None (* placeholder for script *))
     else
       Conn.find_opt Contract_balance_table.update_via_bh (kid, bh, balance, None (* placeholder for script *))
    )
    >>= caqti_or_fail ~__LOC__ >>= fun _ ->
    return_unit
  | Error (((RPC_client_errors.Request_failed {uri=_; error=Connection_failed _; meth=_})::_) as e) ->
    Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
    Verbose.error "Connection to node failed, so I'll exit. Details: %a"
      pp_print_error e;
    Stdlib.exit 1
  | Error (((RPC_client_errors.Request_failed {uri=_; error=Unexpected_content _; meth=_})::_) as em) as e ->
    Verbose.Debug.eprintf ~vl:4
      "Couldn't get balance of %a (contract_id=%Ld) from context of block=%a, I might be using the wrong protocol, I'll try another one."
      Contract.pp k kid Block_hash.pp bh;
    Verbose.Debug.eprintf ~vl:1 "Error @ %s" __LOC__;
    Verbose.Debug.eprintf ~vl:5 "Error was: %a" pp_print_error em;
    Lwt.return e
  | Error ([RPC_context.Not_found _] as em) ->
    Verbose.Debug.eprintf ~vl:4
      "Couldn't get balance of %a (contract_id=%Ld) from context of block=%a, contract probably not actually originated. Will assign balance of 0."
      Contract.pp k kid Block_hash.pp bh;
    Verbose.Debug.eprintf ~vl:1 "Error @ %s" __LOC__;
    Verbose.Debug.eprintf ~vl:5 "Error was: %a" pp_print_error em;
    let balance : Tez.t = Alpha_context.Tez.zero in
    Verbose.Log.contract_update_balance ~bh ~level ~k ~balance ();
    (if Int32.add level 10l < db_max_level then
       Conn.find_opt Contract_balance_table.update (kid, bhid, balance, None (* placeholder for script *))
     else
       Conn.find_opt Contract_balance_table.update_via_bh (kid, bh, balance, None (* placeholder for script *))
    )
    >>= caqti_or_fail ~__LOC__ >>= fun _ ->
    return_unit
  | Error em as e ->
    Verbose.Debug.eprintf ~vl:1 "Error @ %s" __LOC__;
    Verbose.Debug.eprintf ~vl:3
      "Couldn't get balance of %a (contract_id=%Ld) from context of block=%a"
      Contract.pp k kid Block_hash.pp bh;
    Verbose.Debug.eprintf ~vl:5 "Error was: %a" pp_print_error em;
    Lwt.return e

(**# ifdef __TRANSITION__ let _ = record_contract_balance #**)


module PreBalanceCache = struct
  module S = Set.Make (Contract)
  let current_block : int32 option ref = ref None
  let cache = ref S.empty
  let seen bhid k =
    match !current_block with
    | Some h when bhid = h ->
      if S.mem k !cache then
        true
      else
        begin
          current_block := Some bhid;
          cache := S.add k !cache;
          false
        end
    | _ ->
      begin
        current_block := Some bhid;
        cache := S.add k S.empty;
        false
      end
end

let pre_record_contract_balance ~addr_counter ?kid conn pool ~bhid ~bh ~level ~k : _ Lwt.t =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  if PreBalanceCache.seen bhid k then
    Lwt.return_unit
  else
    begin
      begin
        match kid with
        | None ->
          record_address ~__LINE__ ~bhid ~addr_counter level pool conn ~k ()
        | Some kid ->
          Lwt.return kid
      end >>= fun kid ->
      Verbose.Log.contract_update_balance ~bh ~level ~k ();
      Conn.find_opt Contract_balance_table.pre_insert_balance (kid, bhid, level)
      >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      Lwt.return_unit
    end



let store_block =
  let open Apply_results in
  fun
    (* ~depth *)
    conn pool bh
    ({ shell ; protocol_data = _ } : BS.raw_block_header)
    ({ protocol_data =
         { baker;
           (**# ifdef __PROTO10__
              liquidity_baking_escape_ema = _ ; implicit_operations_results = _;
              level_info = {
                level;
                level_position;
                cycle;
                cycle_position;
                expected_commitment = _;
              };
              #**)
           (**# elseifdef __META3__ level_info = _; #**)
           (**#X ifndef __PROTO10__
             level = ({ level_position;
                      cycle; cycle_position;
                      voting_period (**# ifdef __META3__ = _ #**);
                      voting_period_position;
                      level;
                      expected_commitment = _;
                    } (* : Alpha_context.Level.t *));
           voting_period_kind;
             X#**)
           (**# ifdef __META_34__ voting_period_info; #**)
           consumed_gas ;
           deactivated ;
           balance_updates ;
           nonce_hash = _;};
       test_chain_status = _;
       max_operations_ttl = _;
       max_operation_data_length = _;
       max_block_header_length = _;
       operation_list_quota = _;
     } : BS.block_metadata) ->
    (**# ifdef __PROTO10__
       let voting_period_position = voting_period_info.position in
       let voting_period_kind = voting_period_info.Voting_period.voting_period.kind in
#**)

    (**# ifndef __MILLIGAS__
    let consumed_gas = Fpgas.t_of_z consumed_gas in
    #**)
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    record_address ~__LINE__
      (* this address will be recorded ****here**** only when indexing on multicore mode (by segments) and
         the baker's address hasn't been recorded yet. It can actually happen. *)
      ~addr_counter:(fun () -> Int64.add Misc.min_int53 (Int64.of_int32 shell.level))
      shell.level pool conn ~pkh:baker () >>= fun baker_id ->
    (* Store shell header *)
    begin (* logged *)
      Verbose.Log.shell bh shell;
      Conn.find Block_table.insert (bh, shell)
    end >>= caqti_or_fail ~__LOC__ >>= function
    | None ->
      Verbose.Log.eprintf ~force:true "Insertion of block %a %ld failed!"
        Block_hash.pp bh
        (Raw_level.to_int32 level);
      Stdlib.exit 1
    | Some (bhid:int32) ->
      let addr_counter =
        let addr_counter_prefix =
          (* this limits the number of operations per block at about 99M *)
          (* this also makes removing *all* reorg-involved blocks mandatory *)
          Int64.mul (Int64.of_int32 bhid) (10_000_000L) in
        Misc.make_counter64 ~start:addr_counter_prefix ()
      in
      (* Discover deactivated pkhs and fill up deactivated table *)
      begin (* self logged / logged *)
        Lwt_list.iter_s (fun pkh ->
            record_address ~__LINE__ ~bhid ~addr_counter shell.level pool conn ~pkh () >>= fun kid ->
            Verbose.Log.deactivated_delegate_table ~block_level:shell.level bh pkh;
            Conn.find_opt Deactivated_delegate_table.insert (kid, bhid) >>= caqti_or_fail ~__LOC__ >>= fun _ ->
            Lwt.return_unit
          ) deactivated

      end >>= fun () ->
      (* Store alpha header *)
      begin (* logged *)
        Verbose.Log.store_alpha_header bh shell.level baker level_position
          (Cycle.to_int32 cycle) cycle_position
          (**# ifdef __META3__
             (fun out vp -> Format.fprintf out "{%s}" @@ Format.asprintf "%a" Voting_period.pp vp)
             voting_period_info.voting_period #**)(**# else Voting_period.pp voting_period #**)
          voting_period_position
          (Verbose.Log.string_of_voting_period_kind voting_period_kind)
          ((**# ifdef __PROTO8__ Fpgas.p8_to_p7 #**)(**# ifdef __PROTO9__ Fpgas.p9_to_p7 #**)(**# ifdef __PROTO10__ Fpgas.p10_to_p7 #**)
            consumed_gas);
        (* FIXME *)
        Verbose.Log.printf ~vl:1 "store block alpha";
        Conn.find_opt Block_alpha_table.insert
          ((bhid, baker_id, level_position, cycle),
           (cycle_position, (**# ifdef __META3__ voting_period_info.#**)voting_period, voting_period_position, voting_period_kind),
           (**# ifdef __PROTO8__ Fpgas.p8_to_p7 #**)(**# ifdef __PROTO9__ Fpgas.p9_to_p7 #**)(**# ifdef __PROTO10__ Fpgas.p10_to_p7 #**) consumed_gas)
      end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      (* Update balances tables *)
      begin (* logged *)
        (**# ifdef __META4__ let balance_updates = List.map (fun (a,b,_) -> a,b) balance_updates in #**)
        Verbose.Log.balance_update shell.level bh balance_updates;
        let get_kid k = record_address ~__LINE__ ~bhid ~addr_counter shell.level pool conn ~k () in
        Balance_table.update ~get_kid conn ~bhid balance_updates
      end >>= fun () ->
      return (bhid, addr_counter, baker_id)




let int_of_contents : type a. a contents -> int = function
  | Endorsement _ -> 0
  | Seed_nonce_revelation _ -> 1
  | Double_endorsement_evidence _ -> 2
  | Double_baking_evidence _ -> 3
  | Activate_account _ -> 4
  | Proposals _ -> 5
  | Ballot _ -> 6
  | Manager_operation { operation; _ } ->
    begin
      match operation with
      | Reveal _ -> 7
      | Transaction _ -> 8
      | Origination _ -> 9
      | Delegation _ -> 10
    end
(**# ifdef __META4__
  | Endorsement_with_slot _ -> 11
  | Failing_noop _ -> 12
#**)

let int_of_manager_operation : type a. a manager_operation -> _ = function
  | Reveal _ -> 7
  | Transaction _ -> 8
  | Origination _ -> 9
  | Delegation _ -> 10

let string_of_contents : type a. a contents -> string = function
  | Endorsement _ -> "Endorsement"
  | Seed_nonce_revelation _ -> "Seed_nonce_revelation"
  | Double_endorsement_evidence _ -> "Double_endorsement_evidence"
  | Double_baking_evidence _ -> "Double_baking_evidence"
  | Activate_account _ -> "Activate_account"
  | Proposals _ -> "Proposals"
  | Ballot _ -> "Ballot"
  | Manager_operation { operation; _ } ->
    begin
      match operation with
      | Reveal _ -> "Reveal"
      | Transaction _ -> "Transaction"
      | Origination _ -> "Origination"
      | Delegation _ -> "Delegation"
    end
(**# ifdef __META4__
  | Endorsement_with_slot _ -> "Endorsement_with_slot"
  | Failing_noop _ -> "Failing_noop"
#**)

(* let list_of_option_list =
 *   List.fold_left (fun r -> function
 *       | None -> r
 *       | Some e -> e :: r)
 *     [] *)

let rec list_iter_es f = function
  | [] -> return_unit
  | e :: tl ->
    f e >>= function
    | Error e -> Lwt.return (Error e)
    | Ok () -> list_iter_es f tl

module type Manager_operation =
sig
  val process :
    (module Caqti_lwt.CONNECTION) ->
    ((module Caqti_lwt.CONNECTION),
     [< Caqti_error.t
          > `Decode_rejected `Encode_failed `Encode_rejected `Request_failed
          `Request_rejected `Response_failed `Response_rejected ])
      Caqti_lwt.Pool.t ->
    #rpc_context ->
    opaid:int64 ->
    bigmap_counter:(unit -> int) ->
    addr_counter:(unit -> int64) ->
    tokens:bool ->
    block_level:bl ->
    bhid:bhid ->
    ?nonce:int ->
    internal:int ->
    source_id:kid ->
    Block_hash.t ->
    Operation_hash.t ->
    int ->
    Contract.t ->
    Tez.tez ->
    'a manager_operation ->
    'b Apply_results.manager_operation_result ->
    unit tzresult Lwt.t
end

let error_list_to_json (l: _ option) =
  let open Tezos_protocol_environment_(**# get PROTO #**) in
  let enc =
    Data_encoding.list
    (**# ifdef __META4__ error_encoding #**)
    (**# else Environment.Error_monad.error_encoding #**) in
  Option.map
    (fun e ->
       Data_encoding.Json.(
         Format.asprintf "%a" pp
           (construct enc
              ((**#X ifdef __META4__ Environment.wrap_tztrace X#**) e))
       )
    )
    l



module Manager_operation : Manager_operation = struct

  let process_tx
      conn pool
      ~opaid
      ~addr_counter
      ~bhid ~source_id ~destination_id
      ~block_level ~bh ~source ~op_hash ~op_id ~fee
      ~internal ~nonce
      (**# ifdef __META2__ ?entrypoint #**)
      (**# ifdef __META3__ ?entrypoint #**)
      ~consumed_gas ~storage_size ~paid_storage_size_diff
      ~amount ~destination ~balance_updates
      ~originated_contracts ~parameters
      ~storage
      ~status
      ~error_list
    =

    (* begin
     *   Option.iter (
     *     Option.iter (fun parameters ->
     *         Verbose.Debug.printf ~vl:bigmap_debug "TX parameters:%a"
     *           Verbose.Debug.Script.pp_strings_and_bytes_canonical parameters
     *       ))
     *     (Option.map Data_encoding.force_decode parameters)
     * end;
     * begin
     *   Option.iter (
     *     Option.iter (fun storage ->
     *         Verbose.Debug.printf ~vl:bigmap_debug "TX storage:%a"
     *           Verbose.Debug.Script.pp_strings_and_bytes_canonical storage
     *       ))
     *     (Option.map Data_encoding.force_decode storage)
     * end; *)

    let strings =
      match Misc.flatten_option @@ Option.map extract_strings_and_bytes_from_lazy_expr parameters
          , Misc.flatten_option @@ Option.map extract_strings_and_bytes_from_lazy_expr storage
      with
      | None, None -> None
      | Some l, Some l2 ->
        Some (l @ l2)
      | Some l, None
      | None, Some l -> Some l
    in

    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    begin (* logged *)
      Lwt_list.iter_s
        (fun k ->
           record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k () >>= fun kid ->
           begin (* self logged *)
             pre_record_contract_balance ~addr_counter ~kid conn pool ~bhid ~bh ~level:block_level ~k
           end
        )
        originated_contracts
    end >>= fun _ ->
    begin (* logged *)
      (* Store TX (or update but does NOT overwrite existing data), key is (op_hash, op_id) *)
      (**# ifdef __META1__ let entrypoint = None in #**)
      Verbose.Log.store_tx block_level bh op_hash op_id
        ~source ~destination fee amount parameters
        storage consumed_gas storage_size paid_storage_size_diff entrypoint;
      Conn.find_opt Tx_table.insert
        Tx_table.{ opaid ; source = source_id ; destination = destination_id ;
                   fee ; amount ; parameters ; storage ;
                   consumed_gas ; storage_size ; paid_storage_size_diff ;
                   entrypoint ;
                   nonce ;
                   status ;
                   error_list ;
                   strings ;
                 }
    end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
    (* Update balances *)
    begin (* logged *)
      Verbose.Log.balance_update ~op:(op_hash, op_id, internal) block_level bh balance_updates;
      let get_kid k = record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k () in
      Balance_table.update ~get_kid ~tally:decr
        ~opaid conn ~bhid balance_updates
    end >>= fun () ->
    return_unit

  let bigmap_annots ~script ~value_type ~i : string list option =
    match script with
    | None -> None
    | Some script ->
      let open Tezos_micheline.Micheline in
      let open Michelson_v1_primitives in
      let code = Option.map root @@ Data_encoding.force_decode script.Script.code in
      match code with
      | None -> assert false
      | Some code ->
        let rec get_storage_type = function
          | Seq (_, (Prim (_, K_storage, storage_type, _))::_) -> Some storage_type
          | Seq (loc, _::e) -> get_storage_type (Seq (loc, e))
          | Seq (_, [])
          | String _
          | Bytes _
          | Prim _
          | Int _ -> None
        in
        let counter = ref 0 in
        (* * [i] is meant to match [!counter]
           * for some reason, we need to count in the opposite direction
        *)
        let rec get_annots ~rvt = function
          | [] -> None
          | Prim (_, T_big_map, bm_type, bm_annots) :: e ->
            begin
              match get_annots ~rvt e with
              | Some _ as r -> r
              | None ->
                match
                  List.find_opt
                    (fun b -> 0 = compare_script b rvt)
                    (match bm_type with _::tl -> tl | _ -> [])
                with
                | Some _ ->
                  Verbose.Debug.printf ~vl:bigmap_debug "bigmap alloc index %d; big map type index %d" i !counter;
                  Some bm_annots
                | None -> incr counter; None
            end
          | Int _ :: e
          | String _ :: e
          | Bytes _ :: e -> get_annots ~rvt e
          | Prim (_, _, node_list, _) :: e
          | Seq (_, node_list) :: e ->
            match get_annots ~rvt e with
            | None -> get_annots ~rvt node_list
            | Some _ as r -> r
        in
        match get_storage_type code with
        | None -> None
        | Some storage_type -> get_annots ~rvt:(root value_type) storage_type

  let _ = bigmap_annots


  let process_bigmap_diffs :
    ?script:_ ->
    (module Caqti_lwt.CONNECTION) ->
    bigmap_counter:_ ->
    block_level:bl ->
    bh:Block_hash.t ->
    bhid:bhid ->
    opaid:opaid ->
    sender:Contract.t ->
    contract:Contract.t ->
    senderid:kid ->
    contract_id:kid ->
    diffs:(**# ifdef __BIG_MAP_DIFF__ (Contract.big_map_diff option) #**) (**# elseifdef __STORAGE_DIFF__ (Lazy_storage.diffs option) #**)(**#X else unit X#**) ->
    unit Lwt.t =
    fun ?script conn ~bigmap_counter ~block_level ~bh ~bhid ~opaid ~sender ~contract ~senderid ~contract_id ~diffs ->

    if Verbose.debug.contents then
      begin
        Verbose.Debug.printf ~vl:bigmap_debug "big map contract: %a" Contract.pp contract;
        Verbose.Debug.Script.print_script_and_diffs ~script ~diffs;
      end;

    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    let _ = bh, sender, contract in (* FIXME: log me! *)
    (**# ifdef __PROTO1__
       (* There is no big_map_diff in proto 1 *)
       (* avoid unused variable warnings in proto_001 *)
       let _ = conn, block_level, bh, bhid, opaid, sender, contract, senderid, contract_id, diffs, bigmap_counter in
       Lwt.return_unit #**)
    (**# elseifdef __META1__
           begin match diffs with
            | None -> Lwt.return_unit
            | Some l ->
              Lwt_list.iter_s (fun Contract.{ diff_key; diff_key_hash; diff_value } ->
                Conn.find_opt
                  Bigmap.old_insert
                  ((diff_key, diff_key_hash, diff_value, bhid)
                   , (block_level, senderid, contract_id, bigmap_counter())
                   , opaid
                   , Misc.flatten_option (Option.map extract_strings_and_bytes_from_expr diff_value))
                >>= Db_shell.caqti_or_fail ~__LOC__ >>= fun _ ->
                Lwt.return_unit
              ) l
           end #**)
    (**#X else
       begin match diffs with
        | None -> Lwt.return_unit
        | Some l ->
          (**# ifdef __META3__ let module Contract_diff = Contract.Legacy_big_map_diff in
          let l = (Contract_diff.of_lazy_storage_diff l :> Contract_diff.item list) in #**)
          let i = ref (-1) in
          Lwt_list.iter_s (fun e ->
          match e with
            | Contract(**# ifdef __META3__ .Legacy_big_map_diff #**).Update { big_map ; diff_key; diff_key_hash; diff_value } ->
              Conn.find_opt
                Bigmap.update
                ((big_map, diff_key, diff_key_hash, diff_value), (bhid, block_level, senderid, contract_id),
                 (bigmap_counter(), opaid, Misc.flatten_option (Option.map extract_strings_and_bytes_from_expr diff_value)))
              >>= Db_shell.caqti_or_fail ~__LOC__ >>= fun _ ->
              Lwt.return_unit
            | Clear big_map ->
              Conn.find_opt
                Bigmap.clear
                (big_map, (bhid, block_level, senderid, contract_id), bigmap_counter(), opaid)
              >>= Db_shell.caqti_or_fail ~__LOC__ >>= fun _ ->
              Lwt.return_unit
            | Copy (**# ifdef __PROTO7__ ({ src = b1 ; dst = b2 ; }) #**)(**# elseifdef __META3__ ({ src = b1 ; dst = b2 ; }) #**)(**# else (b1, b2) #**)->
              Conn.find_opt
                Bigmap.copy
                ((b1, b2), (bhid, block_level, senderid, contract_id), bigmap_counter(), opaid)
              >>= Db_shell.caqti_or_fail ~__LOC__ >>= fun _ ->
              Lwt.return_unit
            | Alloc { big_map ; key_type ; value_type } ->
              incr i;
              let annots =
                begin match bigmap_annots ~i:i.contents ~script ~value_type with
                | None | Some [] ->
                  Verbose.Debug.printf ~vl:bigmap_debug "big map annot not found for %a"
                    Verbose.Debug.Script.pp_canonical value_type;
                  None
                | Some (e::[]) ->
                  Verbose.Debug.printf ~vl:bigmap_debug "bigmap_annot: %s\n%!" e;
                  Some e
                | _ -> assert false
              end
              in
              Conn.find_opt
                Bigmap.alloc
                ((big_map, key_type, value_type, bhid), (block_level, senderid, contract_id, bigmap_counter()), opaid, annots)
              >>= Db_shell.caqti_or_fail ~__LOC__ >>= fun _ ->
              Lwt.return_unit
            ) l
       end X#**)

  type operation_status =
    | Applied
    | Backtracked
    | Failed
    | Skipped

  let int_of_status = function
    | Applied -> 0
    | Backtracked -> 1
    | Failed -> 2
    | Skipped -> 3

  let process_origination
      conn pool
      ~opaid
      ~bigmap_counter
      ~addr_counter
      ~bhid ~preorigination_id ~source_id
      ~block_level ~bh ~source ~op_hash ~op_id ~fee ~tokens ~cctxt
      ~internal ~nonce
      ~delegate:_ ~delegate_id (**# ifdef __META1__ ~manager ~manager_id ~spendable ~delegatable #**)
      credit preorigination script balance_updates originated_contracts
      (**# ifdef __META2__ ~big_map_diff #**)
      (**# ifdef __META3__ ~lazy_storage_diff #**)
      ~consumed_gas ~storage_size ~paid_storage_size_diff
      ~status
      ~error_list
    =

    begin
      Verbose.Debug.printf ~vl:bigmap_debug "Or script:%a"
        Verbose.Debug.Script.pp_strings_and_bytes_script script
    end;



    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    (**# ifdef __META1__ let manager_id = Some manager_id in  #**)
    (**# ifdef __META2__ let spendable = None and delegatable = None and manager = None and manager_id = None in  #**)
    (**# elseifdef __META3__ let spendable = None and delegatable = None and manager = None and manager_id = None in  #**)
    (**# else let spendable = Some spendable and delegatable = Some delegatable in  #**)
    let originated_contract = match originated_contracts with
      | [hd] -> Some hd
      | [] -> None
      | _ -> assert false
    in
    (* Insert full contract row *)
    begin match originated_contract with
      | None ->
        Lwt.return_none
      | Some originated_contract ->
        record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:originated_contract ()
        >>= fun okid ->
        begin (* self logged *)
          pre_record_contract_balance ~addr_counter ~kid:okid conn pool ~bhid ~bh ~level:block_level ~k:originated_contract
        end >>= fun () ->
        Lwt.return_some okid
    end >>= fun okid ->
    (* Insert in origination table *)
    ignore (delegatable, spendable, manager, preorigination); (* FIXME *)
    begin (* logged *)
      Verbose.Log.insert_origination block_level bh op_hash op_id source originated_contract fee;
      Conn.find_opt Origination_table.insert {
        opaid ;
        src = source_id ;
        k = okid ;
        fee ;
        consumed_gas ; storage_size ; paid_storage_size_diff ;
        nonce ;
        preorigination_id ;
        script ;
        delegate_id;
        credit ;
        manager_id ;
        bhid ;
        status ;
        error_list ;
        strings = Misc.flatten_option (Option.map extract_strings_and_bytes script) ;
      }
    end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
    (* Update balances *)
    begin (* logged *)
      Verbose.Log.balance_update ~op:(op_hash, op_id, internal) block_level bh balance_updates;
      let get_kid k = record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k () in
      Balance_table.update ~get_kid ~tally:decr
        ~opaid conn ~bhid balance_updates
    end >>= fun () ->
    Verbose.Log.store_origination block_level bh source;
    begin (* self logged *)
      match tokens && status = int_of_status Applied, originated_contract with
      | (false, _) | (_, None) -> return ()
      | true, Some originated_contract ->
        Tokens.store_token_contract
          ~addr_counter
          ~chain:cctxt#chain ~block_level ~bhid bh cctxt originated_contract conn pool
    end >>= caqti_or_fail ~__LOC__ >>= fun () ->
    begin
      let diffs =
        (**# ifdef __META2__ big_map_diff #**)(**# elseifdef __META3__ lazy_storage_diff #**)(**# elseifdef __BIG_MAP_DIFF__ None #**)(**# elseifdef __STORAGE_DIFF__ None #**)(**#X else () X#**)
      in
      match originated_contract, okid with
      | _, None | None, _ -> Lwt.return_unit
      | Some originated_contract, Some okid  ->
        if status = int_of_status Applied then
          process_bigmap_diffs
            ?script
            conn
            ~bigmap_counter
            ~block_level
            ~bh
            ~bhid
            ~opaid
            ~sender:source
            ~contract:originated_contract
            ~senderid:source_id
            ~contract_id:okid
            ~diffs
        else
          Lwt.return_unit
    end >>= fun () ->
    return_unit

  let process :
    type a b.
    (module Caqti_lwt.CONNECTION) ->
    ((module Caqti_lwt.CONNECTION),
     [< Caqti_error.t
          > `Decode_rejected `Encode_failed `Encode_rejected `Request_failed
          `Request_rejected `Response_failed `Response_rejected ])
      Caqti_lwt.Pool.t ->
    #rpc_context ->
    opaid:_ ->
    bigmap_counter:_ ->
    addr_counter:_ ->
    tokens:bool ->
    block_level:bl ->
    bhid:bhid ->
    ?nonce:_ ->
    internal:int ->
    source_id:kid ->
    Block_hash.t -> Operation_hash.t -> int -> Contract.t -> Tez.t ->
    a manager_operation ->
    b Apply_results.manager_operation_result -> unit tzresult Lwt.t =
    fun conn pool cctxt
      ~opaid
      ~bigmap_counter ~addr_counter
      ~tokens ~block_level ~bhid ?nonce ~internal ~source_id
      bh op_hash op_id source fee operation operation_result ->
      Verbose.Log.process_mgr_operation block_level bh op_hash op_id source fee;
      let module Conn = (val conn : Caqti_lwt.CONNECTION) in
      match operation with
      | Transaction { amount ; destination ; parameters ;
                      (**# ifdef __META2__ entrypoint #**)
                      (**# ifdef __META3__ entrypoint #**) } ->
        let status, error_list, balance_updates, originated_contracts, storage
            , (consumed_gas : _ option), storage_size, paid_storage_size_diff
            (**# ifdef __BIG_MAP_DIFF__ , big_map_diff #**)
            (**# ifdef __STORAGE_DIFF__ , lazy_storage_diff #**) =
          match operation_result with
          | Applied (Apply_results.Transaction_result
                       { balance_updates ; originated_contracts ; storage ;
                         (**# ifdef __BIG_MAP_DIFF__ big_map_diff ; (* 1 *) #**)
                         (**# ifdef __STORAGE_DIFF__ lazy_storage_diff ; #**)
                         consumed_gas ; storage_size ; paid_storage_size_diff ;
                         (**# ifdef __PROTO1__ #**)
                         (**# elseifdef __PROTO2__ #**)
                         (**# else allocated_destination_contract = (true|false) ; #**)
                       }) ->
            (**# ifndef __MILLIGAS__
               let consumed_gas = Fpgas.t_of_z consumed_gas in
               #**)
            (**# ifdef __PROTO8__ let consumed_gas = Fpgas.p8_to_p7 consumed_gas in #**)
            (**# ifdef __PROTO9__ let consumed_gas = Fpgas.p9_to_p7 consumed_gas in #**)
            (**# ifdef __PROTO10__ let consumed_gas = Fpgas.p10_to_p7 consumed_gas in #**)
            (int_of_status Applied, None, balance_updates, originated_contracts, storage
            , Some consumed_gas, Some storage_size, Some paid_storage_size_diff
            (**# ifdef __BIG_MAP_DIFF__ , big_map_diff #**)
            (**# ifdef __STORAGE_DIFF__ , lazy_storage_diff #**))
          | Backtracked (Apply_results.Transaction_result
                           { balance_updates ; originated_contracts ; storage ;
                             (**# ifdef __BIG_MAP_DIFF__ big_map_diff ; (* 2 *) #**)
                             (**# ifdef __STORAGE_DIFF__ lazy_storage_diff ; #**)
                             consumed_gas ; storage_size ; paid_storage_size_diff ;
                             (**# ifdef __PROTO1__ #**)
                             (**# elseifdef __PROTO2__ #**)
                             (**# else allocated_destination_contract = (true|false) ; #**)
                           }, error_list_option) ->
            (**# ifndef __MILLIGAS__
               let consumed_gas = Fpgas.t_of_z consumed_gas in
               #**)
            (**# ifdef __PROTO8__ let consumed_gas = Fpgas.p8_to_p7 consumed_gas in #**)
            (**# ifdef __PROTO9__ let consumed_gas = Fpgas.p9_to_p7 consumed_gas in #**)
            (**# ifdef __PROTO10__ let consumed_gas = Fpgas.p10_to_p7 consumed_gas in #**)
            (int_of_status Backtracked, error_list_option, balance_updates, originated_contracts
            , storage, Some consumed_gas, Some storage_size, Some paid_storage_size_diff
              (**# ifdef __BIG_MAP_DIFF__ , big_map_diff #**)
              (**# ifdef __STORAGE_DIFF__ , lazy_storage_diff #**))
          | Failed (Transaction_manager_kind, error_list) ->
            (int_of_status Failed, Some error_list, []
            , [], None, None, None, None
              (**# ifdef __BIG_MAP_DIFF__ , None #**)
              (**# ifdef __STORAGE_DIFF__ , None #**))
          | Skipped Transaction_manager_kind ->
            (int_of_status Skipped, None, []
            , [], None, None, None, None
              (**# ifdef __BIG_MAP_DIFF__ , None #**)
              (**# ifdef __STORAGE_DIFF__ , None #**))
          | _ -> assert false
        in
        record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:destination ()
        >>= fun destination_id ->
        begin
          if status = int_of_status Applied then
            begin
              process_bigmap_diffs
                conn
                ~bigmap_counter
                ~block_level
                ~bh
                ~bhid
                ~opaid
                ~sender:source
                ~contract:destination
                ~senderid:source_id
                ~contract_id:destination_id
                ~diffs:(**# ifdef __BIG_MAP_DIFF__ big_map_diff #**) (**# elseifdef __STORAGE_DIFF__ lazy_storage_diff #**)(**#X else () X#**)
            end >>= fun () ->
            begin (* self logged *)
              if tokens then
                let entrypoint =
                  (**# ifdef __META2__ Some entrypoint #**)(**# elseifdef __META3__ Some entrypoint #**)
                  (**# else None #**)
                in
                Tokens.store_token_op ~cctxt ~opaid ~addr_counter ~bhid ~bh ~source_id ~contract_id:destination_id
                  block_level destination source op_hash op_id entrypoint parameters conn pool
              else return ()
            end >>= caqti_or_fail ~__LOC__
          else
            Lwt.return_unit
        end >>= fun () ->
        (**# ifndef __META1__ let parameters = Some parameters in #**)
        let storage =
          match storage with
          | Some storage -> Some (Script.lazy_expr storage)
          | None -> None in
        begin (* self logged *)
          (**# ifdef __META4__ let balance_updates = List.map (fun (a,b,_) -> a,b) balance_updates in #**)
          process_tx
            conn pool
            ~opaid
            ~addr_counter
            ~bhid ~destination_id
            ~nonce ~internal
            ~block_level ~bh ~source ~source_id ~op_hash ~op_id ~fee
            ~consumed_gas:(consumed_gas : _ option)
            ~storage_size ~paid_storage_size_diff
            ~amount ~destination
            (**# ifdef __META2__ ~entrypoint #**)(**# ifdef __META3__ ~entrypoint #**)
            ~balance_updates ~originated_contracts ~parameters
            ~storage
            ~status
            ~error_list:(error_list_to_json error_list)
        end
      | Origination { (**# ifdef __META1__ manager; spendable; delegatable; #**)
          delegate; credit; preorigination ; script } ->
        let status, error_list, balance_updates, originated_contracts
            , consumed_gas, storage_size, paid_storage_size_diff
            (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ , big_map_diff (* 3 *) #**) X#**)
            (**# ifdef __STORAGE_DIFF__ , lazy_storage_diff #**) =
          match operation_result with
          | Applied (Apply_results.Origination_result
                       { balance_updates ; originated_contracts ;
                         (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ big_map_diff ; (* 3 *) #**) X#**)
                         (**# ifdef __STORAGE_DIFF__ lazy_storage_diff ; #**)
                         consumed_gas ; storage_size ; paid_storage_size_diff ;
                       }) ->
            (**# ifndef __MILLIGAS__
               let consumed_gas = Fpgas.t_of_z consumed_gas in
               #**)
            (**# ifdef __PROTO8__
               let consumed_gas = Fpgas.p8_to_p7 consumed_gas in #**)
            (**# ifdef __PROTO9__
               let consumed_gas = Fpgas.p9_to_p7 consumed_gas in #**)
            (**# ifdef __PROTO10__
               let consumed_gas = Fpgas.p10_to_p7 consumed_gas in #**)
            (int_of_status Applied, None, balance_updates, originated_contracts
            , Some consumed_gas, Some storage_size, Some paid_storage_size_diff
              (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ , big_map_diff (* 3 *) #**) X#**)
              (**# ifdef __STORAGE_DIFF__ , lazy_storage_diff #**))
          | Backtracked (Apply_results.Origination_result
                           { balance_updates ; originated_contracts ;
                             (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ big_map_diff ; (* 4 *) #**) X#**)
                             (**# ifdef __STORAGE_DIFF__ lazy_storage_diff ; #**)
                             consumed_gas ; storage_size ; paid_storage_size_diff ;
                           }, error_list_option) ->
            (**# ifndef __MILLIGAS__
               let consumed_gas = Fpgas.t_of_z consumed_gas in
               #**)
            (**# ifdef __PROTO8__
               let consumed_gas = Fpgas.p8_to_p7 consumed_gas in #**)
            (**# ifdef __PROTO9__
               let consumed_gas = Fpgas.p9_to_p7 consumed_gas in #**)
            (**# ifdef __PROTO10__
               let consumed_gas = Fpgas.p10_to_p7 consumed_gas in #**)
            (int_of_status Backtracked, error_list_option, balance_updates, originated_contracts
            , Some consumed_gas, Some storage_size, Some paid_storage_size_diff
              (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ , big_map_diff (* 4 *) #**) X#**)
              (**# ifdef __STORAGE_DIFF__ , lazy_storage_diff #**))
          | Failed (Origination_manager_kind, error_list) ->
            (int_of_status Failed, Some error_list, []
            , [], None, None, None
              (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ , None (* 4 *) #**) X#**)
              (**# ifdef __STORAGE_DIFF__ , None #**))
          | Skipped Origination_manager_kind ->
            (int_of_status Skipped, None, []
            , [], None, None, None
              (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ , None (* 4 *) #**) X#**)
              (**# ifdef __STORAGE_DIFF__ , None #**))
          | _ -> assert false
        in
        begin match delegate with
          | None -> Lwt.return_none
          | Some d ->
            let k = Contract.implicit_contract d in
            record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k ()
            >>= fun kid -> Lwt.return_some kid
        end >>= fun delegate_id ->
        begin match preorigination with
          | None -> Lwt.return_none
          | Some k ->
            record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k ()
            >>= fun kid -> Lwt.return_some kid
        end >>= fun preorigination_id ->
        (**# ifdef __META1__
        record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~pkh:manager ()
        >>= fun manager_id ->
           #**)
        (**# ifdef __META2__ let script = Some script in #**)
        (**# ifdef __META3__ let script = Some script in #**)
        begin (* self logged *)
          (**# ifdef __META4__ let balance_updates = List.map (fun (a,b,_) -> a,b) balance_updates in #**)
          process_origination
            conn pool
            ~opaid
            ~addr_counter
            ~bigmap_counter
            ~bhid ~delegate_id ~preorigination_id
            ~internal ~nonce
            ~block_level ~bh ~source ~source_id ~op_hash ~op_id ~fee ~tokens ~cctxt
            ~delegate (**# ifdef __META1__ ~manager ~manager_id ~spendable ~delegatable #**)
            credit preorigination script balance_updates originated_contracts
            (**# ifdef __META2__ ~big_map_diff #**)
            (**# ifdef __META3__ ~lazy_storage_diff #**)
            ~consumed_gas
            ~storage_size ~paid_storage_size_diff
            ~status
            ~error_list:(error_list_to_json error_list)
        end
      | Reveal pk
        ->
        let status, consumed_gas, error_list =
          match operation_result with
          | Applied (
              Apply_results.Reveal_result
              (**# ifdef __PROTO1__ #**)
              (**# elseifdef __PROTO2__ #**)
              (**# else { consumed_gas } #**)
            ) ->
            (int_of_status Applied
            , (
              (**# ifdef __PROTO1__ None #**)
              (**# elseifdef __PROTO2__ None #**)
              (**# elseifdef __PROTO8__ Some (Fpgas.p8_to_p7 consumed_gas) #**)
              (**# elseifdef __PROTO9__ Some (Fpgas.p9_to_p7 consumed_gas) #**)
              (**# elseifdef __PROTO10__ Some (Fpgas.p10_to_p7 consumed_gas) #**)
              (**# elseifdef __MILLIGAS__ Some consumed_gas #**)
              (**# else Some (Fpgas.t_of_z consumed_gas) #**)
            )
            , None)
          | Backtracked (
              Apply_results.Reveal_result
            (**# ifdef __PROTO1__ #**)
            (**# elseifdef __PROTO2__ #**)
            (**# else { consumed_gas } #**)
            , error_list_option) ->
            (int_of_status Backtracked
            , (
              (**# ifdef __PROTO1__ None #**)
              (**# elseifdef __PROTO2__ None #**)
              (**# elseifdef __PROTO8__ Some (Fpgas.p8_to_p7 consumed_gas) #**)
              (**# elseifdef __PROTO9__ Some (Fpgas.p9_to_p7 consumed_gas) #**)
              (**# elseifdef __PROTO10__ Some (Fpgas.p10_to_p7 consumed_gas) #**)
              (**# elseifdef __MILLIGAS__ Some consumed_gas #**)
              (**# else Some (Fpgas.t_of_z consumed_gas) #**)
            )
            , error_list_option)
          | Failed (Reveal_manager_kind, error_list) ->
            int_of_status Failed, None, Some error_list
          | Skipped Reveal_manager_kind ->
            int_of_status Skipped, None, None
          | _ -> assert false
        in
        begin (* logged *)
          Verbose.Log.store_revelation block_level bh pk;
          Conn.find_opt Reveal_table.insert
            ((opaid, source_id, pk, consumed_gas),
             (fee, nonce, status, error_list_to_json error_list)) >>=
          caqti_or_fail ~__LOC__ >>= fun _ ->
          return_unit
        end
      | Delegation maybe_pkh ->
        let status, consumed_gas, error_list =
          match operation_result with
          | Applied (
              Apply_results.Delegation_result
              (**# ifdef __PROTO1__ #**)
              (**# elseifdef __PROTO2__ #**)
              (**# else { consumed_gas } #**)
            ) ->
            (int_of_status Applied
            , (
              (**# ifdef __PROTO1__ None #**)
              (**# elseifdef __PROTO2__ None #**)
              (**# elseifdef __PROTO8__ Some (Fpgas.p8_to_p7 consumed_gas) #**)
              (**# elseifdef __PROTO9__ Some (Fpgas.p9_to_p7 consumed_gas) #**)
              (**# elseifdef __PROTO10__ Some (Fpgas.p10_to_p7 consumed_gas) #**)
              (**# elseifdef __MILLIGAS__ Some consumed_gas #**)
              (**# else Some (Fpgas.t_of_z consumed_gas) #**)
            )
            , None)
          | Backtracked (
              Apply_results.Delegation_result
            (**# ifdef __PROTO1__ #**)
            (**# elseifdef __PROTO2__ #**)
            (**# else { consumed_gas } #**)
            , error_list_option) ->
            (int_of_status Backtracked
            , (
              (**# ifdef __PROTO1__ None #**)
              (**# elseifdef __PROTO2__ None #**)
              (**# elseifdef __PROTO8__ Some (Fpgas.p8_to_p7 consumed_gas) #**)
              (**# elseifdef __PROTO9__ Some (Fpgas.p9_to_p7 consumed_gas) #**)
              (**# elseifdef __PROTO10__ Some (Fpgas.p10_to_p7 consumed_gas) #**)
              (**# elseifdef __MILLIGAS__ Some consumed_gas #**)
              (**# else Some (Fpgas.t_of_z consumed_gas) #**)
            )
            , error_list_option)
          | Failed (Delegation_manager_kind, error_list) ->
            int_of_status Failed, None, Some error_list
          | Skipped Delegation_manager_kind ->
            int_of_status Skipped, None, None
          | _ -> assert false
        in
        begin
          match maybe_pkh with
          | None -> Lwt.return_none
          | Some pkh ->
            let receiver = Contract.implicit_contract pkh in
            record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:receiver ()
            >>= fun addr -> Lwt.return_some addr
        end >>= fun maybe_pkhid ->
        begin (* logged *)
          Verbose.Log.add_delegation block_level bh source maybe_pkh (consumed_gas: Fpgas.t option) fee;
          Conn.find_opt Delegation_table.insert
            ((opaid, source_id, maybe_pkhid, consumed_gas),
             (fee, nonce, status, error_list_to_json error_list))
        end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
        return_unit
      (* | _ -> assert false *)
      (* end of process *)

end (* of Manager_operation *)

let process_proposals ~opal_counter ~opaid ~addr_counter ~conn ~pool ~bhid ~op_hash:_ ~op_id:_ ~bh:_ ~block_level ~source ~period ~proposals =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  (* FIXME: log me *)
  (**# ifndef __META3__ let period = Voting_period.to_int32 period in #**)
  record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~pkh:source () >>= fun source ->
  match proposals with
  | [] -> return_unit
  | proposal::tl ->
    Proposals_table.(Conn.find_opt insert { opaid; source; period; proposal; i = opal_counter() })
    >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
    Lwt_list.iter_s (fun proposal ->
        Proposals_table.(Conn.find_opt insert2 { opaid; source; period; proposal; i = opal_counter() })
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) -> Lwt.return_unit
      ) tl
    >>= fun () ->
    return_unit

let process_ballot ~opal_counter ~opaid ~addr_counter ~conn ~pool ~bhid ~op_hash:_ ~op_id:_ ~bh:_ ~block_level ~source ~period ~proposal ~ballot =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  (**# ifndef __META3__ let period = Voting_period.to_int32 period in #**)
  (* FIXME: log me *)
  record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~pkh:source () >>= fun source ->
  Ballot_table.(Conn.find_opt insert { opaid; source; period; proposal; ballot; i = opal_counter() })
  >>= caqti_or_fail ~__LOC__ >>= fun _ ->
  return_unit


let get_offender_id ~bhid ~addr_counter block_level pool conn balance_updates =
  (* get offender id *)
  match
    List.find_opt (function (_, Delegate.Debited _) -> true | _ -> false) balance_updates
  with
  | None -> assert false
  | Some (balance, _) -> match balance with
    | Delegate.Contract k ->
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k ()
    | Rewards (pkh, _)
    | Fees (pkh, _)
    | Deposits (pkh, _) ->
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~pkh ()

let baker_id_of_block cctxt pool conn lvl =
  (* This is a little bit messy because of segment indexing: a seed nonce endorsement may
     refer to a block far enough in the past to be a block of the previous protocol *)
  (**#X ifdef __TRANSITION__ (**# ifdef __PROTO1__ let open Tezos_indexer_lib.BS in #**)X#**)
  (**# ifndef __PROTO1__ let open Tezos_indexer_lib.BS in #**)
  let open BS in
  info cctxt ~block:(`Level lvl) ()
  >>=? function
  | { chain_id = _ ; hash = _ ; header = { shell = _ ; protocol_data = _ };
      metadata = Some ({ protocol_data = { baker ; _ } ; _ } : BS.block_metadata) ; operations = _ } ->
    record_address ~__LINE__
      ~addr_counter:(fun () -> Int64.add Misc.min_int53 (Int64.of_int32 lvl))
      lvl pool conn ~pkh:baker () >>=
    begin function
      | baker_id ->
        return baker_id
    end
  | _ ->
    (**#! ifdef PPBS (**# get PPBS #**).!#**)(**# else BS. #**)info cctxt ~block:(`Level lvl) ()
    >>=? function
    | { chain_id = _ ; hash = _ ; header = { shell = _ ; protocol_data = _ };
        metadata = Some ({ protocol_data = { baker ; _ } ; _ } : (**#! ifdef PPBS (**# get PPBS #**).!#**)(**# else BS. #**)block_metadata) ; operations = _ } ->
      record_address ~__LINE__
        ~addr_counter:(fun () -> Int64.add Misc.min_int53 (Int64.of_int32 lvl))
        lvl pool conn ~pkh:baker () >>=
      begin function
        | baker_id ->
          return baker_id
      end
    | _ ->
      (**#! ifdef PBS (**# get PBS #**).!#**)(**# else BS. #**)info cctxt ~block:(`Level lvl) ()
      >>=? function
      | { chain_id = _ ; hash = _ ; header = { shell = _ ; protocol_data = _ };
          metadata = Some ({ protocol_data = { baker ; _ } ; _ } : (**#! ifdef PBS (**# get PBS #**).!#**)(**# else BS. #**)block_metadata) ; operations = _ } ->
        record_address ~__LINE__
          ~addr_counter:(fun () -> Int64.add Misc.min_int53 (Int64.of_int32 lvl))
          lvl pool conn ~pkh:baker () >>=
        begin function
          | baker_id ->
            return baker_id
        end
      | _ ->
        Verbose.error "Error: failed to get block %ld metadata from node" lvl;
        Stdlib.exit 1


let store_op_contents :
  type a b.
  opaid:int64 ->
  baker_id:int64 ->
  bigmap_counter:_ ->
  opal_counter:_ ->
  addr_counter:_ ->
  (* depth:int -> *)
  (module Caqti_lwt.CONNECTION) ->
  _ ->
  #rpc_context ->
  tokens:bool ->
  block_level:int32 ->
  ophid:int64 ->
  Operation_hash.t -> int -> Block_hash.t -> int32 ->
  a contents -> b Apply_results.contents_result -> unit tzresult Lwt.t =
  fun
    ~opaid ~baker_id
    ~bigmap_counter ~opal_counter ~addr_counter
    (* ~depth:_ *) conn pool cctxt ~tokens ~block_level ~ophid op_hash op_id bh bhid contents meta ->
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let get_kid k = record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k () in
  match contents, meta with
  | Seed_nonce_revelation {level ; nonce},
    Apply_results.Seed_nonce_revelation_result balance_updates ->
    (**# ifdef __META4__ let balance_updates = List.map (fun (a,b,_) -> a,b) balance_updates in #**)
    let level = Raw_level.to_int32 level in
    begin
      (* this is required when indexing by segments since it's possible the referred block
         hasn't been indexed yet. FIXME: TODO: if not indexing by segment, get the value from
         the DB instead of making an RPC call. Or at least use disk cache.... *)
      baker_id_of_block cctxt pool conn level
    end >>=? fun sender_id ->
    begin (* logged *)
      Verbose.Log.balance_update
        ~op:(op_hash, op_id, 0) block_level bh balance_updates;
      Balance_table.update ~get_kid conn ~bhid ~opaid balance_updates
    end >>= fun _ ->
    begin (* logged *)
      Verbose.Log.seed_nonce_revelation block_level bh op_hash op_id ~level ~nonce;
      Conn.find_opt Seed_nonce_revelation_table.insert
        { level; sender_id ; baker_id ; nonce ; opaid }
    end
    >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
  | Double_endorsement_evidence { op1 ; op2 (**# ifdef __META4__ ; slot = _ #**) },
    Apply_results.Double_endorsement_evidence_result balance_updates ->
    (**# ifdef __META4__ let balance_updates = List.map (fun (a,b,_) -> a,b) balance_updates in #**)
    begin (* logged *)
      Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
      Balance_table.update ~get_kid conn ~bhid ~opaid balance_updates
    end >>= fun _ ->
    get_offender_id ~bhid ~addr_counter block_level pool conn balance_updates
    >>= fun offender_id ->
    begin (* logged *)
      Verbose.Log.double_endorsement_evidence block_level bh op_hash op_id;
      Double_endorsement_evidence_table.(
        Conn.find_opt insert
          { op1 = Alpha_context.Operation.pack op1 ;
            op2 = Alpha_context.Operation.pack op2 ;
            offender_id ;
            baker_id ;
            opaid }
      ) >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    end
  | Double_baking_evidence { bh1 ; bh2 },
    Apply_results.Double_baking_evidence_result balance_updates ->
    (**# ifdef __META4__ let balance_updates = List.map (fun (a,b,_) -> a,b) balance_updates in #**)
    begin (* logged *)
      Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
      Balance_table.update ~get_kid conn ~bhid ~opaid balance_updates
    end >>= fun _ ->
    get_offender_id ~bhid ~addr_counter block_level pool conn balance_updates
    >>= fun offender_id ->
    begin (* logged *)
      Verbose.Log.double_baking_evidence block_level bh op_hash op_id
        ~baking_level:bh1.Block_header.shell.level;
      Double_baking_evidence_table.(
        Conn.find_opt insert
          { bh1 ; bh2 ; opaid ; baker_id ; offender_id ; }
      ) >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    end
  | Activate_account { id; activation_code; },
    Apply_results.Activate_account_result balance_updates ->
    (**# ifdef __META4__ let balance_updates = List.map (fun (a,b,_) -> a,b) balance_updates in #**)
    begin
      let k = Contract.implicit_contract (Ed25519 id) in
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k () >>= fun kid ->
      begin (* self logged *)
        pre_record_contract_balance ~addr_counter conn pool ~bhid ~bh ~level:block_level ~k
      end >>= fun _ ->
      begin (* logged *)
        Verbose.Log.insert_activated block_level bh (Ed25519 id) op_hash;
        let activation_code =
          Format.asprintf "%a"
            Data_encoding.Json.pp
            (Data_encoding.Json.construct Blinded_public_key_hash.activation_code_encoding activation_code)
        in
        Conn.find_opt Activation_table.insert (opaid, kid, activation_code)
      end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      (* Update balances *)
      begin (* logged *)
        Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
        Balance_table.update ~get_kid conn ~bhid ~opaid balance_updates
      end >>= fun _ ->
      return_unit
    end
(**# ifdef __META4__
   | (Endorsement_with_slot { endorsement = { shell = {branch = _} ; protocol_data = {contents = Single (Endorsement {level}); signature = _} } ; slot },
     Endorsement_with_slot_result Apply_results.Endorsement_result { balance_updates; delegate; slots }) ->
     let level = Raw_level.to_int32 level in
     record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~pkh:delegate () >>= fun delegate_id ->
     Conn.find_opt Endorsement_table.insert_with_slot
       { slot ; opaid ; level; delegate_id ; slots }
     >>= caqti_or_fail ~__LOC__ >>= fun _ ->
     let balance_updates = List.map (fun (a,b,_) -> a,b) balance_updates in
      begin (* logged *)
        Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
        Balance_table.update ~get_kid conn ~bhid ~opaid balance_updates
      end >>= fun () ->
     return_unit
 #**)
  | Endorsement { level },
    Apply_results.Endorsement_result { balance_updates ;
                                       delegate ;
                                       slots } ->
    (**# ifdef __META4__ let balance_updates = List.map (fun (a,b,_) -> a,b) balance_updates in #**)
    (* Update endorsements *)
    let level = Raw_level.to_int32 level in
    record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~pkh:delegate () >>= fun delegate_id ->
    begin (* logged *)
      Verbose.Log.endorsement_result block_level bh
        op_hash op_id ~level ~delegate ~slots;
      Conn.find_opt Endorsement_table.insert
        { level; delegate_id; slots; opaid }
    end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
    (* upsert balance in contract table *)
    begin (* self logged *)
      pre_record_contract_balance ~addr_counter conn pool ~bhid ~bh ~level:block_level
        ~k:(Contract.implicit_contract delegate)
    end >>= fun () ->
    (* Update balances *)
    begin (* logged *)
      Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
      Balance_table.update ~get_kid ~opaid conn ~bhid balance_updates
    end >>= fun () ->
    return_unit
  | Manager_operation { source; fee; operation; gas_limit; storage_limit ; counter },
    Apply_results.Manager_operation_result {
      balance_updates ; (* Correspond to the operation fees *)
      operation_result;
      internal_operation_results ; (* matched and processed later *)
    } ->
    begin
      (**# ifdef __META4__ let balance_updates = List.map (fun (a,b,_) -> a,b) balance_updates in #**)
      (**# ifdef __MILLIGAS__
           let gas_limit = Gas.Arith.integral_to_z gas_limit in
           #**)
      begin (* logged *)
        Verbose.Log.manager_numbers block_level bh ~op_hash ~op_id ~counter ~gas_limit ~storage_limit;
        Conn.find_opt Manager_numbers.insert (opaid, counter, gas_limit, storage_limit)
      end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn (**# ifdef __META1__ ~k: #**)(**# else ~pkh: #**) source () >>= fun source_id ->
      begin
        let internal = ref 0 in
        let process_internal_op_results internal_op_res =
          list_iter_es
            (function
              | Apply_results.Internal_operation_result
                  ({ source; operation; nonce; }, operation_result) ->
                incr internal;
                let internal = !internal in
                record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k:source () >>= fun source_id ->
                let opaid = opal_counter() in
                begin (* logged *)
                  Verbose.Log.operation_alpha_internal block_level bh op_hash op_id internal;
                  Conn.find_opt Operation_alpha_table.insert
                    ((ophid, op_id, int_of_manager_operation operation, bhid), (internal, opaid))
                end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
                begin (* self logged *)
                  Manager_operation.process conn pool
                    ~opaid
                    ~addr_counter
                    ~bigmap_counter
                    ~bhid
                    ~internal ~source_id
                    cctxt ~tokens ~block_level ~nonce
                    bh op_hash op_id
                    source fee operation operation_result
                end >>= fun _ ->
                return_unit
            )
            internal_op_res
        in
        begin (* self logged *)
          process_internal_op_results internal_operation_results
        end
      end >>=? fun () ->
      (**# ifdef __META2__ let source = (Contract.implicit_contract source) in #**)
      (**# ifdef __META3__ let source = (Contract.implicit_contract source) in #**)
      begin (* self logged *)
        Manager_operation.process conn pool
          ~opaid
          ~addr_counter
          ~bigmap_counter
          cctxt ~tokens ~block_level
          ~bhid
          ~internal:0 ~source_id
          bh op_hash op_id
          source fee operation operation_result
      end >>=? fun () ->
      (* Update balances (global operation fee) *)
      begin (* logged *)
        Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
        Balance_table.update ~get_kid conn ~bhid ~opaid balance_updates
      end >>= fun () ->
      return_unit
    end
  (* failed/skipped/backtracked manager operation, ignore for now... *)
  | Proposals { source; period; proposals; },
    Proposals_result ->
    process_proposals ~opal_counter ~addr_counter ~conn ~pool ~op_hash ~opaid ~op_id ~bh ~bhid ~source ~period ~proposals ~block_level
  | Ballot { source; period; proposal; ballot },
    Ballot_result ->
    process_ballot ~opal_counter ~addr_counter ~conn ~pool ~op_hash ~opaid ~op_id ~bh ~bhid ~source ~period ~proposal ~ballot ~block_level
  | Manager_operation _, (
      Proposals_result
    | Ballot_result
    | Endorsement_result _
    | Seed_nonce_revelation_result _
    | Double_endorsement_evidence_result _
    | Double_baking_evidence_result _
    | Activate_account_result _)
  | Endorsement _, (
      Proposals_result
    |  Ballot_result
    |  Seed_nonce_revelation_result _
    |  Double_endorsement_evidence_result _
    |  Double_baking_evidence_result _
    |  Activate_account_result _
    |  Manager_operation_result _)
  | Activate_account _, (
      Ballot_result
    | Double_baking_evidence_result _
    | Double_endorsement_evidence_result _
    | Endorsement_result _
    | Manager_operation_result _
    | Proposals_result
    | Seed_nonce_revelation_result _
    )
  | Double_baking_evidence _, (
      Proposals_result
    | Ballot_result
    | Double_endorsement_evidence_result _
    | Endorsement_result _
    | Manager_operation_result _
    | Seed_nonce_revelation_result _
    | Activate_account_result _
    )
  | Double_endorsement_evidence _, (
      Proposals_result
    | Ballot_result
    | Double_baking_evidence_result _
    | Endorsement_result _
    | Manager_operation_result _
    | Seed_nonce_revelation_result _
    | Activate_account_result _
    )
  | Seed_nonce_revelation _, (
      Proposals_result
    | Ballot_result
    | Double_baking_evidence_result _
    | Endorsement_result _
    | Manager_operation_result _
    | Activate_account_result _
    | Double_endorsement_evidence_result _
    )
  | Proposals _, (
      Ballot_result
    | Double_baking_evidence_result _
    | Endorsement_result _
    | Manager_operation_result _
    | Activate_account_result _
    | Double_endorsement_evidence_result _
    | Seed_nonce_revelation_result _
    )
  | Ballot _, (
      Proposals_result
    | Double_baking_evidence_result _
    | Endorsement_result _
    | Manager_operation_result _
    | Activate_account_result _
    | Double_endorsement_evidence_result _
    | Seed_nonce_revelation_result _
    )
    ->
    assert false
(**# ifdef __META4__
   | (Failing_noop _, _) -> assert false (* those operations always fail and therefore can't appear in a block *)
   | (_, Endorsement_with_slot_result _) -> assert false
   | (Endorsement_with_slot _, _) ->
     assert false
#**)


let store_op_id :
  type a b.
  baker_id:int64 ->
  bigmap_counter:(unit -> int) ->
  opal_counter:(unit -> int64) ->
  addr_counter:(unit -> int64) ->
  (* depth:int -> *)
  (module Caqti_lwt.CONNECTION) ->
  _ ->
  #rpc_context ->
  tokens:bool ->
  block_level:int32 ->
  ophid:int64 ->
  Operation_hash.t -> int -> Block_hash.t -> int32 ->
  a contents -> b Apply_results.contents_result -> unit tzresult Lwt.t =
  fun ~baker_id ~bigmap_counter ~opal_counter ~addr_counter (* ~depth *)
    conn pool cctxt ~tokens ~block_level ~ophid op_hash op_id bh bhid contents meta ->
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let opaid = opal_counter() in
  begin (* logged *)
    Verbose.Log.operation_alpha_preinsert block_level bh op_hash op_id (string_of_contents contents);
    Conn.find_opt Operation_alpha_table.insert
      ((ophid, op_id, int_of_contents contents, bhid), (0, opaid))
  end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
  (* if depth <= 2 then
   *   return_unit
   * else *)
    store_op_contents ~baker_id ~opaid ~addr_counter ~bigmap_counter ~opal_counter (* ~depth *) conn pool cctxt ~tokens ~block_level ~ophid op_hash op_id bh bhid contents meta


let store_op ~baker_id ~addr_counter ~bigmap_counter ~opal_counter ~op_counter (* ~depth *) conn pool cctxt ~tokens ~block_level bh bhid
    ({ BS.chain_id = _ ; hash = op_hash ;
       protocol_data = Operation_data { contents ; signature = _ } ;
       receipt ; shell = _ }) =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let rec inner :
    type a. int -> a contents_list -> _ -> ophid -> unit tzresult Lwt.t =
    fun op_id contents receipt ophid ->
        match contents, receipt with
        | Single op,
          Apply_results.Operation_metadata { contents = Apply_results.Single_result a } ->
          store_op_id ~baker_id ~addr_counter ~bigmap_counter ~opal_counter (* ~depth *) conn pool cctxt ~tokens ~block_level ~ophid op_hash op_id bh bhid op a
        | Cons (op, rest),
          Apply_results.Operation_metadata { contents = Apply_results.Cons_result (a, meta)} ->
          store_op_id ~baker_id ~addr_counter ~bigmap_counter ~opal_counter (* ~depth *) conn pool cctxt ~tokens ~block_level ~ophid op_hash op_id bh bhid op a >>=? fun () ->
          inner (succ op_id) rest (Apply_results.Operation_metadata { contents = meta }) ophid
        | Single _, Apply_results.No_operation_metadata ->
          assert false
        | Cons _, Apply_results.No_operation_metadata ->
          assert false
        | Single _, Apply_results.Operation_metadata { contents = Apply_results.Cons_result _ } ->
          assert false
        | Cons _, Apply_results.Operation_metadata { contents = Apply_results.Single_result _} ->
          assert false
  in
  begin (* logged *)
    Verbose.Log.store_raw_operation block_level bh op_hash;
    Conn.find_opt Operation_table.insert (op_hash, (* chain_id, *) bhid, op_counter())
  end >>= caqti_or_fail ~__LOC__ >>= function
  | None ->
    Verbose.eprintf ~force:true "Error: failed to insert operation %a" Operation_hash.pp op_hash;
    Stdlib.exit Verbose.ExitCodes.indexing_error
  | Some ophid ->
    match receipt with
    | None ->
      failwith
        "Pruned operation by the node, receipt is no longer available. \
         The node is not running in archive-mode."
    | Some receipt ->
      inner 0 contents receipt ophid

let store_ops ~baker_id ~addr_counter ~bigmap_counter ~opal_counter ~op_counter (* ~depth *) conn pool cctxt ~tokens ~block_level bh bhid ops =
  match ops with
  | [endorsements; voting; anon; manager] ->
    begin (* self logged *)
      let f =
        List.iter_es
          (store_op ~baker_id ~addr_counter ~bigmap_counter ~opal_counter ~op_counter (* ~depth *) conn pool ~tokens cctxt ~block_level bh bhid)
      in
      f endorsements >>=? fun () ->
      f voting >>=? fun () ->
      f anon >>=? fun () ->
      f manager
    end
  | _ -> assert false

let previous_chain_id = ref None

type block_info = BS.block_info

let block_info_from_string s : (block_info, string) result =
  match Data_encoding.Json.from_string s with
  | Error e -> Error e
  | Ok j ->
    try
      Ok (Data_encoding.Json.destruct BS.block_info_encoding j)
    with e -> Error (Printexc.to_string e)

let string_of_block_info_from_string s =
  block_info_from_string s |> function
  | Ok b ->
    Data_encoding.Json.construct BS.block_info_encoding b
    |> Data_encoding.Json.to_string
  | Error e ->
    Stdlib.failwith ("string_of_block_info_from_string: " ^ e)


module type File_blocks =
sig
  val get_block : ?dir:string -> int32 -> string option
end

let store_block_full
    (* ~depth *)
    ?(file_blocks=(module Tezos_indexer_lib.File_blocks:File_blocks))
    ?use_disk_cache ?(tokens=false) ?chain ?block (cctxt:#rpc_context) conn pool =
  (* let _ : #RPC_context.simple = cctxt in *)
  let module File_blocks = (val file_blocks : File_blocks) in
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  (* Conn.start () >>= caqti_or_fail ~__LOC__ >>= fun () ->
   * Verbose.Debug.transaction_start (); *)
  begin
    match use_disk_cache, block with
    | Some dir, Some(`Level lvl) ->
      begin
        Verbose.Debug.eprintf ~vl:1 "reading block %ld from disk cache" lvl;
        match File_blocks.get_block ~dir lvl with
        | Some b ->
            block_info_from_string b |> begin function
            | Ok b ->
              begin
              Verbose.Debug.eprintf ~vl:1 "got block %ld from disk cache" lvl;
              let l = b.header.shell.level in
              if l = lvl then
                return b
              else (
                Verbose.Debug.eprintf ~vl:0 "disk cache for block %ld returned wrong block: %ld" lvl l;
                BS.info cctxt ?chain ?block () (* CCTX *)
              )
            end
            | Error e ->
              Verbose.Debug.eprintf ~vl:0 "failed to use disk cache for getting block %ld, error %s" lvl e;
              BS.info cctxt ?chain ?block () (* CCTX *)
          end
        | None ->
          Verbose.Debug.eprintf ~vl:0 "block %ld not found in disk cache" lvl;
          BS.info cctxt ?chain ?block () (* CCTX *)
        | exception e ->
          Verbose.Debug.eprintf ~vl:0 "failed hard to use disk cache for getting block %ld: %a" lvl pp_exn e;
          BS.info cctxt ?chain ?block () (* CCTX *)
      end
    | Some _, _ ->
      Verbose.Debug.eprintf ~vl:1 "weird use case detected";
      BS.info cctxt ?chain ?block () (* CCTX *)
    | None, _ ->
      BS.info cctxt ?chain ?block () (* CCTX *)
  end >>= function
  | Error (((RPC_context.Not_found {uri=_; meth=_})::_) as e)
  | Error (((RPC_client_errors.Request_failed {uri=_; error=_; meth=_})::_) as e) ->
    begin match block with
      | Some (`Level l) ->
        return (Int32.pred l)
      | _ ->
        Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
        Verbose.eprintf ~force:true "[Unexpected] %a" pp_print_error e;
        Stdlib.exit 1
    end
  | Error e ->
    Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
    Verbose.eprintf ~force:true "[While trying to fetch a block] %a" pp_print_error e;
    Stdlib.exit 1
  | Ok { chain_id ; hash ; header = { shell ; protocol_data = _ } as header ;
         metadata ; operations } ->
    (* Store chain *)
    (if Some chain_id = !previous_chain_id then
       Lwt.return_unit
     else
       let () = previous_chain_id := Some chain_id in
       Conn.find_opt Chain_id_table.insert chain_id >>= caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit)
    >>= fun () ->
    (* Store block header (shell + alpha) *)
    match metadata with
    | None ->
      failwith
        "Pruned block by the node, metadatas are no longer available. \
         The node is not running in archive-mode."
    | Some metadata ->
      store_block (* ~depth *) conn pool hash header metadata >>=? fun (bhid, addr_counter, baker_id) ->
      (* Store ops (shell + alpha) *)
      begin
        (* if depth <= 1 then
         *   return_unit
         * else *)
        begin
          store_ops
            ~baker_id
            ~bigmap_counter:(Misc.make_counter())
            ~opal_counter:(
              let bl = Int64.of_int32 bhid in
              let prefix =
                (* this limits the number of operations alpha per block at about 99M *)
                (* this also makes removing *all* reorg-involved blocks mandatory *)
                Int64.mul bl (10_000_000L) in
              Misc.make_counter64 ~start:prefix ()
            )
            ~op_counter:(
              let bl = Int64.of_int32 bhid in
              let prefix =
                (* this limits the number of operations per block at about 99M *)
                (* this also makes removing *all* reorg-involved blocks mandatory *)
                Int64.mul bl (10_000_000L) in
              Misc.make_counter64 ~start:prefix ()
            )
            ~addr_counter
            (* ~depth *) conn pool cctxt ~tokens ~block_level:shell.level hash bhid operations >>=? fun () ->
          return_unit
          (* Conn.commit () >>= caqti_or_fail ~__LOC__ >>= fun () ->
           * Verbose.Debug.transaction_committed (); *)
        end
      end >>= fun _ ->
    (* begin (\* FIXME: logme *\)
     *   Conn.find_opt Block_table.confirm (bhid, depth)
     * end >>= caqti_or_fail ~__LOC__ >>= fun _ -> *)
      Verbose.Debug.block_stored hash shell.level;
      return bhid


let discover_initial_ks (cctxt:#rpc_context) blockid conn pool =
  (* When the chain starts being indexed, we need to become aware of
     all existing contracts.  *)
  let addr_counter = let r = ref 0L in
    fun () -> let n = Int64.pred !r in r := n ; n
  in
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  BS.info cctxt (* CCTX *)
    ~chain:(fst blockid) ~block:(snd blockid) () >>=?
  fun { chain_id ; hash = bh; header = { shell ; protocol_data = _ };
        metadata = _;  operations = _ } ->
  let block_level = shell.level in
  (* Get all contracts at that given context: *)
  Alpha_services.Contract.list cctxt blockid (* CCTX *) >>=? fun ks ->
  (* For each contract, record it: *)
  list_iter_es (fun k ->
    Alpha_services.Contract.info (* CCTX *)
      cctxt blockid k >>=? fun
      { (**# ifdef __META1__ manager ; spendable = _ ; #**)
        balance ; delegate ; counter = _ ; script ;
      }
        ->
(**# ifdef __META1__
    record_address ~__LINE__ ~addr_counter block_level pool conn ~pkh:manager () >>= fun _manager_id ->
   (match snd delegate with
   | None -> Lwt.return_none
   | Some delegate ->
    record_address ~__LINE__ ~addr_counter block_level pool conn ~pkh:delegate () >>= fun d ->
    Lwt.return_some d
   ) >>= fun _delegate_id ->
   #**)
(**# else
    (* record delegate's address, if any *)
    begin
      match delegate with
      | None -> Lwt.return_none
      | Some delegate ->
        record_address ~__LINE__ ~addr_counter block_level pool conn ~pkh:delegate () >>= fun delegate_id ->
        Lwt.return_some delegate_id
    end >>= fun _delegate_id ->
#**)
(**# ifdef __META1__ (* let delegatable = Some (fst delegate) in *) #**)
(**# else (* let delegatable = Some (Contract.is_implicit k = None) in *) #**)
    (* add chain *)
    begin (* logged *)
      if Some chain_id = !previous_chain_id then
        Lwt.return_unit
      else
        let () = previous_chain_id := Some chain_id in
        Verbose.Log.chain_id block_level bh chain_id;
        Conn.find_opt Chain_id_table.insert chain_id
        >>= caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit
    end >>= fun () ->
    (* get bhid *)
    (* begin (\* logged *\)
     *   Verbose.Log.block block_level bh;
     *   Conn.find_opt Block_table.get_bhid bh
     * end >>= caqti_or_fail ~__LOC__ >>= function
     * | None ->
     *   Verbose.Log.eprintf ~force:true "Could not get block_hash_id of block %ld" (shell.level);
     *   Stdlib.exit 1
     * | Some bhid -> *)
      let bhid = 2l in
      record_address ~__LINE__ ~bhid ~addr_counter block_level pool conn ~k () >>= fun kid ->
      begin (* logged *)
        Verbose.Log.contract_update_balance ~bh ~level:block_level ~k ~balance ();
        Conn.find_opt Contract_balance_table.insert_balance_full
          ((kid, bhid, balance, block_level), script,
           Misc.flatten_option (Option.map extract_strings_and_bytes script))
      end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      return_unit
    )
    ks
  >>=? fun () ->
  return_unit

let rec run_one f =
  f () >>= function
  | `Continue -> run_one f
  | `Stop -> Lwt.return_unit

let bootstrap_generic
    ~threads
    ~sqltx_rate
    (* ~depth *)
    ?(stepback=0l)
    ?from ?up_to ~first_alpha_level
    (cctxt:#rpc_context)
    conn pool make_conn
    ~(f:
      (module File_blocks) ->
      (**## ifdef __META1__ #Client.Alpha_client_context.full ->##**)(**# else _ ->#**)
      (module Caqti_lwt.CONNECTION) ->
      _ -> (* pool *)
      int32 ->
      int32 tzresult Lwt.t)
  =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Conn.find Block_table.select_max_level () >>=
  caqti_or_fail ~__LOC__ >>= fun db_max_level ->
  (**#X ifndef __TRANSITION__ let db_max_level = Option.map (Int32.add stepback) db_max_level in X#**)(**# else let _ = stepback in #**)
  let from =
    max first_alpha_level @@
    match from, db_max_level with
    | Some lvl, _ when lvl > 0l ->
      lvl
    | Some from_neg, Some level ->
      max (Int32.neg from_neg) (Int32.succ level)
    | None, Some lvl ->
      Int32.succ lvl
    | None, None
    | Some _, None ->
      first_alpha_level
  in
  begin match db_max_level with
    (* | _ when depth < 2 ->
     *   Verbose.Debug.eprintf "not discovering initial contracts";
     *   return_unit *)
    | _ when from > 2l ->
      Verbose.Debug.eprintf "not discovering initial contracts";
      return_unit
    | Some n when n > 2l && from > 2l ->
      Verbose.Debug.eprintf "not discovering initial contracts";
      return_unit
    | Some _ | None ->
      let db_max_level = match db_max_level with Some n -> n | None -> 0l in
      Verbose.Debug.eprintf "discovering initial contracts first_alpha_level=%ld n=%ld from=%ld" first_alpha_level db_max_level from;
      Conn.start () >>= caqti_or_fail ~__LOC__ >>= fun () ->
      Verbose.Debug.transaction_start ();
      Verbose.Debug.eprintf "indexing block 2 before discovering initial contracts";
      f (module Tezos_indexer_lib.File_blocks) cctxt conn pool 2l >>= function _ ->
        discover_initial_ks cctxt (`Main, `Level first_alpha_level) conn pool
      >>= function
      | Ok _ ->
        Conn.commit () >>= caqti_or_fail ~__LOC__ >>= fun () ->
        Verbose.Debug.transaction_committed ();
        return_unit
      | Error _ as e ->
        Conn.commit () >>= caqti_or_fail ~__LOC__ >>= fun () ->
        Verbose.Debug.transaction_committed ();
        Lwt.return e
  end >>= begin function
    | Ok () ->
      (* because if it's 2l, it was already indexed *)
      Lwt.return (max 3l from)
    | Error e ->
      Verbose.Debug.eprintf "discovering contracts failed with error %a" pp_print_error e;
      (* discovering failed, so we'll assume it's because it's the wrong protocol,
         but mostly we should not modify the value of [from] *)
      Lwt.return from
  end >>= fun from ->
  Verbose.Log.start_downloading_chain from;
  let rec process_n_blocks file_blocks conn counter lvl =
    match counter, up_to with
    | c, _ when c <= 0l -> return (`Counter_exhausted, lvl)
    | _, Some target when lvl > target ->
      Verbose.Log.bootstrapping_target_reached target;
      return (`Target_reached, lvl)
    | _ ->
      Verbose.Log.processing_block lvl;
      protect
        (fun () ->
           f file_blocks cctxt conn pool lvl >>=? fun reached ->
           if reached = lvl then
             return `Reached
           else
             return (`Stuck reached)
        )
        ~on_error:(function
            | [RPC_context.Not_found _] ->
              return `Reached_head
            | e ->
              Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
              Verbose.eprintf "Error while trying to get block %ld: %a" lvl pp_print_error e;
              exit 1
          ) >>=? function
      | `Reached ->
        process_n_blocks file_blocks conn (Int32.pred counter) (Int32.succ lvl)
      | `Reached_head ->
        return (`Reached_head, lvl)
      | `Stuck lvl ->
        return (`Stuck, lvl)
  in
  let protected_process_n_blocks file_blocks conn counter lvl =
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    begin
      if sqltx_rate > 0l then
        Conn.start () >>= caqti_or_fail ~__LOC__ >>= fun () ->
        Verbose.Debug.transaction_start ();
        Lwt.return_unit
      else
        Lwt.return_unit
    end >>= fun () ->
    process_n_blocks file_blocks conn counter lvl >>= function
    | Ok r ->
      if sqltx_rate > 0l then
        Conn.commit () >>= caqti_or_fail ~__LOC__ >>= fun () ->
        Verbose.Debug.transaction_committed ();
        return r
      else
        return r
    | Error e ->
      Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
      Verbose.eprintf ~force:true "Error: %a" pp_print_error e;
      Stdlib.exit Verbose.ExitCodes.indexing_error
  in
  let rec inner lvl =
    let blocks_to_download =
      match up_to with
      | None -> 1l
      | Some up_to ->
        min (Int32.sub up_to lvl) sqltx_rate in
    if blocks_to_download <= 0l then begin
      (**#X ifdef __TRANSITION__ Verbose.Debug.printf ~vl:1 "(Transition protocol)"; X#**)
      Verbose.Log.bootstrapping_done proto;
      return lvl
    end else
      protected_process_n_blocks (module Tezos_indexer_lib.File_blocks) conn blocks_to_download lvl >>=?
      fun (status, lvl) ->
      match status with
      | `Stuck ->
        return (Int32.succ lvl)
      | `Reached_head -> return (Int32.pred lvl)
      | `Target_reached -> return lvl
      | `Counter_exhausted -> inner lvl
  in
  let threaded lvl =
    let target =
      match up_to with
      | None -> Int32.succ lvl
      | Some t -> t
    in
    let m = ref 0l in
    let next =
      let c = ref lvl in
      fun () ->
        c := Int32.succ !c;
        if !c = target then
          None
        else
          Some !c
    in
    let f file_blocks conn () =
      match next () with
      | None ->
        Lwt.return `Stop
      | Some lvl ->
        protected_process_n_blocks file_blocks conn 1l lvl >>= function
        | Ok (`Stuck, l) ->
          m := l;
          Lwt.return `Stop
        | Ok (`Reached_head, l) ->
          m := max !m l;
          Lwt.return `Stop
        | Ok (`Target_reached, l) ->
          m := max !m l;
          Lwt.return `Stop
        | Ok (`Counter_exhausted, l) ->
          m := max !m l;
          Lwt.return `Continue
        | Error e ->
          Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
          Verbose.eprintf "%a" pp_print_error e;
          Stdlib.exit 1
    in
    Stdlib.List.init
      threads
      (fun _ ->
         make_conn () >>= fun conn ->
         let module File_blocks = Tezos_indexer_lib.File_blocks.Make (struct end) in
         let file_blocks : (module File_blocks) = (module File_blocks) in
         run_one (f file_blocks conn)
         >>= fun _ ->
         Lwt.return_unit)
    |>
    Lwt.join >>= fun () ->
    return !m
  in
  if threads > 1 then
    threaded from
  else
    inner from

let bootstrap_chain
    ?stepback
    ?use_disk_cache ?from ?up_to ?tokens ~first_alpha_level ~sqltx_rate (* ~depth *) ?(threads=1) (cctxt:#rpc_context) conn pool make_conn =
  match up_to with
  | Some n when	n < 2l ->
    return n
  | _ ->
    let f file_blocks cctxt conn pool lvl =
      store_block_full ~file_blocks (* ~depth *) ?use_disk_cache ?tokens ~block:(`Level lvl) cctxt conn pool
    in
    bootstrap_generic ~threads ~sqltx_rate (* ~depth *) ?stepback ?from ?up_to ~first_alpha_level cctxt conn pool make_conn ~f


(* Export a simplfied version: *)
let store_block_full (* ~depth *) ?use_disk_cache ?tokens ?chain ?block (cctxt:#rpc_context) conn pool =
  store_block_full (* ~depth *) ?use_disk_cache ?tokens ?chain ?block (cctxt:#rpc_context) conn pool
