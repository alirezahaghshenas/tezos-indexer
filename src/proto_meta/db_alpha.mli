(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** {1 Encoders from Tezos types to Caqti (SQL) types.}  *)

(**#X ifdef __META1__
open Tezos_raw_protocol_(**# get PROTO#**)
open Alpha_context
X#**)(**#X else
open Protocol
open Alpha_context
        X#**)

(**#X ifdef __META2__ open Tezos_protocol_(**# get PROTO #**) open Protocol X#**)
(**#X ifdef __META3__ open Tezos_protocol_(**# get PROTO #**) open Protocol X#**)



(**# ifdef __META3__
module Contract : sig
  include module type of struct include Contract end
  (* type big_map_diff = Lazy_storage.diff *)
  (* type lazy_storage_diff = Lazy_storage.diffs *)
end
   #**)
(**# else
module Contract : sig
  include module type of struct include Contract end
  (* type lazy_storage_diff = unit *)
end
   #**)


open Caqti_type

(**# ifdef __PROTO9__ module Delegate = Receipt #**)
(**# ifdef __PROTO10__ module Delegate = Receipt #**)

val k : Contract.t t
val tez : Tez.t t
val lazy_expr : Script.lazy_expr t
val script : Script.t t
val balance : (**# ifdef __PROTO9__ Receipt.balance t #**)(**# elseifdef __PROTO10__ Receipt.balance t #**)(**# else Delegate.balance t #**)
val balance_update : (**# ifdef __PROTO9__ Receipt.balance_update t #**)(**# elseifdef __PROTO10__ Receipt.balance_update t #**)(**# else Delegate.balance_update t #**)
val cycle : Cycle.t t
val voting_period : Voting_period.t t
val voting_period_kind : Voting_period.kind t

open Db_shell

type opaid = int64

(** {1 Typed requests for Indexer's SQL DB.} *)

module Block_alpha_table : sig
  val insert :
    ((bhid
      * kid (* baker *)
      * int32 (* level position *)
      * Cycle.t)
     * (int32 (* cycle position *)
        * Voting_period.t
        * int32 (* voting period position *)
        * Voting_period.kind)
     * Fpgas.t, (* consumed milligas *)
     unit,
     Caqti_mult.zero_or_one) Caqti_request.t
end

module Operation_alpha_table : sig
  val insert :
    ((ophid * int * int * bhid) * (int * int64), unit, Caqti_mult.zero_or_one) Caqti_request.t
end

(* module Operation_sender_receiver_table : sig
 *   type t = {
 *     bhid : bhid;
 *     ophid : ophid;
 *     op_id : int;
 *     operation_kind : int;
 *     internal: int;
 *     sender: kid;
 *     receiver: kid option;
 *   }
 *   val insert :
 *     (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
 * end *)

module Proposals_table : sig
  type t = {
    opaid    : opaid;
    i        : int64;
    source   : kid;
    period   : int32;
    proposal : Protocol_hash.t;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
  val insert2 : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Ballot_table : sig
  type t = {
    opaid    : opaid;
    i        : int64;
    source   : kid;
    period   : int32;
    proposal : Protocol_hash.t;
    ballot   : Alpha_context.Vote.ballot;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Double_endorsement_evidence_table : sig
  type t = {
    opaid : opaid;
    baker_id : kid;
    offender_id : kid;
    op1 : Alpha_context.Operation.packed;
    op2 : Alpha_context.Operation.packed;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end


module Double_baking_evidence_table : sig
  type t = {
    opaid: opaid;
    bh1 : Block_header.t;
    bh2 : Block_header.t;
    baker_id : kid;
    offender_id : kid;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end


module Manager_numbers : sig
  val insert :
    (opaid * Z.t * Z.t * Z.t,
     unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Contract_table : sig

  val update_script : (kid * Script.t * bhid * string list option, unit, Caqti_mult.zero_or_one) Caqti_request.t
  val get_scriptless_contracts : (kid, Contract.t * kid, Caqti_mult.zero_or_more) Caqti_request.t

end

module Contract_balance_table : sig
  val update : (kid * bhid * Tez.t * Script.t option, unit, Caqti_mult.zero_or_one) Caqti_request.t
  val update_via_bh : (kid * Block_hash.t * Tez.t * Script.t option, unit, Caqti_mult.zero_or_one) Caqti_request.t
  val insert_balance_full : ((kid * bhid * Tez.t * bl) * Script.t option * string list option, unit, Caqti_mult.zero_or_one) Caqti_request.t
  val pre_insert_balance : (kid * bhid * bl, unit, Caqti_mult.zero_or_one) Caqti_request.t
  val get_balanceless_contracts :
    (int * int (* proto, limit *)
    , (Contract.t * kid) * (Block_hash.t * bhid * bl)
    , Caqti_mult.zero_or_more
    ) Caqti_request.t
end

module Balance_table : sig
  type t = (Delegate.balance * Delegate.balance_update) list

  val update :
    get_kid:(Contract.t -> kid Lwt.t) ->
    ?tally:(int ref -> unit) ->
    ?opaid:opaid ->
    (module Caqti_lwt.CONNECTION) -> bhid:bhid -> t -> unit Lwt.t
end

type operation_status = int

type error_list = string

module Origination_table : sig
  type t = {
    opaid : opaid;
    src: kid ;
    k: kid option;
    consumed_gas : Fpgas.t option;
    storage_size : Z.t option;
    paid_storage_size_diff : Z.t option;
    fee : Tez.t ;
    nonce : int option;
    preorigination_id : kid option;
    script : Script.t option;
    delegate_id : kid option;
    credit : Tez.t;
    manager_id : kid option;
    bhid : bhid;
    status : operation_status;
    error_list : error_list option;
    strings : string list option;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t

end

module Tx_table : sig
  type t = {
    opaid : opaid;
    source : kid ;
    destination : kid ;
    fee : Tez.t ;
    amount : Tez.t ;
    parameters : Script.lazy_expr option ;
    storage : Script.lazy_expr option ;
    consumed_gas : Fpgas.t option ;
    storage_size : Z.t option ;
    paid_storage_size_diff : Z.t option ;
    entrypoint : string option ;
    nonce : int option ;
    status : operation_status ;
    error_list : error_list option;
    strings : string list option;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Delegation_table : sig
  val insert :
    ( (opaid * kid * kid option * Fpgas.t option)
      * (Tez.t * int option * operation_status * error_list option)
    , unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Reveal_table : sig
  val insert :
    ( (opaid * kid * public_key * Fpgas.t option)
      * (Tez.t * int option * operation_status * error_list option)
    , unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Addresses : sig
  val insert : (Contract.t * kid, kid, Caqti_mult.zero_or_one) Caqti_request.t
end

module Endorsement_table : sig
  type t = {
    opaid : opaid;
    level : bl;
    delegate_id : kid ;
    slots : int list ;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t

  (**# ifdef __PROTO9__
  type with_slot = { (* proto 9 only *)
    opaid : opaid ;
    delegate_id : kid ;
    slots : int list ;
    level : bl;
    slot : int ;
  }
  val insert_with_slot : (with_slot, unit, Caqti_mult.zero_or_one) Caqti_request.t
     #**)
  (**# ifdef __PROTO10__
  type with_slot = { (* proto 10 only *)
    opaid : opaid ;
    delegate_id : kid ;
    slots : int list ;
    level : bl;
    slot : int ;
  }
  val insert_with_slot : (with_slot, unit, Caqti_mult.zero_or_one) Caqti_request.t
   #**)
end

module Mempool_operations : sig
  type status =
    | Applied | Refused | Branch_refused | Unprocessed | Branch_delayed

  type t = {
    branch: Block_hash.t;
    op_hash : Operation_hash.t;
    status: status;
    id: int;
    operation_kind: int;
    source: Signature.public_key_hash option;
    destination: Contract.t option;
    seen: float;
    json_op: string Lazy.t;
    context_block_level: bl;
  }

  val string_of_status : status -> string
  val status_of_string : string -> status option
  val sql_encoding : t Caqti_type.t
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Seed_nonce_revelation_table : sig
  type t = {
    opaid : opaid;
    sender_id : kid;
    baker_id : kid;
    level : bl;
    nonce : Nonce.t
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end


module Bigmap : sig

  val old_insert :
    ((Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Script.expr
      * Script_expr_hash.t
      * Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Script.expr option
      * bhid)
     * (bl * kid * kid * int)
     * opaid
     * string list option
    , unit
    , Caqti_mult.zero_or_one) Caqti_request.t

  val update :
    ((Z.t
      * Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Script.expr
      * Script_expr_hash.t
      * Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Script.expr option)
     * (bhid * bl * kid * kid)
     * (int * opaid * string list option)
    , unit
    , Caqti_mult.zero_or_one) Caqti_request.t

  val alloc :
    ((Z.t
      * Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Script.expr
      * Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Script.expr
      * bhid)
     * (bl * kid * kid * int)
     * opaid
     * string option
    , unit
    , Caqti_mult.zero_or_one) Caqti_request.t

  val clear :
    (Z.t
     * (bhid * bl * kid * kid)
     * int
     * opaid
    , unit
    , Caqti_mult.zero_or_one) Caqti_request.t

  val copy :
    ((Z.t * Z.t)
     * (bhid * bl * kid * kid)
     * int
     * opaid
    , unit
    , Caqti_mult.zero_or_one) Caqti_request.t

end

module Tokens : sig

  module Contract_table : sig
    type t = {
      address : kid ;
      bhid : bhid;
    }

    val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
    val is_token : (kid, bool, [`One]) Caqti_request.t
  end

  module Balance_table : sig
    type t = {
      token_address : kid;
      address : kid;
      amount : int;
    }

    val upsert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
  end

  module Operation_table : sig
    type kind = Transfer | Approve | GetBalance | GetAllowance | GetTotalSupply

    type t = {
      opaid : opaid ;
      token_address : kid ;
      caller : kid ;
      kind : kind ;
    }

    val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
  end

  module Transfer_table : sig
    type t = {
      opaid : opaid ;
      source : kid ;
      destination : kid ;
      amount : Z.t ;
    }

    val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
  end

  module Approve_table : sig
    type t = {
      opaid : opaid ;
      address : kid ;
      amount : Z.t ;
    }

    val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
  end

  module Get_balance_table : sig
    type t = {
      opaid : opaid ;
      address : kid ;
      callback : kid ;
    }

    val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
  end

  module Get_allowance_table : sig
    type t = {
      opaid : opaid ;
      source : kid ;
      destination : kid ;
      callback : kid ;
    }

    val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
  end

  module Get_total_supply_table : sig
    type t = {
      opaid : opaid ;
      callback : kid ;
    }

    val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
  end

end

module Activation_table : sig
  val insert :
    ((opaid * kid * string)
    , unit
    , Caqti_mult.zero_or_one) Caqti_request.t
end


module Utils : sig
  val extract_strings_and_bytes : Alpha_context.Script.t -> string list option
  val extract_strings_and_bytes_from_expr : Alpha_context.Script.expr -> string list option
  val extract_strings_and_bytes_from_lazy_expr : Alpha_context.Script.lazy_expr -> string list option
end
