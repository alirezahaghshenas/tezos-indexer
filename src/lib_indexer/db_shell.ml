(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Env_V0 = Tezos_protocol_environment.MakeV0(
  struct let name = "Tezos_indexer_lib.Db_shell" end
  )()

open Caqti_type.Std

let caqti_or_fail ~__LOC__ =
  function
  | Error e ->
    Format.eprintf "Database error: %a%a\n%!"
      Caqti_error.pp e
      (fun out e -> if !Verbose.debug then Format.fprintf out " %s" e) __LOC__;
    Stdlib.exit Verbose.ExitCodes.database_error
  | Ok e -> Lwt.return e

module Time = Time.Protocol

let connect url =
  Lwt.catch (fun () ->
      Caqti_lwt.connect url >>=
      caqti_or_fail ~__LOC__
    ) (fun e ->
      Printf.eprintf "Database connection error: %s\n\
                      To get more information about specifying how to connect \
                      to the database, run:\n%s --help\n"
        (Printexc.to_string e)
        Sys.argv.(0);
      exit Verbose.ExitCodes.database_connection
    )


let to_caqti_error = function
  | Ok a -> Ok a
  | Error e -> Error (Format.asprintf "%a" pp_print_error e)

let with_transaction conn request elts =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Lwt_list.iter_s begin fun elt ->
    Conn.exec request elt >>=
    caqti_or_fail ~__LOC__
  end elts

let with_transaction_ignore conn request elts =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Lwt_list.iter_s begin fun elt ->
    Conn.find_opt request elt >>=
    caqti_or_fail ~__LOC__ >>= fun _ ->
    Lwt.return_unit
  end elts

(* let with_transaction_pool pool request elts =
 *   Caqti_lwt.Pool.use
 *     (fun dbh ->
 *        let module Conn = (val dbh : Caqti_lwt.CONNECTION) in
 *        let rec iter f = function
 *          | [] -> return ()
 *          | e::tl ->
 *            Lwt.bind
 *              (f e)
 *              (fun () -> iter f tl)
 *        in
 *        iter
 *          (fun elt -> Conn.exec request elt >>= caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit)
 *          elts
 *        >>= fun _ ->
 *        Lwt.return (Ok ())
 *     ) pool >>= fun _ -> Lwt.return_unit *)

let with_transaction_pool_ignore pool request elts =
  Caqti_lwt.Pool.use
    (fun dbh ->
       let module Conn = (val dbh : Caqti_lwt.CONNECTION) in
       let rec iter f = function
         | [] -> return ()
         | e::tl ->
           Lwt.bind
             (f e)
             (fun () -> iter f tl)
       in
       iter
         (fun elt -> Conn.find_opt request elt >>= caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit)
         elts
       >>= fun _ ->
       Lwt.return (Ok ())
    ) pool >>= fun _ -> Lwt.return_unit


let find_opt pool f x =
  Caqti_lwt.Pool.use
    (fun dbh ->
      let module Conn = (val dbh : Caqti_lwt.CONNECTION) in
      Conn.find_opt f x
    )
    pool

let chain_id =
  let open Chain_id in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let bh =
  let open Block_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let time =
  let open Time in
  custom
    ~encode:begin fun a ->
      match Ptime.of_float_s (Int64.to_float (to_seconds a)) with
      | None -> Error "time"
      | Some v -> Ok v
    end
    ~decode:(fun a -> Ok (of_seconds (Int64.of_float (Ptime.to_float_s a))))
    ptime

let fitness =
  custom
    ~encode:(fun a -> let `Hex a = Env_V0.MBytes.to_hex (Fitness.to_bytes a) in Ok a)
    ~decode:(fun a -> match Fitness.of_bytes (Env_V0.MBytes.of_string a) with
        | Some a -> Ok a
        | None -> Error "fitness")
    octets

let pkh =
  let open Signature.Public_key_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let pk =
  let open Signature.Public_key in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let operation_list_list_hash =
  let open Operation_list_list_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let context_hash =
  let open Tezos_crypto.Context_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

let shell_header =
  let open Block_header in
  custom
    ~encode:begin fun {
      level ; proto_level ; predecessor ; timestamp ;
      validation_passes ; operations_hash ; fitness ; context } ->
      Ok ((level, proto_level, predecessor, timestamp),
          (validation_passes, operations_hash, fitness, context))
    end
    ~decode:begin fun
      ((level, proto_level, predecessor, timestamp),
       (validation_passes, operations_hash, fitness, context)) ->
      Ok { level ; proto_level ; predecessor ; timestamp ;
           validation_passes ; operations_hash ; fitness ; context }
    end
    (tup2
       (tup4 int32 int bh time)
       (tup4 int operation_list_list_hash fitness context_hash))

let json =
  custom
    ~encode:(fun json -> Ok (Data_encoding.Json.to_string json))
    ~decode:Data_encoding.Json.from_string
    string

(* let z =
 *   custom
 *     ~encode:(fun a -> Ok (let `Hex a = Hex.of_string (Z.to_bits a) in a))
 *     ~decode:(function a -> Ok (Z.of_bits (Hex.to_string (`Hex a))))
 *     octets
 * let milligas =
 *   custom
 *     ~encode:(fun a -> Ok (let `Hex a = Hex.of_string (Z.to_bits (Fpgas.unsafe_z_of_t a)) in a))
 *     ~decode:(function a -> Ok (Z.of_bits (Hex.to_string (`Hex a)) |> Fpgas.unsafe_t_of_z))
 *     octets *)

let z =
  custom
    ~encode:(fun a -> Ok (Z.to_string a))
    ~decode:(function a -> Ok (Z.of_string a))
    string

let milligas =
  custom
    ~encode:(fun a -> Ok (Z.to_string (Fpgas.unsafe_z_of_t a)))
    ~decode:(function a -> Ok (Z.of_string a |> Fpgas.unsafe_t_of_z))
    string

let oph =
  let open Operation_hash in
  custom
    ~encode:(fun a -> Ok (to_b58check a))
    ~decode:(fun a -> to_caqti_error (of_b58check a))
    string

type bhid = int32
type bl = int32  (* block level *)
type kid = int64
type ophid = int64

let kid = int64
let bhid = int32
let ophid = int64
let bl = int32  (* block level *)

type depth = int
let depth = int

open Caqti_request

let query_zero input output q =
  (* do not use this if you're using a `select` *)
  create_p input output Caqti_mult.zero (fun _ -> q)

let query_zero_or_one input output q =
  create_p input output Caqti_mult.zero_or_one (fun _ -> q)

let query_zero_or_more input output q =
  create_p input output Caqti_mult.zero_or_more (fun _ -> q)

let query_one input output q =
  create_p input output Caqti_mult.one (fun _ -> q)


module Indexer_log = struct
  let record =
    query_zero (tup2 string string) unit
      "insert into indexer_log(version,argv)values($1,$2)"
end

module Chain_id_table = struct
  let insert =
    query_zero_or_one chain_id unit
      "select I.chain(?)"
end

module Block_table = struct
  let insert0 =
    query_one
      (tup2 bh shell_header)
      (option bhid)
      "select I.block0($1,$2,$3,$4,$5,$6,$7,$8,$9)"

  let insert =
    query_one
      (tup2 bh shell_header)
      (option bhid)
      "select I.block($1,$2,$3,$4,$5,$6,$7,$8,$9)"

  let select_max_level =
    query_one
      unit
      (option bl)
      "select max_level()"

  let confirm =
    query_zero_or_one
      (tup2 bhid depth)
      unit
      "select confirm_block($1,$2)"

  let get_bhid =
    query_zero_or_one
      bh
      bhid
      "select block_hash_id($1)"

  let delete_from_level =
    query_zero_or_one
      bl
      bl
      "select delete_blocks_from_level($1)"
end

module Operation_table = struct
  (* let insert =
   *   query_zero_or_one
   *     (tup4 oph chain_id bhid ophid)
   *     ophid
   *     "select I.operation(?,?,?,?)" *)
  let insert =
    query_zero_or_one
      (tup3 oph bhid ophid)
      ophid
      "select I.operation(?,?,?)"
end

module Deactivated_delegate_table = struct
  let insert =
    query_zero_or_one
      (tup2 kid bhid)
      unit
      "select I.deactivated(?,?)"
end




module Snapshot_table = struct
  let update_snapshot =
    query_zero_or_one
      (tup2 int int32)
      unit
      "select I.snapshot(?,?)"

  let store_snapshot_levels pool levels =
    with_transaction_pool_ignore pool update_snapshot levels

end

module Rejected_blocks = struct
  let mark =
    query_zero_or_more
      int
      (tup2 bh bl)
      "select * from mark_rejected_blocks($1)"

  let delete =
    query_zero_or_one
      unit
      bl
      "select delete_rejected_blocks()"
end

module Version = struct
  let select =
    query_zero_or_one
      unit
      (tup3 string bool bool)
      "select version, dev, multicore from indexer_version order by autoid desc limit 1"
end
