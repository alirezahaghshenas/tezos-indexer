(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019-2020 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Script = struct
  let is_human_readable_ascii s =
    let rec check = function
      | -1 -> true
      | i -> let c = String.get s i in
        if c >= '\x20' && c <= '\x7E' then
          check (pred i)
        else
          false
    in check (String.length s - 1)

  let extract_strings_and_bytes script =
    let module S = Set.Make(String) in
    let rec extract_strings_and_bytes accu = function
      | Tezos_micheline.Micheline.Int (_, _) -> accu
      | String (_, string) ->
        if String.length string = 0 then
          accu
        else
          begin match String.index_opt string '\000' with
            | None -> S.add string accu
            | _ ->
              Format.eprintf "NULL character (\\000) detected in %S\n%!" string;
              accu
          end
      | Bytes (_, bytes) ->
        if Bytes.length bytes = 0 then
          accu
        else
          begin
            let s = Bytes.to_string bytes in
            if is_human_readable_ascii s then
              S.add s accu
            else
              try
                Uucd.(
                match decode (decoder (`String s)) with
                  | `Ok _ -> S.add s accu
                  | _ -> accu
              )
              with _ -> accu
          end
      | Seq (_, node_list)
      | Prim (_, _, node_list, _) ->
        List.fold_left extract_strings_and_bytes accu node_list
    in
    match
      S.elements @@ extract_strings_and_bytes S.empty script
    with
    | [] -> None
    | l -> Some l
end



module Env_V0 = Tezos_protocol_environment.MakeV0(
  struct let name = "Tezos_indexer_lib.Verbose" end
  )()

let verbose = ref false
let debug = ref false
let vlevel =
  (* 0: very little
     1: blocks only
     2: blocks and operations
     3: blocks and operations and sub-operations
  *)
  ref 0


module Utils = struct
  let pp_list pp out list =
    let rec loop = function
      | [] -> ()
      | e::[] ->
        Format.fprintf out "%a" pp e
      | e::tl ->
        Format.fprintf out "%a," pp e;
        loop tl
    in
    loop list

  let pp_option ~name ~pp out = function
    | None -> ()
    | Some x -> Format.fprintf out " %s=%a" name pp x

  let pp_option2 ~name out = function
    | Some (pp, Some x) -> Format.fprintf out " %s=%a" name pp x
    | None | Some (_, None) -> ()

  let pp_bool ~name out b =
    Format.fprintf out "%a" (pp_option ~name ~pp:(fun out -> Format.fprintf out "%b")) b

end
open Utils

module Make(F : sig val prefix: string val flag : unit -> bool end) = struct
  (* always output a new line and flush, unless no message was printed *)
  let fprintf ?(prefix=false) ?(force=false) ?(vl=10) out fmt =
    let k c = Format.fprintf c "\n%!" in
    if (F.flag () && vl <= !vlevel) || force then
      begin
        if prefix then Format.fprintf out "%s" F.prefix;
        Format.kfprintf k out fmt
      end
    else
      Format.ikfprintf (fun _ -> ()) out fmt

  let printf ?force ?vl fmt =
    fprintf ~prefix:true ?force ?vl Format.std_formatter fmt

  let eprintf ?force ?vl fmt =
    fprintf ~prefix:true ?force ?vl Format.err_formatter fmt

  let warn ?force fmt =
    eprintf ?force ~vl:0 fmt

  let error fmt =
    eprintf ~force:true ~vl:0 fmt

end

include Make(struct let prefix = "" let flag () = !verbose end)

module Event = Make(struct let prefix = "> " let flag () = !verbose end)

module type PP = sig
    (* val block_full : Format.formatter -> Block_hash.t -> unit *)
    (* val block : Format.formatter -> Block_hash.t -> unit *)
    (* val time : Format.formatter -> Tezos_base.TzPervasives.Time.Protocol.t -> unit *)
    (* val op : Format.formatter -> Operation_hash.t -> unit *)
    (* val opll : Format.formatter -> Operation_list_list_hash.t -> unit *)
    (* val context : Format.formatter -> Context_hash.t -> unit *)
    type contract
    val contract : Format.formatter -> contract -> unit
    type tez
    val tez : Format.formatter -> tez -> unit
    type nonce
    val nonce : Format.formatter -> nonce -> unit
  end

module Debug = struct
  (** All logging representing debugging should start with "# " *)
  include Make(struct let prefix = "# " let flag () = !debug end)

  let transaction_start ?force () =
    printf ?force ~vl:0 "Transaction start"

  let transaction_committed ?force () =
    printf ?force ~vl:0 "Transaction committed"

  let block_stored ?force hash level =
    printf ?force ~vl:1
      "block_level=%ld block=%a treated" level Block_hash.pp hash

end

module DebugF (PP : PP) = struct
  (** This module is for debugging logs which require protocol-specific pretty printers.
      If you need both Debug and DebugF, just create an instance of DebugF and use it
      for both, since DebugF includes Debug. *)
  include Debug

  let record_address ~__LINE__ level ?force k =
    (* Recording addresses is just a technical formality, therefore we
       do not use "> Recording address", especially since we probably
       do this action too often. *)
    printf ?force ~vl:4 "Recording address (line %d) block_level=%ld %a" __LINE__ level PP.contract k

  let address_already_recorded_recently ~__LINE__ level ?force k =
    eprintf ?force ~vl:4 "Address already recorded recently (line %d) block_level=%ld %a" __LINE__ level PP.contract k

  let implicit_already_recorded_recently ~__LINE__ level ?force pkh =
    eprintf ?force ~vl:4 "Implicit already recorded recently (line %d) block_level=%ld %a" __LINE__ level Signature.Public_key_hash.pp pkh

end

module ProcessingSpeed (What : sig val what : string end) : sig
  val count : int32 -> unit
end = struct
  let init_stamp = lazy (Unix.gettimeofday ())
  let stamp = ref 0.
  let counter = ref 0
  let count n =
    ignore (Lazy.force init_stamp);
    if !debug then begin
      if !counter = 0 then
        stamp := Unix.gettimeofday ();
      incr counter;
      if !counter mod 1000 = 0 then begin
        let t = !stamp in
        stamp := Unix.gettimeofday ();
        let d = !stamp -. t in
        let ago = !stamp -. Lazy.force init_stamp in
        Debug.printf ~vl:1 "1000 %s processed in %f seconds (%f/min) \
                      - %d since %.0f seconds ago (%f/min on average) current=%ld"
          What.what d
          (60. *. 1000. /. d)
          !counter
          ago
          (60. *. float !counter /. ago)
          n
      end
    end
end

module CLog = struct
  let printf = printf
  let eprintf = eprintf

  module OpSpeed = ProcessingSpeed(struct let what = "ops" end)
  module OpAlphaSpeed = ProcessingSpeed(struct let what = "ops_alpha" end)

  let manager_numbers ?force level bh ~op_hash ~op_id ~counter ~gas_limit ~storage_limit : unit =
    Event.printf ?force ~vl:4
      "event=stored block_level=%ld block=%a type=manager_numbers \
       ophash=%a opid=%d counter=%s gaslimit=%s storagelimit=%s"
      level
      Block_hash.pp_short bh
      Operation_hash.pp op_hash
      op_id
      (Z.to_string counter)
      (Z.to_string gas_limit)
      (Z.to_string storage_limit)

  let bootstrapping_done ?force proto =
    Debug.printf ?force ~vl:0
      "Bootstrapping protocol %s done" proto

  let bootstrapping_target_reached ?force target =
    Debug.printf ?force ~vl:0
      "Bootstrapping: target reached = %ld" target

  let store_revelation ?force level bh pk =
    Event.printf ?force ~vl:3
      "event=stored block_level=%ld block=%a type=revelation pk=%a"
      level Block_hash.pp_short bh Signature.Public_key.pp pk

  let start_downloading_chain ?force from =
    Debug.printf ?force ~vl:1
      "Start downloading chain from level %ld" from

  let processing_block ?force level =
    Debug.printf ?force ~vl:1
      "Processing block level %ld" level

  let store_op_id ?force fmt =
    printf ?force ~vl:2 fmt

  let op_counter = ref 0l

  let store_raw_operation ?force level bh op_hash =
    op_counter := Int32.succ !op_counter;
    OpSpeed.count !op_counter;
    Event.printf ?force ~vl:2
      "event=stored block_level=%ld block=%a type=raw_operation ophash=%a"
      level Block_hash.pp_short bh Operation_hash.pp op_hash


  let shell =
    let store_block_shell =
      let module Speed = ProcessingSpeed(struct let what = "blocks" end) in
      fun
        ?force
        ~bh ~level
        ~proto_level ~predecessor ~timestamp
        ~validation_passes ~operations_hash ~fitness ~context () ->
        Speed.count level;
        Event.printf ?force ~vl:1
          "event=stored block_level=%ld type=block_shell hash=%a proto_level=%d \
           pred=%a timestamp=%a validation_passes=%d operations_hash=%a \
           fitness=%s context=%a"
          level
          Block_hash.pp bh
          proto_level
          Block_hash.pp_short predecessor
          Time.Protocol.pp_hum timestamp
          validation_passes
          Operation_list_list_hash.pp operations_hash
          fitness
          Context_hash.pp context
    in
    fun ?force bh shell ->
      let { level ; proto_level ; predecessor ; timestamp ;
            validation_passes ; operations_hash ; fitness ; context }
        : Block_header.shell_header =
        shell
      in
      store_block_shell
        ?force
        ~bh ~level
        ~proto_level ~predecessor ~timestamp
        ~validation_passes ~operations_hash
        ~fitness:(let `Hex a = Env_V0.MBytes.to_hex (Fitness.to_bytes fitness) in a)
        ~context ()


  let store_alpha_header ?force
      bh
      block_level
      baker
      level_position
      cycle
      cycle_position
      voting_period_pp
      voting_period
      voting_period_position
      voting_period_kind
      consumed_gas =
    Event.printf ?force ~vl:2
      "event=stored block_level=%ld block=%a type=alpha_header baker=%a \
       level_position=%ld cycle=%ld cycle_position=%ld voting_period=%a \
       voting_period_position=%ld voting_period_kind=%s consumed_gas=%a"
      block_level
      Block_hash.pp bh
      Signature.Public_key_hash.pp baker
      level_position
      cycle
      cycle_position
      voting_period_pp
      voting_period
      voting_period_position
      voting_period_kind
      Fpgas.pp consumed_gas


  let implicit_insert_discovered ?force level bh pkh =
    Event.printf ?force ~vl:3
      "event=stored block_level=%ld block=%a type=implicit_discovered address=%a" level
      Block_hash.pp_short bh
      Signature.Public_key_hash.pp pkh


  let deactivated_delegate_table ?force ~block_level bh pkh =
    Event.printf ?force ~vl:3
      "event=stored block_level=%ld block=%a type=deactivated pkh=%a"
      block_level
      Block_hash.pp_short bh
      Signature.Public_key_hash.pp pkh

  let op_alpha_counter = ref 0l

  let operation_alpha_preinsert ?force level bh op_hash i contents =
    op_alpha_counter := Int32.succ !op_alpha_counter;
    OpAlphaSpeed.count !op_alpha_counter;
    Event.printf ?force ~vl:3
      "event=store block_level=%ld block=%a type=prestore ophash=%a opid=%d contents=%s"
      level
      Block_hash.pp_short bh
      Operation_hash.pp op_hash
      i
      contents

  let operation_alpha_internal ?force level bh op_hash i internal =
    OpAlphaSpeed.count !op_alpha_counter;
    Event.printf ?force ~vl:3
      "event=store block_level=%ld block=%a type=prestore ophash=%a opid=%d internal=%d"
      level
      Block_hash.pp_short bh
      Operation_hash.pp op_hash
      i
      internal

  let insert_activated ?force level bh pkh op_hash =
    Event.printf ?force ~vl:3
      "event=insert block_level=%ld block=%a type=activated pkh=%a activated=%a"
      level
      Block_hash.pp_short bh
      Signature.Public_key_hash.pp pkh
      Operation_hash.pp op_hash

  let endorsement_result ?force block_level bh op_hash i ~level ~delegate ~slots =
    Event.printf ?force ~vl:3
      "event=upsert block_level=%ld block=%a type=endorsement_result ophash=%a opid=%d level=%ld delegate=%a slots={%a}"
      block_level
      Block_hash.pp_short bh
      Operation_hash.pp op_hash
      i
      level
      Signature.Public_key_hash.pp delegate
      (pp_list (fun out -> Format.fprintf out "%d")) slots

end

module Log (PP : PP) = struct
  (** All logging representing relevant data insertion or data updates should start with "> " *)
  include CLog

  let store_origination ?force level bh source =
    Debug.printf ?force ~vl:3
      "event=stored block_level=%ld block=%a type=origination source=%a"
      level Block_hash.pp_short bh PP.contract source

  let store_tx ?force level bh op_hash i ~source ~destination fee amount parameters
    storage consumed_gas storage_size paid_storage_size_diff entrypoint =
    Event.printf ?force ~vl:3
      "event=stored block_level=%ld block=%a type=tx ophash=%a id=%d \
       source=%a destination=%a fee=%a amount=%a%a%a%a%a%a%a"
      level Block_hash.pp_short bh Operation_hash.pp op_hash i
      PP.contract source
      PP.contract destination
      PP.tez fee
      PP.tez amount
      (fun out -> function
         | Some _ -> Format.fprintf out "<params>"
         | None -> ()) parameters
      (fun out -> function
         | Some _ -> Format.fprintf out "<storage>"
         | None -> ()) storage
      (pp_option ~name:"consumed_gas" ~pp:Fpgas.pp) consumed_gas
      (pp_option ~name:"storage_size" ~pp:(fun out e -> Format.fprintf out "%s" (Z.to_string e))) storage_size
      (pp_option ~name:"paid_storage_size_diff" ~pp:(fun out e -> Format.fprintf out "%s" (Z.to_string e))) paid_storage_size_diff
      (fun out -> function
         | Some _ -> Format.fprintf out "<entrypoint>"
         | None -> ())  entrypoint


  let remove_delegation ?force level bh source =
    Event.printf ?force ~vl:3
      "event=stored block_level=%ld block=%a type=remove_delegation source=%a"
      level Block_hash.pp_short bh PP.contract source

  let add_delegation ?force level bh source pkh (consumed_gas:Fpgas.t option) fee =
    Event.printf ?force ~vl:3
      "event=stored block_level=%ld block=%a type=delegation source=%a%a%a fee=%a"
      level Block_hash.pp_short bh
      PP.contract source
      (pp_option ~name:"destination" ~pp:Signature.Public_key_hash.pp) pkh
      (pp_option ~name:"consumed_gas" ~pp:Fpgas.pp) consumed_gas
      PP.tez fee

  let update_contract_delegate ?force level bh source pkh =
    Event.printf ?force ~vl:3
      "event=update block_level=%ld block=%a type=contract_delegation source=%a delegate=%a"
      level Block_hash.pp_short bh
      PP.contract source
      (fun out -> function
         | None -> Format.fprintf out "none"
         | Some pkh -> Format.fprintf out "%a" Signature.Public_key_hash.pp pkh) pkh

  let add_contract ?force level bh k =
    Event.printf ?force ~vl:4
      "event=stored block_level=%ld block=%a type=contract address=%a"
      level Block_hash.pp_short bh PP.contract k

  let chain_id ?force level bh id =
    Event.printf ?force ~vl:4
      "event=stored block_level=%ld block=%a type=chain_id id=%a"
      level Block_hash.pp_short bh Chain_id.pp id

  let block ?force level bh =
    Event.printf ?force ~vl:4
      "event=stored block_level=%ld block=%a type=block"
      level Block_hash.pp_short bh

  let contract_update_balance ?force ~level ~bh ?balance ~k () =
    match balance with
    | None ->
      Event.printf ?force ~vl:4
        "event=preinsert block_level=%ld block=%a type=contract_balance address=%a"
        level
        Block_hash.pp_short bh
        PP.contract k
    | Some balance ->
      Event.printf ?force ~vl:4
        "event=insert block_level=%ld block=%a type=contract_balance address=%a balance=%a"
        level
        Block_hash.pp_short bh
        PP.contract k
        PP.tez balance

  let operation_sender_receiver ?force block_level bh ~subtype ~sender ~receiver ~internal op_hash i =
    Event.printf ?force ~vl:3
      "event=insert block_level=%ld block=%a type=operation_sender_receiver subtype=%s \
       ophash=%a index=%d internal=%d sender=%a%a "
      block_level
      Block_hash.pp_short bh
      subtype
      Operation_hash.pp op_hash
      i
      internal
      PP.contract sender
      (pp_option ~name:"receiver" ~pp:PP.contract) receiver

  let process_mgr_operation ?force block_level bh op_hash i source fee =
    Debug.printf ?force ~vl:3
      "event=process block_level=%ld block=%a type=manager_operation ophash=%a id=%d \
       source=%a fee=%a"
      block_level
      Block_hash.pp_short bh
      Operation_hash.pp op_hash
      i
      PP.contract source
      PP.tez fee


  let insert_origination ?force level bh ophash opid source originated fee =
    Event.printf ?force ~vl:3
      "event=stored block_level=%ld block=%a type=origination source=%a \
       ophash=%a opid=%d%a fee=%a"
      level
      Block_hash.pp_short bh
      PP.contract source
      Operation_hash.pp ophash
      opid
      (pp_option ~name:"originated_contract" ~pp:PP.contract) originated
      PP.tez fee

  let seed_nonce_revelation ?force block_level bh op_hash i ~level ~nonce =
    Event.printf ?force ~vl:3
      "event=upsert block_level=%ld block=%a type=seed_nonce_revelation \
      ophash=%a opid=%d level=%ld nonce=%a"
      block_level
      Block_hash.pp_short bh
      Operation_hash.pp op_hash
      i
      level
      PP.nonce nonce

  (* FIXME: better logs, at least endorsement level *)


  let double_endorsement_evidence
      ?force block_level bh op_hash i =
    Event.printf ?force ~vl:3
      "event=upsert block_level=%ld block=%a type=double_endorsement_evidence \
       ophash=%a opid=%d"
      block_level
      Block_hash.pp_short bh
      Operation_hash.pp op_hash
      i

  let double_baking_evidence
      ?force block_level bh op_hash i ~baking_level =
    Event.printf ?force ~vl:3
      "event=upsert block_level=%ld block=%a type=double_baking_evidence \
       ophash=%a opid=%d baking_level=%ld"
      block_level
      Block_hash.pp_short bh
      Operation_hash.pp op_hash
      i
      baking_level

  let token_contract ?force block_level address bh bhid =
    Event.printf ?force ~vl:3
      "event=upsert block_level=%ld block=%a type=token_contract address=%a bhid=%ld"
      block_level
      Block_hash.pp_short bh
      PP.contract address
      bhid

  let token_balance ?force block_level token address amount =
    Event.printf ?force ~vl:3
      "event=upsert block_level=%ld type=token_balance token=%a \
       address=%a amount=%d"
      block_level
      PP.contract token
      PP.contract address
      amount

  let token_transfer
      ?force block_level token op_hash op_id caller src dst amount =
    Event.printf ?force ~vl:3
      "event=upsert block_level=%ld type=token_transfer token=%a \
       caller=%a src=%a dst=%a amount=%a op_hash=%a op_id=%d"
      block_level
      PP.contract token
      PP.contract caller
      PP.contract src
      PP.contract dst
      Z.pp_print amount
      Operation_hash.pp op_hash
      op_id

  let token_approve
      ?force block_level token op_hash op_id caller address amount =
    Event.printf ?force ~vl:3
      "event=upsert block_level=%ld type=token_approve token=%a \
       caller=%a address=%a amount=%a op_hash=%a op_id=%d"
      block_level
      PP.contract token
      PP.contract caller
      PP.contract address
      Z.pp_print amount
      Operation_hash.pp op_hash
      op_id

  let token_get_balance
      ?force block_level token op_hash op_id caller address callback =
    Event.printf ?force ~vl:3
      "event=upsert block_level=%ld type=token_transfer token=%a \
       caller=%a address=%a callback=%a op_hash=%a op_id=%d"
      block_level
      PP.contract token
      PP.contract caller
      PP.contract address
      PP.contract callback
      Operation_hash.pp op_hash
      op_id

  let token_get_allowance
      ?force block_level token op_hash op_id caller src dst callback =
    Event.printf ?force ~vl:3
      "event=upsert block_level=%ld type=token_transfer token=%a \
       caller=%a src=%a dst=%a callback=%a op_hash=%a op_id=%d"
      block_level
      PP.contract token
      PP.contract caller
      PP.contract src
      PP.contract dst
      PP.contract callback
      Operation_hash.pp op_hash
      op_id

  let token_get_total_supply
      ?force block_level token op_hash op_id caller callback =
    Event.printf ?force ~vl:3
      "event=upsert block_level=%ld type=token_transfer token=%a \
       caller=%a callback=%a op_hash=%a op_id=%d"
      block_level
      PP.contract token
      PP.contract caller
      PP.contract callback
      Operation_hash.pp op_hash
      op_id

end

module ExitCodes = struct
  let ok = 0
  let cmdline = 7
  let tezos_client_config = 2
  let database_connection = 3
  let failure_bootstrap = 4
  let indexing_error = 5
  let database_error = 6
  let other = 42
end
