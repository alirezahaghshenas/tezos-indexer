(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** {1 Encoders from Tezos types to Caqti (SQL) types.}  *)
module Time = Time.Protocol

val connect : Uri.t -> (module Caqti_lwt.CONNECTION) Lwt.t
val chain_id : Chain_id.t Caqti_type.t

val caqti_or_fail : __LOC__:string -> ('a, [< Caqti_error.t ]) result -> 'a Lwt.t

val query_zero :
  'a Caqti_type.t ->
  'b Caqti_type.t ->
  string ->
  ('a, 'b, [`Zero ]) Caqti_request.t
val query_zero_or_one :
  'a Caqti_type.t ->
  'b Caqti_type.t ->
  string ->
  ('a, 'b, [`Zero | `One ]) Caqti_request.t
val query_zero_or_more :
  'a Caqti_type.t ->
  'b Caqti_type.t ->
  string ->
  ('a, 'b, Caqti_mult.zero_or_more) Caqti_request.t
val query_one :
  'a Caqti_type.t ->
  'b Caqti_type.t ->
  string ->
  ('a, 'b, [`One ]) Caqti_request.t

val find_opt :
  ((module Caqti_lwt.CONNECTION),
   [> Caqti_error.call_or_retrieve ] as 'e)
    Caqti_lwt.Pool.t ->
  ('a, 'b, [< `One | `Zero ]) Caqti_request.t ->
  'a -> ('b option, 'e) result Lwt.t


val bh : Block_hash.t Caqti_type.t
val time : Time.t Caqti_type.t
val fitness : Fitness.t Caqti_type.t

val pkh : Signature.public_key_hash Caqti_type.t
val pk : Signature.public_key Caqti_type.t

val operation_list_list_hash : Operation_list_list_hash.t Caqti_type.t
val context_hash : Context_hash.t Caqti_type.t

val shell_header : Block_header.shell_header Caqti_type.t
val json : Data_encoding.json Caqti_type.t

val z : Z.t Caqti_type.t
val milligas : Fpgas.t Caqti_type.t
val oph : Operation_hash.t Caqti_type.t

type bhid = int32
type bl = int32
type kid = int64
type ophid = int64
type depth = int (* block indexing depth *)

val bhid : bhid Caqti_type.t
val bl : bl Caqti_type.t
val kid : kid Caqti_type.t
val ophid : ophid Caqti_type.t

module Indexer_log : sig
  val record : (string * string, unit, [`Zero]) Caqti_request.t
end

(** {1 Typed requests for Indexer's SQL DB.} *)

module Chain_id_table : sig
  val insert : (Chain_id.t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Block_table : sig
  val insert0 :
    (Block_hash.t * Block_header.shell_header,
     bhid option, Caqti_mult.one) Caqti_request.t

  val insert :
    (Block_hash.t * Block_header.shell_header,
     bhid option, Caqti_mult.one) Caqti_request.t

  val select_max_level :
    (unit, bl option, Caqti_mult.one) Caqti_request.t

  val confirm :
    (bhid * depth, unit, Caqti_mult.zero_or_one) Caqti_request.t

  val get_bhid :
    (Block_hash.t, bhid, Caqti_mult.zero_or_one) Caqti_request.t

  val delete_from_level :
    (bl, bl, Caqti_mult.zero_or_one) Caqti_request.t

end

module Operation_table : sig
  (* val insert :
   *   (Operation_hash.t * Chain_id.t * bhid * ophid,
   *    ophid, Caqti_mult.zero_or_one) Caqti_request.t *)
  val insert :
    (Operation_hash.t *  bhid * ophid,
     ophid, Caqti_mult.zero_or_one) Caqti_request.t
end

module Deactivated_delegate_table : sig
  val insert :
    (kid * bhid, unit, Caqti_mult.zero_or_one) Caqti_request.t
end


module Snapshot_table : sig
  val store_snapshot_levels :
    ((module Caqti_lwt.CONNECTION), 'a) Caqti_lwt.Pool.t -> (int * int32) list -> unit Lwt.t
end

(** {1 Caqti helpers} *)

val with_transaction :
  (module Caqti_lwt.CONNECTION) ->
  ('a, unit, [`Zero]) Caqti_request.t -> 'a list -> unit Lwt.t

val with_transaction_ignore :
  (module Caqti_lwt.CONNECTION) ->
  ('a, unit, Caqti_mult.zero_or_one) Caqti_request.t -> 'a list -> unit Lwt.t

module Rejected_blocks : sig
  val mark :
    (int, Block_hash.t * bl, Caqti_mult.zero_or_more) Caqti_request.t
  val delete :
    (unit, bl, Caqti_mult.zero_or_one) Caqti_request.t
end

module Version : sig
  val select : (unit, string * bool * bool, Caqti_mult.zero_or_one) Caqti_request.t
end
