(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)


open Cmdliner
open Rresult

module Convs = struct
  let uri =
    let parser v = R.ok (Uri.of_string v) in
    Arg.conv ~docv:"URL" (parser, Uri.pp_hum)
end

module Terms = struct

  let tezos_client_dir =
    Arg.(value
         & opt string (Filename.concat (Sys.getenv "HOME") ".tezos-client")
         & info ["tezos-client-dir"]
           ~doc:"Where Tezos client config resides"
           ~docv:"PATH")

  let uri ~default ~doc ~args =
    Arg.(value & opt Convs.uri default & info args ~doc ~docv: "URL")

  let uri_option ?default ~doc ~args () =
    Arg.(value & opt (some Convs.uri) default & info args ~doc ~docv: "URL")

  let db ~default =
    uri ~default ~doc:"Database to connect to." ~args:["db"]

end


module Options_cmd = struct

  let force_from_cmd = Arg.(value & opt int32 0l & info ["force-from"]
                             ~doc:"Force indexing from specified level. \
                                   The purpose of this option is to start \
                                   indexing from a level different from the \
                                   latest indexed block. \
                                   The first purpose is to be able to easily \
                                   reindex blocks that have already been indexed, \
                                   but which for some reason might need an update. \
                                   You may index from an arbitrary high level \
                                   only if you are also using `--no-watch` \
                                   and the DB is in multicore mode. \
                                   Also, you cannot start indexing from \
                                   a level that doesn't exist. \
                                   A negative value is considered as an offset (towards the past) \
                                   from the highest block recorded in the DB.")

  let use_disk_cache_cmd = Arg.(value & flag &
                            info ["use-disk-cache"]
                              ~doc:"Use disk cache for reading blocks.")
  let first_block_level_cmd =
    let doc = "Specify level of first alpha block" in
    Arg.(value & opt int32 2l & info ~doc ["first-block"])
  (* first block level *)

  let uri_option_cmd =
    Terms.uri_option ~args:["tezos-url"]
      ~doc:"URL of a running Tezos node. Will use http://localhost:8732 \
            if no URL is specified and --tezos-client-dir isn't used either. Note that --tezos-url has precedence over --tezos-client-dir." ()

  let tezos_client_dir_cmd =
    Terms.tezos_client_dir

  let db_cmd  =

    let default_db_url = Uri.make ~scheme:"postgresql" ~host:"localhost" ~path:"chain" () in
    Terms.db ~default:default_db_url

  let no_snapshot_blocks_cmd =
    Arg.(value & flag & info ~doc:"Deactivate snapshot blocks." ["no-snapshots"])

  let snapshot_blocks_only_cmd =
    Arg.(value &
         flag &
         info ~doc:"Get and update snapshot blocks ONLY (forever)." ["snapshots"])

  let verbose_mode_cmd =
    Arg.(value &
         flag &
         info ~doc:"Verbose mode." ["verbose"])

  let debug_mode_cmd =
    Arg.(value & flag & info ~doc:"Debug mode." ["debug"])

  let print_db_schema_cmd =
    Arg.(value &
         flag &
         info ~doc:"Print default full DB schema and exit." ["db-schema"])

  let print_db_schema_multicore_cmd =
    Arg.(value &
         flag &
         info
           ~doc:"Print \"multicore\" full DB schema and exit. \
                 Do not forget to convert to default schema after bootstrap is over."
           ["db-schema-multicore"])

  let watch_hook_cmd =
    Arg.(value & opt string "" &
         info ["watch-hook"]
           ~doc:"A command to execute after a new block is indexed during the watch process. \
                 If a reorganization happens, the hook will be executed after the reorganization is over.")

  let version_cmd =
    Arg.(value & flag & info ~doc:"Print version and exit." ["version"])

  let rejected_blocks_depth_cmd =
    Arg.(value &
         opt int 100
         & info ["rejected-blocks-depth"]
           ~doc:"How far in the past to look for rejected blocks (happens at every new block during the watch process). \
                 Default value is 100, and 0 means not to look at all.")

  let mempool_indexing_cmd =
    Arg.(value &
         flag &
         info ~doc:"Index mempool ONLY: every other indexing activity will be disabled. \
                    You may run those in another process." ["mempool-only"])

  let verbosity_cmd =
    Arg.(value & opt int 5 &
         info ["verbosity"]
           ~doc:"Verbosity level (requires --verbose and/or --debug, otherwise will have no effect). \
                 0: some logs. 1: block level. 2: operation level. \
                 3: sub-operation level. 4: more logs. 5: even more logs!")

  let balance_updates_only_cmd =
    Arg.(value &
         flag &
         info ["contract-balances-only"]
           ~doc:"Only fill balances in table contract_balances.")

  let no_balance_updates_cmd =
    Arg.(value & flag &
         info ["no-contract-balances"]
           ~doc:"Don't fill balances in table contract_balances.")

  let cache_blocks_dir_cmd =
    Arg.(value & opt string "./cache/" &
         info ["block-cache-directory"]
           ~doc:"Directory from which to read cached blocks.")

  let ignore_db_version_cmd =
    Arg.(value &
         flag &
         info ["no-check-db-schema-version"]
           ~doc:"Do not check DB schema version (not recommended)")

  let tokens_support_cmd =
    Arg.(value &
         flag &
         info ~doc:"Enable FA1.2 tokens indexing." ["tokens-support"])

  let skip_babylon_contracts_cmd =
    Arg.(value &
         flag &
         info ["skip-prebaby-contracts"]
           ~doc:"Skip indexing pre-babylon contracts")

  let sql_tx_rate_cmd =
    Arg.(value & opt int32 1l &
         info ["sql-rate"]
           ~doc:"Number of blocks per SQL transaction, during bootstrap. Use 0 to deactivate SQL transactions.")

  let up_to_cmd =
    Arg.(value & opt int32 (-1l) &
         info ["bootstrap-upto"]
           (*~doc:"bootstrap until it's over or until given level is reached (whichever comes first), then exit. Negative values mean no limit.")*)
           ~doc:"Bootstrap until it's over or until given \
                 level is reached (whichever comes first), \
                 then exit. Negative values mean no limit.")

  let no_watch_cmd =
    Arg.(value & flag & info ~doc:"No watch: exit once bootstrap is done." ["no-watch"])

  let threads_cmd =
    Arg.(value & opt int 1 &
         info ["threads"]
           ~doc:"Number of threads for bootstrapping mode (>1 is experimental, requires DB in multicore mode, and implies --no-watch).")

end
