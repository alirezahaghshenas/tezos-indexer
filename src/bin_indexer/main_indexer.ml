(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Cmdliner

let index
    ~threads
    ~force_from
    ?use_disk_cache
    ~first_alpha_level
    ~tezos_url
    ~tezos_client_dir
    ~db
    ~no_snapshot_blocks
    ~snapshot_blocks_only
    ~verbose
    ~debug
    watch_hook
    ~rejected_blocks_depth
    ~mempool_indexing
    ~verbosity
    ~balance_updates_only
    ~no_balance_updates
    ~ignore_db_version
    ~tokens
    ~skip_babylon_contracts
    ~sqltx_rate
    up_to
    ~no_watch
  =
  Verbose.vlevel := verbosity;
  Verbose.verbose := verbose;
  Verbose.debug := debug;
  Caqti_lwt.connect_pool db |> Db_shell.caqti_or_fail ~__LOC__ >>= fun pool ->
  Tezos_indexer_lib.Db_shell.connect db >>= fun conn ->
  let make_conn () = Tezos_indexer_lib.Db_shell.connect db in
  begin if ignore_db_version then
      Lwt.return_unit
    else
      Indexer_helpers.get_version conn >>= Db_shell.caqti_or_fail ~__LOC__ >>= function
      | None ->
        (Verbose.error "The DB schema is absent or outdated." ; exit 1)
      | Some (v, dev, multicore) ->
        Tezos_indexer_lib.Config.multicore_mode := true;
        let db_version =
          match String.split '.' v with
          | [ major ; minor ; rev ] ->
            (major, minor), rev
          | _ ->
            (Verbose.error "DB version (%s) is unreadable." v; exit 1)
        in
        let exe_version =
          match String.split '.' Version.sql with
          | [ major ; minor ; rev ] ->
            (major, minor), rev
          | _ ->
            (Verbose.error "Executable version (%s) is unreadable." Version.sql; exit 1)
        in
        if fst db_version < fst exe_version then
          (Verbose.error "The DB schema (%s) is outdated. Required: %s." v Version.sql; exit 1);
        if fst db_version > fst exe_version then
          (Verbose.error "The DB schema (%s) is more advanced than this executable. Required: %s." v Version.sql; exit 1);
        if multicore && not no_watch then
          (Verbose.error "The DB schema is in multicore mode but you did not use --no-watch. Therefore I'll exit now."; exit 1);

        if db_version <> exe_version then
          Verbose.warn "Warning: The DB schema (%s) does not match this executable's (%s)." v Version.sql;
        if Version.sql_dev then
          Verbose.warn "Warning: You're using a development version of the database schema. Any change can happen without prior notice.";
        if dev <> Version.sql_dev then
          Verbose.warn "Warning: The development status of the database schema and of the indexer differ! Use at your own risk.";
        Lwt.return_unit
  end >>= fun () ->
  Lwt.async (fun () -> Indexer_helpers.record_action pool >>= fun _ -> Lwt.return_unit);
  Tezos_cfg.mk_rpc_cfg
    tezos_client_dir tezos_url >>= function
  | Error err ->
    Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
    Verbose.error "%a@." pp_print_error err;
    exit Verbose.ExitCodes.tezos_client_config
  | Ok (rpc_config, confirmations, host, port, tls) ->
  Verbose.CLog.printf ~force:true "# host=%S ; port=%d ; tls=%b" host port tls;
  let cctxt =
    new Tezos_client_007_PsDELPH1.Protocol_client_context.wrap_full
      (Tezos_cfg.tezos_indexer_full ~rpc_config ?confirmations ()) in
  Verbose.Debug.printf ~vl:0 "# Context created";
  (* begin if cache_blocks then
   *     let _dir = cache_blocks_dir
   *     and _from = cache_blocks_from
   *     and _upto = cache_blocks_upto in
   *     prerr_endline "This feature is deactivated for now. Please use curl to fill block cache, it's much faster.";
   *     Stdlib.exit 1
   *     (\* Tezos_indexer_lib.File_blocks.write_blocks cctxt ~dir ~from ~upto
   *      * >>= function
   *      * | Ok _ ->
   *      *   exit 0
   *      * | Error err ->
   *      *   Verbose.error "%a@." pp_print_error err;
   *      *   exit Verbose.ExitCodes.other *\)
   *   else
   *     return_unit
   * end >>=? fun () -> *)
  let start_watch, push_start_watch = Lwt.wait () in
  let start_snapshots, push_start_snapshots = Lwt.wait () in
  if mempool_indexing then
    Indexer_helpers.do_mempool_indexing pool cctxt
  else if snapshot_blocks_only then
    Indexer_helpers.snapshots pool no_snapshot_blocks snapshot_blocks_only start_snapshots cctxt
  else if balance_updates_only then
    Indexer_helpers.update_balances no_balance_updates pool cctxt
  else
    begin
      if force_from <> 0l then
        Lwt.return_unit
      else
        begin (* store initial blocks (0 and 1) *)

          Indexer_helpers.store_block_header conn cctxt 0l >>= fun () ->
          Indexer_helpers.store_block_header conn cctxt 1l >>= fun () ->
          if up_to <= 2l then
            Stdlib.exit 0
          else
            Lwt.return_unit
        end
    end >>= fun () ->
    let a =
      Lwt.catch
        (fun () -> Indexer_helpers.snapshots pool no_snapshot_blocks snapshot_blocks_only start_snapshots cctxt)
        (fun e ->
           Verbose.Debug.eprintf ~vl:0 "Getting snapshot blocks ended with %s" (Printexc.to_string e);
           return_unit)
    and b =
      Indexer_helpers.watch ~no_watch
        ~watch_hook
        cctxt
        conn pool make_conn
        rejected_blocks_depth
        use_disk_cache
        first_alpha_level
        start_watch
        tokens
        ~skip_babylon_contracts
        ~sqltx_rate
        (* ~depth *)
        ~up_to
    and c =
      Indexer_helpers.bootstrap
        (* first bootstrap *)
        ~exit_when_done:no_watch
        ~threads
        ~force_from
        ?use_disk_cache
        ~conn
        ~pool
        ~make_conn
        ~cctxt
        ~first_alpha_level
        ~push_start_snapshots
        ~push_start_watch
        ~tokens
        ~skip_babylon_contracts
        ~sqltx_rate
        ~up_to ()
    and d = Indexer_helpers.update_balances no_balance_updates pool cctxt
    in
    a >>=? fun _ -> b >>=? fun _ -> c >>=? fun _ -> d


let print_then_index
    force_from
    use_disk_cache
    first_alpha_level
    tezos_url
    tezos_client_dir
    db
    no_snapshot_blocks
    snapshot_blocks_only
    verbose
    debug
    print_db_schema
    print_db_schema_multicore
    watch_hook
    version
    rejected_blocks_depth
    mempool_indexing
    verbosity
    balance_updates_only
    no_balance_updates
    cache_blocks_dir
    ignore_db_version
    tokens
    skip_babylon_contracts
    sqltx_rate
    up_to
    no_watch
    threads
    () =

  if print_db_schema then begin
    Db_schema.print ();
    exit 0;
  end;

  if print_db_schema_multicore then begin
    Db_schema.print_multicore ();
    exit 0;
  end;

  if version then
    Version.version ();

  index
    ~threads
    ~force_from
    ?use_disk_cache:(if use_disk_cache then Some cache_blocks_dir else None)
    ~first_alpha_level
    ~tezos_url
    ~tezos_client_dir
    ~db
    ~no_snapshot_blocks
    ~snapshot_blocks_only
    ~verbose
    ~debug
    (if watch_hook = "" then (fun () -> Lwt.return_none) else (fun () -> Lwt.return_some (Sys.command watch_hook)))
    ~rejected_blocks_depth
    ~mempool_indexing
    ~verbosity
    ~balance_updates_only
    ~no_balance_updates
    ~ignore_db_version
    ~tokens
    ~skip_babylon_contracts
    ~sqltx_rate
    (if up_to < 0l then Int32.max_int else up_to)
    ~no_watch:(no_watch || threads > 1)

let index_cmd_lwt =
  let open Cmdliner_helpers in

  let doc = "Store and index a Tezos blockchain into a Postgres database." in

  Term.(const print_then_index $
        Options_cmd.force_from_cmd $
        Options_cmd.use_disk_cache_cmd $
        Options_cmd.first_block_level_cmd $
        Options_cmd.uri_option_cmd $
        Options_cmd.tezos_client_dir_cmd $
        Options_cmd.db_cmd $
        Options_cmd.no_snapshot_blocks_cmd $
        Options_cmd.snapshot_blocks_only_cmd $
        Options_cmd.verbose_mode_cmd $
        Options_cmd.debug_mode_cmd $
        Options_cmd.print_db_schema_cmd $
        Options_cmd.print_db_schema_multicore_cmd $
        Options_cmd.watch_hook_cmd $
        Options_cmd.version_cmd $
        Options_cmd.rejected_blocks_depth_cmd $
        Options_cmd.mempool_indexing_cmd $
        Options_cmd.verbosity_cmd $
        Options_cmd.balance_updates_only_cmd $
        Options_cmd.no_balance_updates_cmd $
        Options_cmd.cache_blocks_dir_cmd $
        Options_cmd.ignore_db_version_cmd $
        Options_cmd.tokens_support_cmd $
        Options_cmd.skip_babylon_contracts_cmd $
        Options_cmd.sql_tx_rate_cmd $
        Options_cmd.up_to_cmd $
        Options_cmd.no_watch_cmd $
        Options_cmd.threads_cmd $
        const ()),
  Term.info ~doc ~sdocs:""
    "tezos-indexer"


let lwt_run v =
  Lwt.async_exception_hook := begin fun exn ->
    Verbose.error "[Uncaught Error] %a" pp_exn exn;
  end ;
  match Lwt_main.run v with
  | Error err ->
    Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
    Verbose.error "%a@." pp_print_error err;
    exit Verbose.ExitCodes.other
  | Ok () ->
    ()


let cmd_of_lwt (term, info) =
  Term.((const lwt_run) $ term), info

let () = match Term.eval (cmd_of_lwt index_cmd_lwt) with
  | `Error _ -> exit Verbose.ExitCodes.cmdline
  | #Term.result -> exit Verbose.ExitCodes.ok
