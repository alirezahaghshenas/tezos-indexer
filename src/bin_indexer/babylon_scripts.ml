(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019-2021 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)



let get_script cctxt k =
  Tezos_protocol_008_PtEdo2Zk.Protocol.Alpha_services.Contract.info
    cctxt ((`Main, `Head 0):Block_services.chain * Block_services.block) k >>=? fun
    {
      balance = _ ; delegate = _ ; counter = _ ; script ;
    } ->
  return script

let get_script2 cctxt k =
    Tezos_protocol_009_PsFLoren.Protocol.Alpha_services.Contract.info
      cctxt ((`Main, `Head 0):Block_services.chain * Block_services.block) k >>=? fun
      {
        balance = _ ; delegate = _ ; counter = _ ; script ;
      } ->
    return script


(** The functions using the value below are not reentrant. If you need
   concurrency support, don't forget to adapt the code that uses this. *)
let latest_seen = ref 0L




let rec update_scripts2 cctxt pool =
  let cpt = ref 0 in
  Caqti_lwt.Pool.use (fun conn ->
      Verbose.printf ~vl:1 "# Update scripts for pre-Babylon scriptless contracts";
      let module Conn = (val conn : Caqti_lwt.CONNECTION) in
      Verbose.printf ~vl:1 "# Getting scripts to update for pre-Babylon scriptless contracts";
      Conn.collect_list
        Tezos_indexer_009_PsFLoren.Db_alpha.Contract_table.get_scriptless_contracts
        !latest_seen
      >>= Db_shell.caqti_or_fail ~__LOC__ >>= function
      | [] ->
        Verbose.printf ~force:true ~vl:1 "# No script to update for pre-Babylon contracts";
        Lwt.return (Ok ())
      | scriptless_contracts ->
        Conn.start () >>= Db_shell.caqti_or_fail ~__LOC__ >>= fun _ ->
        Lwt_list.iter_s
          (fun (k, kid) ->
             latest_seen := kid;
             incr cpt;
             Verbose.printf ~vl:1 "# getting script for contract %a" Tezos_raw_protocol_009_PsFLoren.Alpha_context.Contract.pp k;
             get_script2 cctxt k >>= function
             | Ok (Some s) ->
               Verbose.printf ~vl:1 "# updating script for contract %a" Tezos_raw_protocol_009_PsFLoren.Alpha_context.Contract.pp k;
               Conn.find_opt
                 Tezos_indexer_009_PsFLoren.Db_alpha.Contract_table.update_script
                 (kid, s, 655360l, Tezos_indexer_009_PsFLoren.Db_alpha.Utils.extract_strings_and_bytes s)
               >>= Db_shell.caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit
             | Ok None ->
               Verbose.Debug.printf ~vl:1 "getting script for %a failed" Tezos_raw_protocol_009_PsFLoren.Alpha_context.Contract.pp k;
               Lwt.return_unit
             | Error e ->
               Verbose.Debug.printf ~vl:1 "getting script for %a failed with error %a" Tezos_raw_protocol_009_PsFLoren.Alpha_context.Contract.pp k pp_print_error e;
               Lwt.return_unit
          )
          scriptless_contracts
        >>= fun () ->
        Conn.commit () >>= Db_shell.caqti_or_fail ~__LOC__ >>= fun _ ->
        Lwt.return (Ok ())
    ) pool
  >>= fun _ ->
  if !cpt = 100 then
    update_scripts2 cctxt pool
  else
    Lwt.return_unit


let rec update_scripts cctxt pool =
  let cpt = ref 0 in
  Caqti_lwt.Pool.use (fun conn ->
      Verbose.printf ~vl:1 "# Update scripts for pre-Babylon scriptless contracts";
      let module Conn = (val conn : Caqti_lwt.CONNECTION) in
      Verbose.printf ~vl:1 "# Getting scripts to update for pre-Babylon scriptless contracts";
      Conn.collect_list
        Tezos_indexer_008_PtEdo2Zk.Db_alpha.Contract_table.get_scriptless_contracts
        !latest_seen
      >>= Db_shell.caqti_or_fail ~__LOC__ >>= function
      | [] ->
        Verbose.printf ~force:true ~vl:1 "# No script to update for pre-Babylon contracts";
        Lwt.return (Ok ())
      | scriptless_contracts ->
        Conn.start () >>= Db_shell.caqti_or_fail ~__LOC__ >>= fun _ ->
        Lwt_list.iter_s
          (fun (k, kid) ->
             latest_seen := kid;
             incr cpt;
             Verbose.printf ~vl:1 "# getting script for contract %a" Tezos_raw_protocol_008_PtEdo2Zk.Alpha_context.Contract.pp k;
             get_script cctxt k >>= function
             | Ok (Some s) ->
               Verbose.printf ~vl:1 "# updating script for contract %a" Tezos_raw_protocol_008_PtEdo2Zk.Alpha_context.Contract.pp k;
               Conn.find_opt Tezos_indexer_008_PtEdo2Zk.Db_alpha.Contract_table.update_script
                 (kid, s, 655360l, Tezos_indexer_008_PtEdo2Zk.Db_alpha.Utils.extract_strings_and_bytes s)
               >>= Db_shell.caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit
             | Ok None ->
               Verbose.Debug.printf ~vl:1 "getting script for %a failed" Tezos_raw_protocol_008_PtEdo2Zk.Alpha_context.Contract.pp k;
               Lwt.return_unit
             | Error e ->
               Verbose.Debug.printf ~vl:1 "getting script for %a failed with error %a" Tezos_raw_protocol_008_PtEdo2Zk.Alpha_context.Contract.pp k pp_print_error e;
               Lwt.return_unit
          )
          scriptless_contracts
        >>= fun () ->
        Conn.commit () >>= Db_shell.caqti_or_fail ~__LOC__ >>= fun _ ->
        Lwt.return (Ok ())
    ) pool
  >>= fun _ ->
  if !cpt = 100 then
    update_scripts cctxt pool
  else if !cpt > 0 then
    update_scripts2 cctxt pool
  else
    Lwt.return_unit
