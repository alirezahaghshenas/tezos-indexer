(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_protocol_008_PtEdo2Zk.Protocol
open Tezos_baking_008_PtEdo2Zk

(* let mainnet_protocol_heights = {
 *   proto_001 =  28082l ;
 *   proto_002 = 204761l;
 *   proto_003 = 458752l;
 *   proto_004 = 655360l;
 *   proto_005 = 851968l;
 *   proto_006 = 1212416l;
 *   proto_007 = 1343488l;
 *   proto_008 = Int32.max_int;
 *   proto_009 = Int32.max_int;
 * } *)


let hash_1 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PtCJ7pwoxe8JasnHY8YonnLYjcVHmhiARPJvqcC6VfHT5s8k8sY"
let hash_2 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt"
let hash_3 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP"
let hash_4 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
let hash_5 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS"
let hash_6 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
let hash_7 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"
let hash_8 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA"
let hash_9 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
let hash_10 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV"

let get_version conn =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Conn.find_opt Db_shell.Version.select ()

let bootstrap
    ?(exit_when_done=false)
    ?threads
    ?use_disk_cache
    ~conn ~pool ~make_conn
    ~cctxt
    ~force_from ~first_alpha_level ~up_to
    ?push_start_snapshots ?push_start_watch
    ?tokens
    ~skip_babylon_contracts
    ~sqltx_rate
    (* ~depth *)
    () =
  Verbose.CLog.printf ~vl:0 ~force:true "# Starting bootstrap";
  (* begin
   *   Client_baking_blocks.info cctxt (`Head 0) >>= function
   *   | Error err ->
   *     Verbose.error "# Error when trying to bootstrap: %a" pp_print_error err;
   *     exit Verbose.ExitCodes.failure_bootstrap
   *   | (Ok { Client_baking_blocks.level ; _ }) ->
   *     Lwt.return (Alpha_context.Raw_level.to_int32 level)
   * end >>= fun current_head_level ->
   * let up_to =
   *   if up_to >= 2l then
   *     min current_head_level up_to
   *   else
   *     current_head_level
   * in
   * Verbose.CLog.printf ~vl:0 ~force:true "# Target_block=%ld" up_to; *)
  let rec fold f (accu:int32) = function
    | [] ->
      return accu
    | e::tl ->
      f accu e >>=? fun accu ->
      fold f accu tl
  in
  let proto_list : (string
                    * (?stepback:int32 ->
                       ?use_disk_cache:string ->
                       ?from:int32 ->
                       ?up_to:int32 ->
                       ?tokens:bool ->
                       first_alpha_level:int32 ->
                       sqltx_rate:int32 ->
                       (* depth:int -> *)
                       ?threads:int ->
                       (#Tezos_client_001_PtCJ7pwo.Alpha_client_context.full as 'd) ->
                       (module Caqti_lwt.CONNECTION) -> _ -> _ -> int32 tzresult Lwt.t)
                   ) list =
    [
      "001_PtCJ7pwo", Tezos_indexer_001_PtCJ7pwo.Chain_db.bootstrap_chain
    ; "001_002", Chain_db_transition_001_002.bootstrap_chain
    ; "002_PsYLVpVv", Tezos_indexer_002_PsYLVpVv.Chain_db.bootstrap_chain
    ; "002_003", Chain_db_transition_002_003.bootstrap_chain
    ; "003_PsddFKi3", Tezos_indexer_003_PsddFKi3.Chain_db.bootstrap_chain
    ; "003_004", Chain_db_transition_003_004.bootstrap_chain
    ; "004_Pt24m4xi", Tezos_indexer_004_Pt24m4xi.Chain_db.bootstrap_chain
    ; "004_005", Chain_db_transition_004_005.bootstrap_chain
    ; "prebabylon-scripts", (fun ?stepback:_ ?use_disk_cache:_ ?from ?up_to:_ ?tokens:_ ~first_alpha_level:_
        ~sqltx_rate:_ (* ~depth:_ *) ?threads:_ cctxt _conn pool _make_conn ->
        (* Once Babylon gets activated on mainnet, scriptless accounts are assigned a default
           script. Here we fill that void. *)
        begin
          if skip_babylon_contracts then
            Lwt.return_unit
          else
            Babylon_scripts.update_scripts cctxt pool
        end >>= fun _ ->
        match from with
        | None -> return 2l
        | Some f -> return f
      )
    ; "005_PsBabyM1", Tezos_indexer_005_PsBabyM1.Chain_db.bootstrap_chain
    ; "005_006", Chain_db_transition_005_006.bootstrap_chain
    ; "006_PsCARTHA", Tezos_indexer_006_PsCARTHA.Chain_db.bootstrap_chain
    ; "006_007", Chain_db_transition_006_007.bootstrap_chain
    ; "007_PsDELPH1", Tezos_indexer_007_PsDELPH1.Chain_db.bootstrap_chain
    ; "007_008", Chain_db_transition_007_008.bootstrap_chain
    ; "008_PtEdo2Zk", Tezos_indexer_008_PtEdo2Zk.Chain_db.bootstrap_chain
    ; "008_009", Chain_db_transition_008_009.bootstrap_chain
    ; "009_PsFLoren", Tezos_indexer_009_PsFLoren.Chain_db.bootstrap_chain
    ; "009_010", Chain_db_transition_009_010.bootstrap_chain
    ; "010_PtGRANAD", Tezos_indexer_010_PtGRANAD.Chain_db.bootstrap_chain
    ]
  in
  fold
    (fun last_treated_block
      (proto,
       (f : (?stepback:int32 ->
             ?use_disk_cache:string ->
             ?from:int32 ->
             ?up_to:int32 ->
             ?tokens:bool ->
             first_alpha_level:int32 ->
             sqltx_rate:int32 ->
             (* depth:int -> *)
             ?threads:int ->
             Tezos_client_007_PsDELPH1.Protocol_client_context.wrap_full ->
             (module Caqti_lwt.CONNECTION) -> _ -> _ -> int32 tzresult Lwt.t)))
      ->
        Verbose.Debug.printf ~force:true "%s last_treated_block=%ld proto=%s" __LOC__
          last_treated_block proto;
        let from, stepback =
          if force_from < 0l && last_treated_block < up_to then
            Some last_treated_block, Some force_from
          else if force_from <= 0l then
            None, None
          else
            Some (max last_treated_block force_from), None
        in
        f
          ?stepback
          ?from
          ?use_disk_cache
          ~up_to
          ~first_alpha_level:(max first_alpha_level last_treated_block)
          ?tokens
          ~sqltx_rate
          (* ~depth *)
          ?threads
          cctxt
          conn
          pool
          make_conn
    )
    first_alpha_level
    proto_list
  >>=? fun last_treated_block ->
  Verbose.CLog.printf ~vl:0 ~force:true "# [bootstrap] last_treated_block=%ld" last_treated_block;
  if exit_when_done then
    begin
      Verbose.CLog.printf ~force:true "Done bootstrapping, exiting now as you asked for.";
      exit 0;
    end;
  Option.iter (fun e -> Lwt.wakeup e last_treated_block) push_start_watch ;
  Option.iter (fun e -> Lwt.wakeup e ())  push_start_snapshots;
  return_unit

let record_action pool =
  let argv = Array.fold_left (Printf.sprintf "%s %S") "" Sys.argv in
  Caqti_lwt.Pool.use
    (fun dbh ->
       let module Conn = (val dbh : Caqti_lwt.CONNECTION) in
       Conn.exec Db_shell.Indexer_log.record
         (Version.(Printf.sprintf "%s %s %s" t b c), argv)
    )
    pool

let snapshots pool no_snapshot_blocks snapshot_blocks_only start_snapshots cctxt =
  if no_snapshot_blocks then
    return ()
  else
    let rec get_snapshots cycle =
      (
        Snapshot_utils_6.get_roll_snapshot_for_cycle cycle cctxt
        >>= function
        | Error _ ->
          Snapshot_utils_7.get_roll_snapshot_for_cycle cycle cctxt
        | Ok x -> return x
      )
      >>= function
      | Ok block ->
        let block = Int32.of_int block in
        Verbose.Debug.printf ~force:true "Snapshot: cycle=%d block=%ld" cycle block;
        Tezos_indexer_lib.Db_shell.Snapshot_table.store_snapshot_levels
          pool [cycle, block]
        >>= fun () ->
        Verbose.Debug.printf ~force:true "Getting next snapshot with cycle=%d" (succ cycle);
        get_snapshots (succ cycle)
      | Error _ ->
        Lwt_unix.sleep 60. >>= fun () ->
        get_snapshots cycle
    in
    (if snapshot_blocks_only then
       Lwt.return_unit
     else
       start_snapshots) >>= fun () ->
    get_snapshots 7

let watch ~no_watch
    ~watch_hook
    cctxt
    conn pool make_conn
    rejected_blocks_depth use_disk_cache first_alpha_level start_watch
    tokens ~skip_babylon_contracts
    ~sqltx_rate
    (* ~depth *) ~up_to =
  let fold_f block result =
    match block, result with
    | _, Error err
    | Error err, _ ->
      Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
      Verbose.error "%a" pp_print_error err ;
      exit 1
    | Ok ({ Client_baking_blocks.hash ;
            chain_id = _; predecessor = _; fitness = _; timestamp = _;
            protocol ; next_protocol ; proto_level = _; level ;
            context = _; _ ;
          }), Ok () ->
      match
        Tezos_indexer_lib.Misc.list_assoc_opt (protocol, next_protocol)
          [
            (* most probable *)
            (hash_9, hash_9),
            (fun () -> Tezos_indexer_009_PsFLoren.Chain_db.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool);
            (hash_10, hash_10),
            (fun () -> Tezos_indexer_010_PtGRANAD.Chain_db.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool);
            (hash_8, hash_8),
            (fun () -> Tezos_indexer_008_PtEdo2Zk.Chain_db.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool);
            (hash_8, hash_9),
            (fun () -> Chain_db_transition_008_009.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool);
            (hash_7, hash_7),
            (fun () -> Tezos_indexer_007_PsDELPH1.Chain_db.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool);
            (hash_6, hash_6),
            (fun () -> Tezos_indexer_006_PsCARTHA.Chain_db.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool) ;
            (* least probable *)
            (hash_5, hash_5),
            (fun () -> Tezos_indexer_005_PsBabyM1.Chain_db.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool) ;
            (hash_4, hash_4),
            (fun () -> Tezos_indexer_004_Pt24m4xi.Chain_db.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool) ;
            (hash_3, hash_3),
            (fun () -> Tezos_indexer_003_PsddFKi3.Chain_db.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool) ;
            (hash_2, hash_2),
            (fun () -> Tezos_indexer_002_PsYLVpVv.Chain_db.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool) ;
            (hash_1, hash_1),
            (fun () -> Tezos_indexer_001_PtCJ7pwo.Chain_db.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool) ;
            (* even less probable, since these transitions happen at most once per chain *)
            (hash_6, hash_7),
            (fun () -> Chain_db_transition_006_007.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool);
            (hash_7, hash_8),
            (fun () -> Chain_db_transition_007_008.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool);
            (hash_5, hash_6),
            (fun () -> Chain_db_transition_005_006.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool) ;
            (hash_4, hash_5),
            (fun () -> Chain_db_transition_004_005.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool) ;
            (hash_3, hash_4),
            (fun () -> Chain_db_transition_003_004.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool) ;
            (hash_2, hash_3),
            (fun () -> Chain_db_transition_002_003.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool) ;
            (hash_1, hash_2),
            (fun () -> Chain_db_transition_001_002.store_block_full
                (* ~depth *)
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn pool) ;
          ]
      with
      | None ->
        Verbose.error "Unsupported protocol";
        exit 1
      | Some f ->
        f () >>= function
        | Error e ->
          Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
          Verbose.error "Error when recording block %a: %a"
            Block_hash.pp hash
            pp_print_error e;
          exit 1
        | Ok bhid ->
          begin
            let level = Alpha_context.Raw_level.to_int32 level in
            if bhid <> level then
              Verbose.Debug.printf ~vl:1
                "Chain reorganization has happened, block_hash=%a level=%ld has bhid=%ld"
                Block_hash.pp hash
                level
                bhid;
            let module Conn = (val conn : Caqti_lwt.CONNECTION) in
            if rejected_blocks_depth > 0 then
              begin
                Conn.collect_list Db_shell.Rejected_blocks.mark (rejected_blocks_depth)
                >>=
                Db_shell.caqti_or_fail ~__LOC__ >>= fun l ->
                let min_level = ref level in
                Tezos_indexer_lib.Misc.list_iter (fun (bh, level) ->
                    min_level := min level !min_level;
                    Verbose.Event.printf ~vl:1 "event=update block_level=%ld block=%a type=rejected_block"
                      level Block_hash.pp bh
                  ) l;
                if l <> [] then
                  begin
                    Conn.find_opt Db_shell.Block_table.delete_from_level !min_level >>=
                    Caqti_lwt.or_fail >>= function
                    | None ->
                      Verbose.error "DB returned an error after trying to delete blocks due to a chain reorganization.";
                      exit 1
                    | Some force_from ->
                      if force_from <> Int32.pred !min_level then
                        Verbose.Debug.eprintf ~vl:0
                          "After chain reorganization, \
                           force_from (%ld) <> Int32.pred !min_level (%ld)"
                          (force_from) (Int32.pred !min_level);
                      bootstrap
                        (* reorganization happened *)
                        ~exit_when_done:no_watch
                        ~force_from
                        ?use_disk_cache
                        ~conn ~pool ~cctxt ~first_alpha_level
                        ~tokens
                        ~skip_babylon_contracts
                        ~up_to:Int32.max_int
                        ~sqltx_rate
                        (* ~depth *)
                        ~make_conn
                        ()
                      >>= fun _ ->
                      return_unit
                  end
                else
                  return_unit
              end
            else
              return_unit
          end >>=? fun () ->
          watch_hook () >>= function
          | None -> return_unit
          | Some i ->
            Verbose.CLog.eprintf "Your hook was executed and its return code is %d" i;
            return_unit
  in
  Verbose.Debug.printf ~vl:0 "Start watching";
  begin
    Client_baking_blocks.monitor_valid_blocks
      cctxt ~next_protocols:None ()
    >>=? fun stream ->
    Verbose.Debug.printf ~vl:0 "Start monitoring blocks";
    Lwt.catch begin fun () ->
      Lwt_stream.peek stream >>= function
      | None ->
        Verbose.error "Failed to peak block stream from node. I'll exit now.";
        (* this should never happen unless the connection to the node or the node
           stops working *)
        Stdlib.exit 1
      | Some (Error e)  ->
        Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
        Verbose.error "Error when tried to peak stream: %a" pp_print_error e;
        exit 1
      | Some Ok { Client_baking_blocks.level ; _ } ->
        let level = Alpha_context.Raw_level.to_int32 level in
        (* ************************************ *)
        (* HERE we wait until bootstrap is over *)
        (* ************************************ *)
        start_watch
        >>= fun last_treated_block ->
        (* When we arrive here, bootstrap is done. *)
        if no_watch then
          exit 0;
        if last_treated_block >= up_to then
          exit 0;
        (if level > last_treated_block then
           (* If ever bootstrap didn't go high enough, we try again: *)
           bootstrap
             (* second bootstrap *)
             ~exit_when_done:false
             ~force_from:(Int32.pred last_treated_block)
             ?use_disk_cache
             ~conn ~pool ~make_conn
             ~cctxt ~first_alpha_level
             ~tokens
             ~skip_babylon_contracts
             ~up_to:(Int32.max_int) (* <- if we needed to stop earlier, we would have exited already *)
             ~sqltx_rate
             (* ~depth *)
             ()
         else
           return_unit) >>= function
        | Ok () ->
          Lwt_stream.fold_s fold_f stream (Ok ())
        | Error e ->
          Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
          Verbose.error "Error while bootstrapping: %a" pp_print_error e;
          exit 1
    end begin function
      | exn ->
        Verbose.eprintf ~force:true
          "An unhandled exception happened, I'll exit now: %s"
          (Printexc.to_string exn);
        exit 1
    end
  end


let do_mempool_indexing pool cctxt =
  let prev = ref `_0 in
  let new_proto = ref true in
  let level = ref 0l in
  let one b _r = match b with
    | (Error e)  ->
      Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
      Verbose.Debug.error "Error when tried to peak stream: %a" pp_print_error e;
      Lwt.return (Error e)
    | (Ok { Client_baking_blocks.protocol ; next_protocol ; level = bl ; _ }) ->
      level := Alpha_context.Raw_level.to_int32 bl;
      begin
        if (protocol, next_protocol) = (hash_6, hash_6)
           (* assuming here that other supported networks will never
              reach mainnet's protocol transition 6 to 7 block anyways *)
        then
          if !prev = `_6 then
            return ()
          else
            begin
              prev := `_6;
              new_proto := true;
              Lwt.async (fun () ->
                  Verbose.CLog.printf ~vl:0 "Watching mempool proto 6";
                  Tezos_indexer_006_PsCARTHA.Mempool_utils.watch_mempool ~pool cctxt new_proto level
                  >>= fun _ -> Lwt.return_unit);
              return ()
            end
        else if (protocol, next_protocol) = (hash_7, hash_7)
             || (protocol, next_protocol) = (hash_6, hash_7) then
          if !prev = `_7 then
            return ()
          else
            begin
              prev := `_7;
              new_proto := true;
              Lwt.async (fun () ->
                  Verbose.CLog.printf ~vl:0 "Watching mempool proto 7";
                  Tezos_indexer_007_PsDELPH1.Mempool_utils.watch_mempool ~pool cctxt new_proto level
                  >>= fun _ -> Lwt.return_unit);
              return ()
            end
        else if (protocol, next_protocol) = (hash_8, hash_8)
             || (protocol, next_protocol) = (hash_7, hash_8) then
          if !prev = `_8 then
            return ()
          else
            begin
              prev := `_8;
              new_proto := true;
              Lwt.async (fun () ->
                  Verbose.CLog.printf ~vl:0 "Watching mempool proto 8";
                  Tezos_indexer_008_PtEdo2Zk.Mempool_utils.watch_mempool ~pool cctxt new_proto level
                  >>= fun _ -> Lwt.return_unit);
              return ()
            end
        else if (protocol, next_protocol) = (hash_9, hash_9)
             || (protocol, next_protocol) = (hash_8, hash_9) then
          if !prev = `_9 then
            return ()
          else
            begin
              prev := `_9;
              new_proto := true;
              Lwt.async (fun () ->
                  Verbose.CLog.printf ~vl:0 "Watching mempool proto 9";
                  Tezos_indexer_009_PsFLoren.Mempool_utils.watch_mempool ~pool cctxt new_proto level
                  >>= fun _ -> Lwt.return_unit);
              return ()
            end
        else if (protocol, next_protocol) = (hash_10, hash_10)
             || (protocol, next_protocol) = (hash_9, hash_10) then
          if !prev = `_10 then
            return ()
          else
            begin
              prev := `_10;
              new_proto := true;
              Lwt.async (fun () ->
                  Verbose.CLog.printf ~vl:0 "Watching mempool proto 10";
                  Tezos_indexer_010_PtGRANAD.Mempool_utils.watch_mempool ~pool cctxt new_proto level
                  >>= fun _ -> Lwt.return_unit);
              return ()
            end
        else
          begin
            Verbose.CLog.eprintf ~force:true "Unsupported protocols for monitoring mempool: %a %a"
              Protocol_hash.pp protocol
              Protocol_hash.pp next_protocol;
            Stdlib.exit 1
          end
      end
  in
  Client_baking_blocks.info cctxt (`Head 0) >>= fun b ->
  one b () >>= fun _ ->
  Client_baking_blocks.monitor_valid_blocks
    cctxt ~next_protocols:None ()
  >>=? fun stream ->
  Lwt_stream.fold_s one stream (Ok ())


let update_balances no_balance_updates ?(limit=1000) pool cctxt =
  let exception Balance_error in
  let run conn =
    let f get_balanceless_contracts protocol_number record_contract_balance =
      let module Conn = (val conn : Caqti_lwt.CONNECTION) in
      Verbose.Debug.printf ~vl:3 "Getting balanceless contracts with protocol #%d, limit=%d" protocol_number limit;
      Conn.collect_list
        get_balanceless_contracts
        (protocol_number, limit)
      >>= Db_shell.caqti_or_fail ~__LOC__ >>= function
      | [] ->
        (* wait a few seconds if there's no contract balance to be filled *)
        Lwt_unix.sleep 3. >>= fun () ->
        Lwt.return false
      | to_be_filled ->
        Conn.find Db_shell.Block_table.select_max_level () >>=
        Db_shell.caqti_or_fail ~__LOC__ >>= function
        | None ->
          Verbose.Debug.error
            "Block table is empty but you have entries in contract balances. \
             This should not be possible unless you have a corrupted DB.";
          Stdlib.exit 1
        | Some db_max_level ->
          let counter = ref 0 in
          Lwt.catch (fun () ->
              Lwt_list.iter_s (fun ((k, kid), (bh, bhid, level)) ->
                  incr counter;
                  record_contract_balance conn cctxt ~bhid ~bh ~level ~k ~kid ~db_max_level
                  >>= function
                  | Ok _ -> Lwt.return_unit
                  | Error _ -> if true then Lwt.return_unit else Lwt.fail Balance_error
                ) to_be_filled >>= fun () ->
              if !counter > 0 && !counter <= limit then
                Lwt.return true  (* keep same protocol *)
              else
                Lwt.return false (* use next protocol *)
            )
            (function Balance_error -> Lwt.return false | e -> Lwt.fail e)
    in
    let rec loop proto =
      match proto with
      | `_1 ->
        f Tezos_indexer_001_PtCJ7pwo.Db_alpha.Contract_balance_table.get_balanceless_contracts
          1 Tezos_indexer_001_PtCJ7pwo.Chain_db.record_contract_balance
        >>= fun proto_stay ->
        if proto_stay then
          loop proto
        else
          loop `_2
      | `_2 ->
        f Tezos_indexer_002_PsYLVpVv.Db_alpha.Contract_balance_table.get_balanceless_contracts
          2 Tezos_indexer_002_PsYLVpVv.Chain_db.record_contract_balance
        >>= fun proto_stay ->
        if proto_stay then
          loop proto
        else
          loop `_3
      | `_3 ->
        f Tezos_indexer_003_PsddFKi3.Db_alpha.Contract_balance_table.get_balanceless_contracts
          3 Tezos_indexer_003_PsddFKi3.Chain_db.record_contract_balance
        >>= fun proto_stay ->
        if proto_stay then
          loop proto
        else
          loop `_4
      | `_4 ->
        f Tezos_indexer_004_Pt24m4xi.Db_alpha.Contract_balance_table.get_balanceless_contracts
          4 Tezos_indexer_004_Pt24m4xi.Chain_db.record_contract_balance
        >>= fun proto_stay ->
        if proto_stay then
          loop proto
        else
          loop `_5
      | `_5 ->
        f Tezos_indexer_005_PsBabyM1.Db_alpha.Contract_balance_table.get_balanceless_contracts
          5 Tezos_indexer_005_PsBabyM1.Chain_db.record_contract_balance
        >>= fun proto_stay ->
        if proto_stay then
          loop proto
        else
          loop `_6
      | `_6 ->
        f Tezos_indexer_006_PsCARTHA.Db_alpha.Contract_balance_table.get_balanceless_contracts
          6 Tezos_indexer_006_PsCARTHA.Chain_db.record_contract_balance
        >>= fun proto_stay ->
        if proto_stay then
          loop proto
        else
          loop `_7
      | `_7 ->
        f Tezos_indexer_007_PsDELPH1.Db_alpha.Contract_balance_table.get_balanceless_contracts
          7 Tezos_indexer_007_PsDELPH1.Chain_db.record_contract_balance
        >>= fun proto_stay ->
        if proto_stay then
          loop proto
        else
          loop `_8
      | `_8 ->
        f Tezos_indexer_008_PtEdo2Zk.Db_alpha.Contract_balance_table.get_balanceless_contracts
          8 Tezos_indexer_008_PtEdo2Zk.Chain_db.record_contract_balance
        >>= fun proto_stay ->
        if proto_stay then
          loop proto
        else
          loop `_9
      | `_9 ->
        f Tezos_indexer_009_PsFLoren.Db_alpha.Contract_balance_table.get_balanceless_contracts
          9 Tezos_indexer_009_PsFLoren.Chain_db.record_contract_balance
        >>= fun proto_stay ->
        if proto_stay then
          loop proto
        else
          loop `_10
      | `_10 ->
        f Tezos_indexer_010_PtGRANAD.Db_alpha.Contract_balance_table.get_balanceless_contracts
          10 Tezos_indexer_010_PtGRANAD.Chain_db.record_contract_balance
        >>= (function
            | false -> loop `_1
            | true -> loop `_10)
    in
    loop `_1
  in
  if no_balance_updates then
    return_unit
  else
    Caqti_lwt.Pool.use
      (fun conn ->
         run conn
      ) pool >>= Db_shell.caqti_or_fail ~__LOC__ >>= return


let store_block_header conn cctxt n =
  Client_baking_blocks.info cctxt (`Level n) >>= function
  | Ok {
      hash (* : Block_hash.t *);
      chain_id = _ (* : Chain_id.t *);
      predecessor (* : Block_hash.t *);
      fitness (* : Bytes.t list *);
      timestamp (* : Time.Protocol.t *);
      protocol = _ (* : Protocol_hash.t *);
      next_protocol = _ (* : Protocol_hash.t *);
      proto_level (* : int *);
      level (* : Raw_level.t *);
      context (* : Context_hash.t *);
      predecessor_block_metadata_hash = _ ;
      predecessor_operations_metadata_hash = _ ;
    } ->
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    Conn.find
      Db_shell.Block_table.insert0
      (hash, { level = Alpha_context.Raw_level.to_int32 level
             ; proto_level ; predecessor ; timestamp ; fitness
             ; validation_passes = 0
             ; operations_hash=Operation_list_list_hash.empty;context })
    >>= Db_shell.caqti_or_fail ~__LOC__ >>=
    begin function
      | Some x when x = n ->
        Verbose.Debug.block_stored hash n;
        Lwt.return_unit
      | _ ->
        Verbose.error "Error: failed to insert block %ld's header" n;
        exit 1
    end
  | Error (((RPC_client_errors.Request_failed {uri=_; error=Connection_failed msg; meth=_})::_) as em) ->
    Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
    Verbose.error "Unable to connect to the node: \"%s\"" msg;
    Verbose.eprintf ~vl:4 "Details: %a" pp_print_error em;
    exit 1
  | Error e ->
    Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
    Verbose.error "[Error] %a" pp_print_error e;
    exit 1
